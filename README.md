# TraceXYZ Blockchain

## Starting and Stopping the Blockchain Network

1. Go to ./scripts folder
2. Run startTrace.sh to lauch the network

## Installing required pacages

1. Go to project root folder
2. Run sudo npm install --unsafe-perm=true --allow-root 

## Installing Chaincode in test network

1. Place chaincode in ./chaincode/src/trace/ folder
2. Go to ./scripts folder
3. Run `node install_chaincode.js` to install the chaincodes
4. Run `node instantiate_chaincode.js` to instantiate the chaincodes

## Test/Invoke Chaincode

1. Make sure you have installed and instantiated the chaincode first
2. Go to ./scripts folder
3. Run `node invoke.js` to run the chaincode method specified in `invoke.js`

## Upgrade Chaincode

1. Make sure you have installed the chaincode with a different version than you have already installed
2. Go to ./scripts folder
3. Run `node upgrade_chaincode.js` to upgrade the chaincode in the peers

# Contribution Guide

Contribution guidelines goes here
