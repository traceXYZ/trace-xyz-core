#!/usr/bin/env bash

cd ./storage/
govendor update bitbucket.org/tracexyz/core/...
govendor update github.com/golang-collections/collections/queue
govendor update github.com/golang-collections/collections/stack
govendor update github.com/golang/protobuf/proto
govendor update github.com/hyperledger/fabric/common/attrmgr
govendor update github.com/hyperledger/fabric/core/chaincode/lib/cid
govendor update github.com/oliveagle/jsonpath
govendor update github.com/pkg/errors
govendor update github.com/tidwall/gjson
govendor update github.com/tidwall/match
govendor update github.com/tidwall/sjson

cd ../admin/
govendor update bitbucket.org/tracexyz/core/...
govendor update github.com/golang-collections/collections/queue
govendor update github.com/golang-collections/collections/stack
govendor update github.com/golang/protobuf/proto
govendor update github.com/hyperledger/fabric/common/attrmgr
govendor update github.com/hyperledger/fabric/core/chaincode/lib/cid
govendor update github.com/pkg/errors
govendor update github.com/oliveagle/jsonpath
govendor update github.com/tidwall/gjson
govendor update github.com/tidwall/match
govendor update github.com/tidwall/sjson

cd ../milkproducer/
govendor update bitbucket.org/tracexyz/core/...
govendor update github.com/golang/protobuf/proto
govendor update github.com/golang/protobuf/ptypes/any
govendor update github.com/golang/protobuf/ptypes/empty
govendor update github.com/golang/protobuf/ptypes/timestamp
govendor update github.com/golang-collections/collections/queue
govendor update github.com/golang-collections/collections/stack
govendor update github.com/hyperledger/fabric/common/attrmgr
govendor update github.com/hyperledger/fabric/core/chaincode/lib/cid
govendor update github.com/pkg/errors
govendor update github.com/oliveagle/jsonpath
govendor update github.com/tidwall/gjson
govendor update github.com/tidwall/match
govendor update github.com/tidwall/sjson

cd ../saltproducer/
govendor update bitbucket.org/tracexyz/core/...
govendor update github.com/golang/protobuf/proto
govendor update github.com/golang/protobuf/ptypes/any
govendor update github.com/golang/protobuf/ptypes/empty
govendor update github.com/golang/protobuf/ptypes/timestamp
govendor update github.com/golang-collections/collections/queue
govendor update github.com/golang-collections/collections/stack
govendor update github.com/hyperledger/fabric/common/attrmgr
govendor update github.com/hyperledger/fabric/core/chaincode/lib/cid
govendor update github.com/pkg/errors
govendor update github.com/oliveagle/jsonpath
govendor update github.com/tidwall/gjson
govendor update github.com/tidwall/match
govendor update github.com/tidwall/sjson

cd ../cheeseproducer/
govendor update bitbucket.org/tracexyz/core/...
govendor update github.com/golang/protobuf/proto
govendor update github.com/golang/protobuf/ptypes/any
govendor update github.com/golang/protobuf/ptypes/empty
govendor update github.com/golang/protobuf/ptypes/timestamp
govendor update github.com/golang-collections/collections/queue
govendor update github.com/golang-collections/collections/stack
govendor update github.com/hyperledger/fabric/common/attrmgr
govendor update github.com/hyperledger/fabric/core/chaincode/lib/cid
govendor update github.com/pkg/errors
govendor update github.com/oliveagle/jsonpath
govendor update github.com/tidwall/gjson
govendor update github.com/tidwall/match
govendor update github.com/tidwall/sjson


cd ../transportservice/
govendor update bitbucket.org/tracexyz/core/...
govendor update github.com/golang/protobuf/proto
govendor update github.com/golang/protobuf/ptypes/any
govendor update github.com/golang/protobuf/ptypes/empty
govendor update github.com/golang/protobuf/ptypes/timestamp
govendor update github.com/golang-collections/collections/queue
govendor update github.com/golang-collections/collections/stack
govendor update github.com/hyperledger/fabric/common/attrmgr
govendor update github.com/hyperledger/fabric/core/chaincode/lib/cid
govendor update github.com/pkg/errors
govendor update github.com/oliveagle/jsonpath
govendor update github.com/tidwall/gjson
govendor update github.com/tidwall/match
govendor update github.com/tidwall/sjson

cd ../retailshop/
govendor update bitbucket.org/tracexyz/core/...
govendor update github.com/golang/protobuf/proto
govendor update github.com/golang/protobuf/ptypes/any
govendor update github.com/golang/protobuf/ptypes/empty
govendor update github.com/golang/protobuf/ptypes/timestamp
govendor update github.com/golang-collections/collections/queue
govendor update github.com/golang-collections/collections/stack
govendor update github.com/hyperledger/fabric/common/attrmgr
govendor update github.com/hyperledger/fabric/core/chaincode/lib/cid
govendor update github.com/pkg/errors
govendor update github.com/oliveagle/jsonpath
govendor update github.com/tidwall/gjson
govendor update github.com/tidwall/match
govendor update github.com/tidwall/sjson