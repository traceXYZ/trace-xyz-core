package graph

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"github.com/golang-collections/collections/queue"
	"github.com/golang-collections/collections/stack"
	"fmt"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
)

// Returns all the source nodes of a given node and for a given edge type
// The edge type is used for efficiency of queries
func CustomWalk(stub shim.ChaincodeStubInterface, itemID string, relationshipWalkPath []relationships.GraphRelationshipType,
	includeHistory, includeFuture bool) ([]datastructs.Node, []datastructs.Edge, error) {
	var nodesInPath []datastructs.Node
	var edgesInPath []datastructs.Edge
	walkerQueue := queue.New()
	for _, edgeType := range relationshipWalkPath {
		walkerQueue.Enqueue(edgeType)
	}
	anchorNode, success, err := GetNode(stub, itemID)
	if err != nil {
		return nil, nil, err
	}
	if !success {
		return nil, nil, fmt.Errorf("cannot find the anchor node for the traversal")
	}
	var visitedNodes = make(map[string]bool)
	currentNodes := stack.New()
	currentNodes.Push(anchorNode)
	nodesInPath = append(nodesInPath, anchorNode)
	visitedNodes[anchorNode.ItemID] = true
	for walkerQueue.Len() > 0 {
		currentRelationshipType := walkerQueue.Dequeue()
		currentTempNodes := stack.New()
		for currentNodes.Len() > 0 {
			currentTempNodes.Push(currentNodes.Pop())
		}
		for currentTempNodes.Len() > 0 {
			currentAnchorNode := currentTempNodes.Pop()
			var nodesD []datastructs.Node
			var edgesD []datastructs.Edge
			if includeFuture {
				nodesD, edgesD, err = GetDestinationsList(stub, currentAnchorNode.(datastructs.Node).
					ItemID, currentRelationshipType.(relationships.GraphRelationshipType))
			}
			var nodesS []datastructs.Node
			var edgesS []datastructs.Edge
			if includeHistory {
				nodesS, edgesS, err = GetSourcesList(stub, currentAnchorNode.(datastructs.Node).
					ItemID, currentRelationshipType.(relationships.GraphRelationshipType))
			}
			nodesD = append(nodesD, nodesS...)
			edgesD = append(edgesD, edgesS...)
			var currentVisitedNodes []datastructs.Node
			var currentVisitedEdges []datastructs.Edge
			if len(nodesD) == 0 {
				return nil, nil, fmt.Errorf("the path cannot be discovered in the graph")
			}
			if err != nil {
				return nil, nil, err
			}
			for _, node := range nodesD {
				if !visitedNodes[node.ItemID] {
					visitedNodes[node.ItemID] = true
					currentNodes.Push(node)
					currentVisitedNodes = append(currentVisitedNodes, node)
				}
			}
			for _, node := range currentVisitedNodes {
				for _, edge := range edgesD {
					if edge.SourceItemID == node.ItemID || edge.DestItemID == node.ItemID {
						currentVisitedEdges = append(currentVisitedEdges, edge)
					}
				}
			}

			nodesInPath = append(nodesInPath, currentVisitedNodes...)
			edgesInPath = append(edgesInPath, currentVisitedEdges...)
		}
	}
	return nodesInPath, edgesInPath, nil
}
