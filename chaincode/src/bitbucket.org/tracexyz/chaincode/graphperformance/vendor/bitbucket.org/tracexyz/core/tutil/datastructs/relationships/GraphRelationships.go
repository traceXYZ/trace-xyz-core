package relationships

type GraphRelationshipType string

const (
	PublicTrace   GraphRelationshipType = "PublicTrace"
	Specification GraphRelationshipType = "Specification"
	Handler       GraphRelationshipType = "Handler"
	Owner         GraphRelationshipType = "Owner"
	Transfer      GraphRelationshipType = "Transfer"
)
