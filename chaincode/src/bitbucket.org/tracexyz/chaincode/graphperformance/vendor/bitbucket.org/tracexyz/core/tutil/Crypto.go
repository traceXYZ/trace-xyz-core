package tutil

import (
	"github.com/hyperledger/fabric/bccsp/factory"
	"encoding/pem"
	"fmt"
	"github.com/hyperledger/fabric/bccsp"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
)

func Verify(publicKey, msg, signature []byte) (bool, error) {
	bccspInst := factory.GetDefault()
	fmt.Printf("\n PublicKey: %v \n", string(publicKey))
	fmt.Printf("\n Msg: %v \n", string(msg))

	if len(publicKey) == 0 || len(msg) == 0 || len(signature) == 0 {
		return false, fmt.Errorf("ownership verification failure. publicKey, message, or signature is empty")
	}
	key, _ := pem.Decode(publicKey)
	fmt.Printf("PKBLK: %v \n", key)
	if len(key.Bytes) == 0 {
		return false, fmt.Errorf("public key decode failed")
	}

	signKey, err := bccspInst.KeyImport(key.Bytes, &bccsp.ECDSAPKIXPublicKeyImportOpts{Temporary: true})
	if err != nil {
		fmt.Printf("ERR %v: \n", err)
		return false, err
	}
	isVerified, err := bccspInst.Verify(signKey, signature, msg, nil)
	fmt.Printf("ERRXX %v : \n ISV : %v \n", err, isVerified)
	return isVerified, err
}

func VerifyItemToken(storageItem datastructs.StorageItem, challengeResponse []byte) (bool, error) {
	isVerified, err := Verify([]byte(storageItem.Ownership.AuthorityPublicKey), []byte(storageItem.Ownership.NextChallenge), challengeResponse)
	if err != nil {
		return false, err
	}
	return isVerified, nil
}

func VerifyRelationshipToken(storageItem datastructs.StorageItem, challengeResponse []byte) (bool, error) {
	isVerified, err := Verify([]byte(storageItem.Handler.AuthorityPublicKey), []byte(storageItem.Handler.NextChallenge), challengeResponse)
	if err != nil {
		return false, err
	}
	return isVerified, nil
}

func VerifyOwnership(storageItem datastructs.StorageItem) (error) {
	fmt.Printf("PYLD :%v \n", storageItem.PAYLOAD)
	fmt.Printf("PK :%v \n", storageItem.Ownership.AuthorityPublicKey)

	isVerified, err := Verify([]byte(storageItem.Ownership.AuthorityPublicKey), storageItem.PAYLOAD, storageItem.Ownership.PayloadSignature)
	if err != nil {
		return fmt.Errorf(GenerateResponse("Fail", err.Error()))
	}
	if !isVerified {
		return fmt.Errorf(GenerateResponse("Fail", "failed to verify signature for owner "+storageItem.Ownership.AuthorityPublicKey))
	}
	return nil
}

func VerifyHandler(storageItem datastructs.StorageItem) (error) {
	payloadBytes, err := json.Marshal(storageItem.PAYLOAD)
	if err != nil {
		return fmt.Errorf(GenerateResponse("Fail", err.Error()))
	}
	isVerified, err := Verify([]byte(storageItem.Handler.AuthorityPublicKey), payloadBytes, []byte(storageItem.Handler.PayloadSignature))
	if err != nil {
		return fmt.Errorf(GenerateResponse("Fail", err.Error()))
	}
	if !isVerified {
		return fmt.Errorf(GenerateResponse("Fail", "failed to verify signature for owner "+storageItem.Handler.AuthorityPublicKey))
	}
	return nil
}
