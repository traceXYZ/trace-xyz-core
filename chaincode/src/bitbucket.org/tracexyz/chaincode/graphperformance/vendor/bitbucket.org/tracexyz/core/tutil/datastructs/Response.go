package datastructs

type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type StoreTxnResponse struct {
	Response
	StoredItemTokens          []StoredItemToken `json:"stored_item_tokens"`
	UpdatedItemTokens         []StoredItemToken `json:"updated_item_tokens"`
	UpdatedRelationshipTokens []StoredItemToken `json:"updated_relationship_tokens"`
	ProcessToken              StoredItemToken   `json:"process_token"`
	TxnID                     string            `json:"txn_id"`
}

type UpdateTxnResponse struct {
	Response
	UpdatedItemTokens []StoredItemToken `json:"updated_item_tokens"`
}

type AuthorityUpdateResponse struct {
	Response
	UpdatedItemToken StoredItemToken `json:"updated_item_token"`
}

type AssetRetrievalResponse struct {
	Response
	Nodes        []Node        `json:"nodes"`
	Edges        []Edge        `json:"edges"`
	StorageItems []StorageItem `json:"storage_items"`
}

type DirectRetrieveResponse struct {
	Response
	StorageItem StorageItem `json:"storage_item"`
}

type CreateItemResponse struct {
	StorageNetworkResponse StoreTxnResponse `json:"storage_network_response"`
	ItemID                 string           `json:"item_id"`
	ProcessID              string           `json:"process_id"`
	ProcessSpecID          string           `json:"process_spec_id"`
}

type AddItemResponse struct {
	StorageNetworkResponse StoreTxnResponse `json:"storage_network_response"`
	ItemID                 string           `json:"item_id"`
	ProcessID              string           `json:"process_id"`
	ProcessSpecID          string           `json:"process_spec_id"`
}

type SellItemResponse struct {
	StorageNetworkResponse StoreTxnResponse `json:"storage_network_response"`
	SellingItemID          string           `json:"selling_item_id"`
	SellingQuantity        float64          `json:"selling_quantity"`
	SellingProcessID       string           `json:"selling_process_id"`
	SellingQuantityKey     string           `json:"selling_quantity_key"`
}

type ManufactureItemResponse struct {
	StorageNetworkResponse StoreTxnResponse `json:"storage_network_response"`
	Inputs                 []InputItemToken `json:"inputs"`
	ManufacturedItemID     string           `json:"manufactured_item_id"`
	ProcessID              string           `json:"process_id"`
	ProcessSpecID          string           `json:"process_spec_id"`
}

type MergeItemsResponse struct {
	StorageNetworkResponse StoreTxnResponse `json:"storage_network_response"`
	InputItems             []InputToken     `json:"input_items"`
	ProcessID              string           `json:"process_id"`
	ProcessSpecID          string           `json:"process_spec_id"`
	ContainerID            string           `json:"container_id"`
}

type SplitItemsResponse struct {
	StorageNetworkResponse StoreTxnResponse `json:"storage_network_response"`
	OutputItems            []InputToken     `json:"output_items"`
	ProcessID              string           `json:"process_id"`
	ProcessSpecID          string           `json:"process_spec_id"`
	Container              InputToken       `json:"container"`
}

type TransferResponse struct {
	StorageNetworkResponse  StoreTxnResponse        `json:"storage_network_response"`
	AuthorityUpdateResponse AuthorityUpdateResponse `json:"authority_update_response"`
	ProcessID               string                  `json:"process_id"`
	ProcessSpecID           string                  `json:"process_spec_id"`
}
