package graph

import (
	"fmt"
	"bitbucket.org/tracexyz/core/tutil"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
)

func GetSourceNodesRecursive(stub shim.ChaincodeStubInterface, itemID string, relationshipType relationships.GraphRelationshipType, maxDepth int) ([]datastructs.Node, []datastructs.Edge, error) {
	nodes, edges, err := GetSourcesList(stub, itemID, relationshipType)
	if err != nil {
		return nil, nil, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	if nodes == nil || maxDepth < 0 {
		return nodes, edges, nil
	} else {
		for _, node := range nodes {
			tempNodes, tempEdges, err := GetSourceNodesRecursive(stub, node.ItemID, relationshipType, maxDepth-1)
			if err != nil {
				return nil, nil, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
			}
			nodes = append(nodes, tempNodes...)
			edges = append(edges, tempEdges...)
		}
		return nodes, edges, nil
	}
}

func GetDestNodesRecursive(stub shim.ChaincodeStubInterface, itemID string, relationshipType relationships.GraphRelationshipType, maxDepth int) ([]datastructs.Node, []datastructs.Edge, error) {
	var nodes []datastructs.Node
	nodes, edges, err := GetDestinationsList(stub, itemID, relationshipType)
	if err != nil {
		return nil, nil, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	if nodes == nil || maxDepth < 0 {
		return nodes, edges, nil
	} else {
		for _, node := range nodes {
			var tempNodes []datastructs.Node
			tempNodes, tempEdges, err := GetDestNodesRecursive(stub, node.ItemID, relationshipType, maxDepth-1)
			if err != nil {
				return nil, nil, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
			}
			nodes = append(nodes, tempNodes...)
			edges = append(edges, tempEdges...)
		}
		return nodes, edges, nil
	}
}
