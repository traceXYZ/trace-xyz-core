package datastructs

type CreateItemRequest struct {
	SpecificationID string      `json:"specification_id"`
	CreationProcess StorageItem `json:"creation_process"`
	CreatedItem     StorageItem `json:"created_item"`
}

type AddSingleItemRequest struct {
	Item StorageItem `json:"item"`
}

type SellingRequest struct {
	SpecificationID     string         `json:"specification_id"`
	SellingProcess      StorageItem    `json:"selling_process"`
	SaleAgreement       SoldQuantity   `json:"sale_agreement"`
	SoldQuantity        StorageItem    `json:"sold_quantity"`
	SellingItem         InputItemToken `json:"selling_item_token"`
	SellingRelationship InputToken     `json:"selling_relationship_token"`
}

type SoldQuantity struct {
	Item                      []byte `json:"item"`
	BuyerPublicKey            string `json:"buyer_public_key"`
	SellerPublicKey           string `json:"seller_public_key"`
	SellerSignatureForSoldQty []byte `json:"seller_signature_for_sold_qty"`
	BuyerSignatureForSoldQty  []byte `json:"buyer_signature_for_sold_qty"`
}

type ManufactureItemRequest struct {
	SpecificationID      string           `json:"specification_id"`
	InputItems           []InputItemToken `json:"input_item_tokens"`
	InputRelationships   []InputToken     `json:"input_relationship_tokens"`
	ManufacturingProcess StorageItem      `json:"creation_process"`
	ManufacturedItem     StorageItem      `json:"created_item"`
}

type InputToken struct {
	ItemID                 string `json:"item_id"`
	InputChallengeResponse []byte `json:"input_challenge_response"`
}

type InputItemToken struct {
	InputToken
	UsedQty float64 `json:"used_qty"`
}

type SplitItemRequest struct {
	ItemToken         InputToken    `json:"item_token"`
	RelationshipToken InputToken    `json:"relationship_token"`
	SplittingProcess  StorageItem   `json:"splitting_process"`
	SpecificationID   string        `json:"specification_id"`
	SplitItems        []StorageItem `json:"split_items"`
}

type MergeIntoContainerRequest struct {
	SpecificationID string       `json:"specification_id"`
	InputItems      []InputToken `json:"input_items"`
	MergingProcess  StorageItem  `json:"merging_process"`
	Container       StorageItem  `json:"container"`
}

type SplitFromContainerRequest struct {
	SpecificationID  string       `json:"specification_id"`
	Container        InputToken   `json:"container"`
	OutputItems      []InputToken `json:"output_items"`
	SplittingProcess StorageItem  `json:"splitting_process"`
}

type TransferRequest struct {
	SpecificationID               string                   `json:"specification_id"`
	TransferProcess               StorageItem              `json:"transfer_process"`
	AuthorityTransferRequest      AuthorityTransferRequest `json:"authority_transfer_request"`
	RelationshipChallengeResponse []byte                   `json:"relationship_challenge_response"`
}
