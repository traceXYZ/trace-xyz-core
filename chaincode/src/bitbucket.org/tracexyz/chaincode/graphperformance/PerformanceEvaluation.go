/*
TraceXYZ MilkProducer Chaincode
*/

package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"github.com/hyperledger/fabric/bccsp"
	"github.com/hyperledger/fabric/bccsp/factory"
	"strconv"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/graph"
	"github.com/hyperledger/fabric/common/util"
	"math/rand"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
	"encoding/json"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
)

type PerformanceEvaluation struct {
	bccspInst bccsp.BCCSP
}

func main() {
	err := shim.Start(new(PerformanceEvaluation))
	if err != nil {
		fmt.Printf("Error starting TraceXZ chaincode - %s", err)
	}
}

func (t *PerformanceEvaluation) Init(stub shim.ChaincodeStubInterface) pb.Response {
	t.bccspInst = factory.GetDefault()
	return shim.Success([]byte("Success"))
}

func (t *PerformanceEvaluation) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Printf("F %v \n", function)
	fmt.Printf("A %v \n", args)
	// Handle different functions
	switch function {
	case "init":
		return t.Init(stub)
	case "create_graph":
		if len(args) < 2 {
			return shim.Error("ERR")
		}
		nodes, err := strconv.Atoi(args[0])
		if err != nil {
			return shim.Error(err.Error())
		}
		edges, err := strconv.Atoi(args[1])
		if err != nil {
			return shim.Error(err.Error())
		}

		return AddGraph(stub, nodes, edges)
	case "bfs":
		if len(args) < 2 {
			return shim.Error("ERR")
		}
		return BFS(stub, args[0], args[1])
	case "supply_chain_simulation":
		if len(args) < 1 {
			return shim.Error("ERR")
		}
		levels, err := strconv.Atoi(args[0])
		if err != nil {
			return shim.Error(err.Error())
		}
		connf, err := strconv.Atoi(args[1])
		if err != nil {
			return shim.Error(err.Error())
		}
		return AddSupplyChainSimulationGraph(stub, levels, connf)
	}

	fmt.Println("Received unknown invoke function name - " + function)
	return shim.Error("Received unknown invoke function name - '" + function + "'")
}

type GraphTestResponse struct {
	Nodes []datastructs.Node `json:"nodes"`
	TxnID string             `json:"txn_id"`
}

func AddGraph(stub shim.ChaincodeStubInterface, nodes, edges int) pb.Response {
	nodeObjects := make([]datastructs.Node, nodes)

	// Create nodes
	for i := 0; i < nodes; i++ {
		testID := util.GenerateUUID() + util.GenerateUUID()
		tempNode := graph.NewNode(testID, "TEST")
		nodeObjects[i] = tempNode

		success, err := graph.AddNode(stub, tempNode)
		if err != nil {
			return shim.Error(err.Error())
		}
		if !success {
			fmt.Printf("Node not added")
		}
	}

	for i := 0; i < edges; i++ {
		sourceID := nodeObjects[rand.Intn(nodes)].ItemID
		destID := nodeObjects[rand.Intn(nodes)].ItemID
		for sourceID == destID {
			destID = nodeObjects[rand.Intn(nodes)].ItemID
		}
		tempEdge := graph.NewEdge(sourceID, destID, relationships.PublicTrace, states.Active)
		success, err := graph.AddEdge(stub, tempEdge)
		if err != nil {
			return shim.Error(err.Error())
		}
		if !success {
			fmt.Printf("Node not added")
		}
	}

	var response GraphTestResponse
	response.Nodes = nodeObjects
	response.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error("cannot marshal nodes array")
	}
	return shim.Success(responseBytes)
}

type GraphTestResponse2 struct {
	Nodes []datastructs.Node `json:"nodes"`
	TxnID string             `json:"txn_id"`
}

func AddSupplyChainSimulationGraph(stub shim.ChaincodeStubInterface, levels, conn_factor int) pb.Response {
	var nodeObjects = make([][]datastructs.Node, levels)
	var nodeObjectsResponse []datastructs.Node
	// Create nodes
	for i := 0; i < levels; i++ {
		var nodesCount = 0
		for nodesCount < 2 {
			nodesCount = rand.Intn(30)
		}
		nodeObjects[i] = make([]datastructs.Node, nodesCount)
		for j := 0; j < nodesCount; j++ {
			testID := util.GenerateUUID() + util.GenerateUUID()
			tempNode := graph.NewNode(testID, "TEST")
			nodeObjects[i][j] = tempNode
			success, err := graph.AddNode(stub, tempNode)
			nodeObjectsResponse = append(nodeObjectsResponse, tempNode)
			if err != nil {
				return shim.Error(err.Error())
			}
			if !success {
				fmt.Printf("Node not added")
			}
		}
	}

	for i := 0; i < levels-1; i++ {
		layer1Length := len(nodeObjects[i])
		layer2Length := len(nodeObjects[i+1])

		for j := 0; j < (layer1Length+layer2Length)*conn_factor; j++ {
			sourceID := nodeObjects[i][rand.Intn(layer1Length)].ItemID
			destID := nodeObjects[i+1][rand.Intn(layer2Length)].ItemID
			tempEdge := graph.NewEdge(sourceID, destID, relationships.PublicTrace, states.Active)
			success, err := graph.AddEdge(stub, tempEdge)
			if err != nil {
				return shim.Error(err.Error())
			}
			if !success {
				fmt.Printf("Node not added")
			}
		}
	}

	var response GraphTestResponse
	response.Nodes = nodeObjectsResponse
	response.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error("cannot marshal nodes array")
	}
	return shim.Success(responseBytes)
}

func BFS(stub shim.ChaincodeStubInterface, nodeID, txnID string) pb.Response {

	discoveredNodes, _, err := graph.BFS(stub, nodeID, relationships.PublicTrace)
	if err != nil {
		return shim.Error(err.Error())
	}

	var response GraphTestResponse
	response.Nodes = discoveredNodes
	response.TxnID = txnID

	responseBytes, err := json.Marshal(response)
	//fmt.Printf("%v \n",string(responseBytes))
	return shim.Success(responseBytes)
}
