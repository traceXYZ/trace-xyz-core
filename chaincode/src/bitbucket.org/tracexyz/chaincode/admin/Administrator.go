/*
TraceXYZ Administrator Chaincode
*/

package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"encoding/json"
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/rwledger"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/supplychain"
)

type Administrator struct {
}

// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(Administrator))
	if err != nil {
		fmt.Printf("Error starting TraceXZ chaincode - %s", err)
	}
}

var keys datastructs.KeyPair
var keyPairIdentity string

// ============================================================================================================================
// Init - initialize the chaincode
//
// ============================================================================================================================
func (t *Administrator) Init(stub shim.ChaincodeStubInterface) pb.Response {

	return shim.Success([]byte("Success"))
}

// ============================================================================================================================
// Invoke - Our entry point for Invocations
// Administrator Chaincode is used to perform following functions
// *Register user in the system
// *Update a quota value
// *Read a quota value
// ============================================================================================================================
func (t *Administrator) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	if len(args) < 1 {
		return shim.Error(tutil.GenerateResponse("Fail",
			"minimum of 1 arguments required to execute this function"))
	}
	// Handle different functions
	switch function {
	case "init":
		return t.Init(stub)
	case "register":
		return t.Register(stub, args[0])
	case "update_quota":
		return t.QuotaUpdate(stub, args[0])
	case "read_quota":
		return t.ReadQuota(stub, args[0])
	case "read_user_context":
		return t.ReadUserContext(stub, args[0])
	case "spend_resource":
		return t.SpendQuota(stub, args[0])
	case "rollback_spend_resource":
		return t.RollbackQuota(stub, args[0])
	case "store_spec":
		return t.AddSpec(stub, args[0])
	case "add_inspection_report":
		return t.AddInspectionReport(stub, args[0])

	}
	fmt.Println("Received unknown invoke function name - " + function)
	return shim.Error("Received unknown invoke function name - '" + function + "'")
}

// ============================================================================================================================
// Register - Smart Contract for Registering a User
// ============================================================================================================================
func (t *Administrator) Register(stub shim.ChaincodeStubInterface, request string) pb.Response {
	isAdmin, err := tutil.IsAdmin(stub)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !isAdmin {
		return shim.Error(tutil.GenerateResponse("Fail", "only admin users can register other users"))
	}
	var userContext datastructs.UserContext
	err = json.Unmarshal([]byte(request), &userContext)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	userExist, err := rwledger.KeyExists(stub, userContext.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if userExist {
		return shim.Error(tutil.GenerateResponse("Fail", "user ID already exists"))
	}
	return updateUserContext(stub, userContext)
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *Administrator) QuotaUpdate(stub shim.ChaincodeStubInterface, request string) pb.Response {
	isAdmin, err := tutil.IsAdmin(stub)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !isAdmin {
		return shim.Error(tutil.GenerateResponse("Fail", "only admin users can update the quota"))
	}
	var quotaUpdate datastructs.QuotaUpdate
	json.Unmarshal([]byte(request), &quotaUpdate)
	userContext, err := readUserContext(stub, quotaUpdate.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var updated = false
	for index, quota := range userContext.Quota {
		if quota.FieldPath == quotaUpdate.Quota.FieldPath {
			userContext.Quota[index].Quota = quotaUpdate.Quota.Quota
			updated = true
		}
	}
	if updated {
		return updateUserContext(stub, userContext)
	} else {
		return shim.Error(tutil.GenerateResponse("Fail", "the requested quota field not found"))
	}

}

// ============================================================================================================================
// ReadQuota - Smart Contract for Retrieving Quota values from blockchain
// ============================================================================================================================
func (t *Administrator) ReadQuota(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var quotaRead datastructs.QuotaRead
	json.Unmarshal([]byte(request), &quotaRead)
	isValidUser, err := tutil.IsValidUser(stub, quotaRead.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !isValidUser {
		return shim.Error(tutil.GenerateResponse("Fail", "the requester does not have proper access to read the information"))
	}
	userContext, err := readUserContext(stub, quotaRead.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	for index, quota := range userContext.Quota {
		if quota.FieldPath == quotaRead.FieldPath {
			ret, err := json.Marshal(userContext.Quota[index])
			if err != nil {
				return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
			}
			return shim.Success(ret)
		}
	}
	return shim.Error(tutil.GenerateResponse("Fail", "the requested field path not found"))
}

// ============================================================================================================================
// ReadUserContext - Smart Contract for Retrieving User Context from blockchain
// ============================================================================================================================
func (t *Administrator) ReadUserContext(stub shim.ChaincodeStubInterface, request string) pb.Response {
	userContext, err := readUserContext(stub, request)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	isValidUser, err := tutil.IsValidUser(stub, userContext.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !isValidUser {
		return shim.Error(tutil.GenerateResponse("Fail", "the requester does not have proper access to read the information"))
	}
	userContextBytes, err := json.Marshal(userContext)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return shim.Success(userContextBytes)
}

// ============================================================================================================================
// SpendQuota - Smart Contract for Spending an amount of resource
// ============================================================================================================================
func (t *Administrator) SpendQuota(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var quotaSpend datastructs.QuotaSpend
	json.Unmarshal([]byte(request), &quotaSpend)
	isValidUser, err := tutil.IsValidInvokerUser(stub, quotaSpend.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !isValidUser {
		return shim.Error(tutil.GenerateResponse("Fail", "the requester does not have proper access to read the information"))
	}
	userContext, err := readUserContext(stub, quotaSpend.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	for index, quota := range userContext.Quota {
		if quota.FieldPath == quotaSpend.FieldPath {
			nonce := util.GenerateUUID()
			userContext.Quota[index].Quota = quota.Quota - quotaSpend.SpendingQty
			if userContext.Quota[index].Quota < 0 {
				return shim.Error(tutil.GenerateResponse("Fail", "cannot spend over the quota limit"))
			}
			userContext.Quota[index].Nonce = append(userContext.Quota[index].Nonce,
				string(util.ComputeSHA256([]byte(nonce+fmt.Sprintf("%f", quotaSpend.SpendingQty)))))
			isSuccess, err := rwledger.CreateOrUpdate(stub, userContext.UserID, userContext)
			if err != nil {
				return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
			}
			if !isSuccess {
				return shim.Error(tutil.GenerateResponse("Fail", "failed to update the blockchain records!"))
			}
			return shim.Success([]byte(tutil.GenerateResponse("Success", nonce)))
		}
	}
	return shim.Error(tutil.GenerateResponse("Fail", "the requested field path not found"))
}

// ============================================================================================================================
// RollbackQuota - Smart Contract for Rolling back the quota into its previous value
// ============================================================================================================================
func (t *Administrator) RollbackQuota(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var quotaRollback datastructs.QuotaRollback
	json.Unmarshal([]byte(request), &quotaRollback)
	isValidUser, err := tutil.IsValidInvokerUser(stub, quotaRollback.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !isValidUser {
		return shim.Error(tutil.GenerateResponse("Fail", "the requester does not have proper access to read the information"))
	}
	userContext, err := readUserContext(stub, quotaRollback.UserID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	for index, quota := range userContext.Quota {
		if quota.FieldPath == quotaRollback.FieldPath {
			realNonce := string(util.ComputeSHA256([]byte(quotaRollback.Nonce + fmt.Sprintf("%f", quotaRollback.SpendingQty))))
			for nindex, nonce := range quota.Nonce {
				if nonce == realNonce {
					// update user context
					userContext.Quota[index].Quota = userContext.Quota[index].Quota + quotaRollback.SpendingQty
					// delete old nonce
					userContext.Quota[index].Nonce = append(userContext.Quota[index].Nonce[:nindex], userContext.Quota[index].Nonce[nindex+1:]...)
					return updateUserContext(stub, userContext)
				}
			}
		}
	}
	return shim.Error(tutil.GenerateResponse("Fail", "the requested field path not found"))
}

type AddSpecRequest struct {
	SpecifyingAuthorityPublicKey string `json:"specifying_authority_public_key"`
	Specification                []byte `json:"specification"`
	SpecificationSignature       []byte `json:"specification_signature"`
}

// ============================================================================================================================
// RollbackQuota - Smart Contract for Rolling back the quota into its previous value
// ============================================================================================================================
func (t *Administrator) AddSpec(stub shim.ChaincodeStubInterface, request string) pb.Response {
	isAdmin, err := tutil.IsAdmin(stub)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !isAdmin {
		return shim.Error(tutil.GenerateResponse("Fail", "only admin users can register other users"))
	}
	var requestObj AddSpecRequest
	err = json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var spec datastructs.StorageItem
	spec.Ownership.AuthorityPublicKey = requestObj.SpecifyingAuthorityPublicKey
	spec.Ownership.PayloadSignature = requestObj.SpecificationSignature
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.SpecifyingAuthorityPublicKey
	handlerAuthority.PayloadSignature = requestObj.SpecificationSignature
	spec.Handlers = append(spec.Handlers, handlerAuthority)
	spec.PAYLOAD = requestObj.Specification

	specBytes, err := json.Marshal(spec)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	fmt.Printf("PASS1 \n")
	return supplychain.AddSingleItem(stub, string(specBytes))
}

// ============================================================================================================================
// updateUserContext - updates the user context with a different value set
// ============================================================================================================================
func updateUserContext(stub shim.ChaincodeStubInterface, userContext datastructs.UserContext) pb.Response {
	isSuccess, err := rwledger.CreateOrUpdate(stub, userContext.UserID, userContext)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if isSuccess {
		return shim.Success([]byte(tutil.GenerateResponse("Success", "successfully stored values in blockchain!")))
	} else {
		return shim.Error(tutil.GenerateResponse("Fail", "failed to update the blockchain records!"))
	}
}

// ============================================================================================================================
// readUserContext - Reads the user context from the blockchain
// ============================================================================================================================
func readUserContext(stub shim.ChaincodeStubInterface, userID string) (datastructs.UserContext, error) {
	userContextBytes, err := rwledger.IfKeyExistsReturnValue(stub, userID)
	var userContext datastructs.UserContext
	if err != nil {
		return userContext, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	if userContextBytes == nil {
		return userContext, fmt.Errorf(tutil.GenerateResponse("Fail", "user context not found in blockchain"))
	}
	err = json.Unmarshal(userContextBytes, &userContext)
	if err != nil {
		return userContext, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	return userContext, nil
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *Administrator) AddInspectionReport(stub shim.ChaincodeStubInterface, request string) pb.Response {
	return supplychain.AddUpdate(stub, request)
}
