package graph

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"fmt"
	"bitbucket.org/tracexyz/core/rwledger"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
)

// ============================================================================================================================
// Graph Library Low Level Implementation
// ============================================================================================================================
// The Low level graph library provides functionality to create and manage a graph data structure within the blockchain

func NewNode(itemID string, nodeType string) datastructs.Node {
	return datastructs.Node{
		ItemID:   itemID,
		ItemType: nodeType,
		Enabled:  true,
	}
}

func NewEdge(srcItemID, destItemID string, relationshipType relationships.GraphRelationshipType, relationshipState states.GraphRelationshipState) datastructs.Edge {
	return datastructs.Edge{
		SourceItemID:      srcItemID,
		DestItemID:        destItemID,
		RelationshipType:  string(relationshipType),
		RelationshipState: string(relationshipState),
		Enabled:           true,
	}
}

// GetNode returns the Node. It returns false if the Edge
// does not exist in the graph.
func GetNode(stub shim.ChaincodeStubInterface, itemID string) (datastructs.Node, bool, error) {
	var tempNode datastructs.Node
	compKeyNode, err := stub.CreateCompositeKey("Node", []string{itemID})
	if err != nil {
		return tempNode, false, err
	}
	success, err := rwledger.Read(stub, compKeyNode, &tempNode)
	if err != nil || !success {
		return tempNode, false, err
	}
	return tempNode, true, nil
}

// AddNode adds a node to a graph, and returns false
// if the node already existed in the graph.
func AddNode(stub shim.ChaincodeStubInterface, node datastructs.Node) (bool, error) {
	exists, err := rwledger.KeyExists(stub, node.ItemID)
	compKeyNode, err := stub.CreateCompositeKey("Node", []string{node.ItemID})
	if err != nil {
		return false, err
	}
	if exists {
		return false, fmt.Errorf(" graph node already exsists. ID:" + node.ItemID)
	}
	if err != nil {
		return false, err
	}
	return rwledger.CreateOrUpdate(stub, compKeyNode, node)
}

// AddEdge adds an edge from node1 to node2 with the type and state
// It returns error if a node does not exist.
// This method stores the edge without explicitly checking if there are other similar edges between
// src and destination
func AddEdgeUnsafe(stub shim.ChaincodeStubInterface, edge datastructs.Edge) (bool, error) {
	compKeySrc, err := stub.CreateCompositeKey("Edge", []string{edge.RelationshipType, edge.SourceItemID, edge.DestItemID})
	if err != nil {
		return false, err
	}
	compKeyDest, err := stub.CreateCompositeKey("Edge", []string{edge.RelationshipType, edge.DestItemID, edge.SourceItemID})
	if err != nil {
		return false, err
	}
	success, err := rwledger.CreateOrUpdate(stub, compKeySrc, edge)
	if err != nil || !success {
		return false, err
	}
	success, err = rwledger.CreateOrUpdate(stub, compKeyDest, edge)
	if err != nil || !success {
		return false, err
	}
	return true, nil
}

// AddEdge adds an edge from node1 to node2 with the type and state
// It returns error if a node does not exist.
// This method stores the edge without explicitly checking if there are other similar edges between
// src and destination
func AddEdge(stub shim.ChaincodeStubInterface, edge datastructs.Edge) (bool, error) {
	_, isAvailable, _ := GetEdge(stub, edge.RelationshipType, edge.SourceItemID, edge.DestItemID)
	if isAvailable {
		return false, fmt.Errorf("an edge of this type already exists between nodes. Use update instead")
	}
	fmt.Printf("ADD EDGE \n")
	return AddEdgeUnsafe(stub, edge)
}

// GetEdge returns the edge if it exists in the graph. It returns false if the Edge
// does not exist in the graph.
func GetEdge(stub shim.ChaincodeStubInterface,
	edgeType, src, dest string) (datastructs.Edge, bool, error) {
	var tempSrcEdge datastructs.Edge
	edgeKey, err := stub.CreateCompositeKey("Edge", []string{edgeType, src, dest})
	if err != nil {
		return tempSrcEdge, false, err
	}
	success1, err := rwledger.Read(stub, edgeKey, &tempSrcEdge)
	if err != nil {
		return tempSrcEdge, false, err
	}
	if !success1 {
		return tempSrcEdge, false, fmt.Errorf("cannot find the edge associated with the source node")
	}

	var tempDestEdge datastructs.Edge
	edgeKey, err = stub.CreateCompositeKey("Edge", []string{edgeType, dest, src})
	if err != nil {
		return tempDestEdge, false, err
	}
	success2, err := rwledger.Read(stub, edgeKey, &tempDestEdge)
	if err != nil {
		return tempSrcEdge, false, err
	}
	if !success2 {
		return tempSrcEdge, false, fmt.Errorf("cannot find the edge associated with the source node")
	}
	if success1 && success2 {
		if tempSrcEdge.SourceItemID != tempDestEdge.SourceItemID ||
			tempSrcEdge.DestItemID != tempDestEdge.DestItemID ||
			tempSrcEdge.RelationshipType != tempDestEdge.RelationshipType ||
			tempSrcEdge.RelationshipState != tempDestEdge.RelationshipState {
			return tempSrcEdge, false, fmt.Errorf("edge state returned for the given keys is inconsistent")
		}
	}

	fmt.Printf("EDGE %v \nSUCCESS %v \n", tempDestEdge, success1 && success2)

	return tempDestEdge, true, nil
}

// UpdateEdgeState updates the state of an edge between 2 nodes.
// Discouraged from using this function because its computationally inefficient
func UpdateEdge(stub shim.ChaincodeStubInterface, src, dest,
edgeType, edgeState string) (bool, error) {
	tempEdge, isAvailable, err := GetEdge(stub, edgeType, src, dest)
	if err != nil {
		return false, err
	}
	if !isAvailable {
		return false, fmt.Errorf("edge of this type does not exist between the nodes")
	}
	tempEdge.RelationshipType = edgeType
	tempEdge.RelationshipState = edgeState
	compKeySrc, err := stub.CreateCompositeKey("Edge", []string{edgeType, src, dest})
	if err != nil {
		return false, err
	}
	compKeyDest, err := stub.CreateCompositeKey("Edge", []string{edgeType, dest, src})
	if err != nil {
		return false, err
	}
	success, err := rwledger.CreateOrUpdate(stub, compKeySrc, tempEdge)
	if err != nil || !success {
		return false, err
	}
	success, err = rwledger.CreateOrUpdate(stub, compKeyDest, tempEdge)
	if err != nil || !success {
		return false, err
	}
	return true, nil
}

// DeleteEdge function removes the edge between two nodes
// Discouraged to use this function because it could lead the graph into inconsistent stages
// instead, use UpdateEdgeState function and set the edgeState to 'Disabled' for efficient use
func DeleteEdge(stub shim.ChaincodeStubInterface, src, dest,
edgeType string) (bool, error) {
	_, isAvailable, err := GetEdge(stub, edgeType, src, dest)
	if err != nil {
		return false, err
	}
	if !isAvailable {
		return false, fmt.Errorf("edge of this type does not exist between the nodes")
	}
	compKeySrc, err := stub.CreateCompositeKey("Edge", []string{edgeType, src, dest})
	if err != nil {
		return false, err
	}
	compKeyDest, err := stub.CreateCompositeKey("Edge", []string{edgeType, dest, dest})
	if err != nil {
		return false, err
	}
	success, err := rwledger.Remove(stub, compKeySrc)
	if err != nil {
		return false, err
	}
	if !success {
		return false, fmt.Errorf("error deleting the edge")
	}
	success, err = rwledger.Remove(stub, compKeyDest)
	if err != nil {
		return false, err
	}
	if !success {
		return false, fmt.Errorf("error deleting the edge")
	}
	return true, nil
}
