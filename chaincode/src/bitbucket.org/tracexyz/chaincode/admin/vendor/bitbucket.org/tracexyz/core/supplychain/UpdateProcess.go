package supplychain

import (
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/comm"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"encoding/json"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/hyperledger/fabric/common/util"
)

func AddUpdate(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var requestObj datastructs.ProcessUpdate
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(err.Error())
	}

	//Create the items for storage

	generatedIOTUpdateID := util.GenerateUUID()
	IOTUpdateNode := graph.NewNode(generatedIOTUpdateID, "ProcessUpdate")

	itemToUpdateEdge := graph.NewEdge(requestObj.ProcessToken.ItemID, generatedIOTUpdateID, requestObj.UpdateType, states.Active)
	var itemToUpdateEdgeUpdate datastructs.EdgeUpdate
	itemToUpdateEdgeUpdate.Edge = itemToUpdateEdge
	itemToUpdateEdgeUpdate.UpdateAuthentication.ItemID = requestObj.ProcessToken.ItemID
	itemToUpdateEdgeUpdate.UpdateAuthentication.ChallengeResponse = requestObj.ProcessToken.InputChallengeResponse

	var update = requestObj.Update
	update.ID = generatedIOTUpdateID

	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = []datastructs.Node{IOTUpdateNode}
	storageRequest.SourceToItemEdges = []datastructs.EdgeUpdate{itemToUpdateEdgeUpdate}
	storageRequest.Items = []datastructs.StorageItem{update}
	storageRequest.Process.ProcessData.ID = ""

	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	// todo: commit the creation item

	var response datastructs.ProcessUpdateResponse
	response.ResponseType = "process_update"
	response.TxnID = stub.GetTxID()
	response.StorageNetworkResponse = storageNWResponse

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(responseBytes)
}
