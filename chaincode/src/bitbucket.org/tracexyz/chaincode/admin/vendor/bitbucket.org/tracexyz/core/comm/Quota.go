package comm

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"fmt"
	"encoding/json"
	"bitbucket.org/tracexyz/core/tutil"
)

func SpendQuota(stub shim.ChaincodeStubInterface, spendingQty float64, fieldPath string) (string, error) {
	args := toChaincodeArgs("spend_resource")
	enrollmentID, err := tutil.GetEnrollmentID(stub)
	if err != nil {
		fmt.Printf("Storage Response Err %v \n", err)
		return "", err
	}
	var quotaSpendRequest = datastructs.QuotaSpend{
		SpendingQty: spendingQty,
		QuotaRead: datastructs.QuotaRead{
			UserID:    enrollmentID,
			FieldPath: fieldPath,
		},
	}
	quotaSpendRequestBytes, err := json.Marshal(quotaSpendRequest)
	if err != nil {
		fmt.Printf("Storage Response Err %v \n", err)
		return "", err
	}
	args = append(args, quotaSpendRequestBytes)
	fmt.Printf("ARGS %v \n", args)
	response := stub.InvokeChaincode("admin_cc", args, "")
	if response.Status == shim.OK {
		fmt.Printf("Storage Response Payload %v \n", string(response.Payload))
		var quotaUseResponse datastructs.Response
		err := json.Unmarshal(response.Payload, &quotaUseResponse)
		if err != nil {
			fmt.Printf("Storage Response Err %v \n", err)
			return "", err
		}
		if quotaUseResponse.Status == "Fail" {
			return "", fmt.Errorf(quotaUseResponse.Message)
		}
		return quotaUseResponse.Message, nil
	} else {
		return "", fmt.Errorf(response.Message)
	}
}

func RollbackQuota(stub shim.ChaincodeStubInterface, spendingQty float64, fieldPath, nonce string) (string, error) {
	args := toChaincodeArgs("rollback_spend_resource")
	enrollmentID, err := tutil.GetEnrollmentID(stub)
	if err != nil {
		fmt.Printf("Storage Response Err %v \n", err)
		return "", err
	}
	var quotaRollbackRequest = datastructs.QuotaRollback{
		QuotaSpend: datastructs.QuotaSpend{
			SpendingQty: spendingQty,
			QuotaRead: datastructs.QuotaRead{
				UserID:    enrollmentID,
				FieldPath: fieldPath,
			},
		},
		Nonce: nonce,
	}
	quotaSpendRequestBytes, err := json.Marshal(quotaRollbackRequest)
	if err != nil {
		fmt.Printf("Storage Response Err %v \n", err)
		return "", err
	}
	args = append(args, quotaSpendRequestBytes)
	fmt.Printf("ARGS %v \n", args)
	response := stub.InvokeChaincode("admin_cc", args, "")
	if response.Status == shim.OK {
		fmt.Printf("Storage Response Payload %v \n", string(response.Payload))
		var quotaUseResponse datastructs.Response
		err := json.Unmarshal(response.Payload, &quotaUseResponse)
		if err != nil {
			fmt.Printf("Storage Response Err %v \n", err)
			return "", err
		}
		if quotaUseResponse.Status == "Fail" {
			return "", fmt.Errorf(quotaUseResponse.Message)
		}
		return quotaUseResponse.Message, nil
	} else {
		return "", fmt.Errorf(response.Message)
	}
}
