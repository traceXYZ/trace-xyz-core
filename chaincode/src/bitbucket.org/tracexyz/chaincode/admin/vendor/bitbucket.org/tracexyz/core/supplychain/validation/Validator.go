package validation

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func validateInputs(stub shim.ChaincodeStubInterface, specification map[string]interface{}, request interface{}) (bool, error) {
	if len(specification) < 1 {
		return false, nil
	}
	var retValue = true
	for specKey, specValue := range specification {
		switch specKey {
		case "compare":
			isValid, err := processSpecCompare(specValue, request)
			if err != nil {
				return false, err
			}
			retValue = retValue && isValid
			break
		case "ratio":
			isValid, err := processSpecRatio(specValue, request)
			if err != nil {
				return false, err
			}
			retValue = retValue && isValid
			break
		case "quota":
			isValid, err := processSpecQuota(stub, specValue, request)
			if err != nil {
				return false, err
			}
			retValue = retValue && isValid
			break
		}
	}
	return retValue, nil
}

func ValidateInputsWithSpec(stub shim.ChaincodeStubInterface, specification []byte, request string) (bool, error) {
	var requestMap map[string]interface{}
	err := json.Unmarshal([]byte(request), &requestMap)
	if err != nil {
		fmt.Printf("REQ PARSE ERR")
		return false, err
	}
	var specMap map[string]interface{}
	err = json.Unmarshal(specification, &specMap)
	if err != nil {
		fmt.Printf("SPEC PARSE ERR")
		return false, err
	}
	return validateInputs(stub, specMap, requestMap)
}
