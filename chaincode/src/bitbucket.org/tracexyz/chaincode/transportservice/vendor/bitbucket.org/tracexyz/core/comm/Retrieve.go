package comm

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"fmt"
	"encoding/json"
)

func ReadStorageItemDirect(stub shim.ChaincodeStubInterface, itemID string) (datastructs.StorageItem, error) {
	args := toChaincodeArgs(datastructs.RetrieveDirect)
	args = append(args, []byte(itemID))
	fmt.Printf("ARGS %v \n", args)
	response := stub.InvokeChaincode("storage_cc", args, "")
	if response.Status == shim.OK {
		fmt.Printf("Storage Response Payload %v \n", string(response.Payload))
		var directRetrieveResponse datastructs.DirectRetrieveResponse
		err := json.Unmarshal(response.Payload, &directRetrieveResponse)
		if err != nil {
			fmt.Printf("Storage Response Err %v \n", err)
			return datastructs.StorageItem{}, err
		}
		if directRetrieveResponse.Status == "Fail" {
			return datastructs.StorageItem{}, fmt.Errorf(directRetrieveResponse.Message)
		}
		return directRetrieveResponse.StorageItem, nil
	} else {
		return datastructs.StorageItem{}, fmt.Errorf(response.Message)
	}
}

func RetrieveWithRelationships(stub shim.ChaincodeStubInterface, request datastructs.AssetRetrieveRequest) (datastructs.AssetRetrievalResponse, error) {
	requestBytes, err := json.Marshal(request)
	var directRetrieveResponse datastructs.AssetRetrievalResponse
	if err != nil {
		return directRetrieveResponse, fmt.Errorf(err.Error())
	}
	args := toChaincodeArgs(datastructs.Retrieve)
	args = append(args, requestBytes)
	response := stub.InvokeChaincode("storage_cc", args, "")
	if response.Status == shim.OK {
		err := json.Unmarshal(response.Payload, &directRetrieveResponse)
		if err != nil {
			return datastructs.AssetRetrievalResponse{}, err
		}
		if directRetrieveResponse.Status == "Fail" {
			return datastructs.AssetRetrievalResponse{}, fmt.Errorf(directRetrieveResponse.Message)
		}
		return directRetrieveResponse, nil
	} else {
		return datastructs.AssetRetrievalResponse{}, fmt.Errorf(response.Message)
	}
}
