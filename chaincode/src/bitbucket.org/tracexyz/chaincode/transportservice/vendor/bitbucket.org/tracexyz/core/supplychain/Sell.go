package supplychain

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/comm"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/supplychain/validation"
	"encoding/json"
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
	"fmt"
)

// Core chaincode for selling an item
// When an item is sold, following will happen
// A new item and a new node will be created with the sold quantity
// The sold quantity will be deducted from the source of input
//
func SellRawMaterial(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var requestObj datastructs.SellingRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Printf("Selling request in library: %v \n", request)
	// verify signatures
	isValid, err := tutil.Verify([]byte(requestObj.SaleAgreement.BuyerPublicKey),
		requestObj.SaleAgreement.Item, requestObj.SaleAgreement.BuyerSignatureForSoldQty)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !isValid {
		return shim.Error("signature of the buyer for this item is not valid")
	}
	isValid, err = tutil.Verify([]byte(requestObj.SaleAgreement.SellerPublicKey),
		requestObj.SaleAgreement.Item, requestObj.SaleAgreement.SellerSignatureForSoldQty)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !isValid {
		return shim.Error("signature of the buyer for this item is not valid")
	}

	// Read the specification
	specStorageItem, err := comm.ReadStorageItemDirect(stub, requestObj.SpecificationID)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Printf("SPEC OK\n")

	// Validate the selling request
	success, err := validation.ValidateInputsWithSpec(stub, specStorageItem.PAYLOAD, request)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !success {
		return shim.Error("the request fails to comply with the specification ID:" + requestObj.SpecificationID)
	}
	fmt.Printf("VALIDATION OK\n")

	// Generate IDs for new item and process
	generatedProcessID := util.GenerateUUID()
	generatedItemID := util.GenerateUUID()

	// Generate item nodes
	tempItemNode := graph.NewNode(generatedItemID, "Item")
	processNode := graph.NewNode(generatedProcessID, "SellProcess")

	// Generate update request for source item
	var update datastructs.StorageUpdate
	update.UpdateType = datastructs.Subtract
	update.ItemID = requestObj.SellingItem.ItemID
	var soldQtyMap map[string]interface{}
	err = json.Unmarshal(requestObj.SaleAgreement.Item, &soldQtyMap)
	if err != nil {
		return shim.Error(err.Error())
	}
	sellingQty, ok := soldQtyMap["qty"].(float64)
	if !ok {
		return shim.Error("sold quantity must have 'qty' field defined")
	}
	update.UpdateValue = sellingQty
	update.UpdatePath = "$.qty"
	update.UpdateAuthentication.ChallengeResponse = requestObj.SellingItem.InputChallengeResponse
	update.UpdateAuthentication.ItemID = requestObj.SellingItem.ItemID

	// Generate Relationships
	tempProcessToItemRelationship := graph.NewEdge(generatedProcessID, generatedItemID, relationships.PublicTrace, states.Active)

	specRelationship := graph.NewEdge(requestObj.SpecificationID, generatedProcessID, relationships.Specification, states.Active)
	itemToProcessRelationship := graph.NewEdge(requestObj.SellingItem.ItemID, generatedProcessID, relationships.PublicTrace, states.Active)
	var itemToProcessRelationshipUpdate datastructs.EdgeUpdate
	itemToProcessRelationshipUpdate.Edge = itemToProcessRelationship
	itemToProcessRelationshipUpdate.UpdateAuthentication.ChallengeResponse = requestObj.SellingRelationship.InputChallengeResponse

	fmt.Printf("REQ OK\n")

	requestObj.SellingProcess.ID = generatedProcessID
	requestObj.SoldQuantity.ID = generatedItemID

	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = []datastructs.Node{tempItemNode, processNode}
	storageRequest.Edges = []datastructs.Edge{tempProcessToItemRelationship}
	storageRequest.SourceToItemEdges = []datastructs.EdgeUpdate{itemToProcessRelationshipUpdate}
	storageRequest.Items = []datastructs.StorageItem{requestObj.SoldQuantity}
	storageRequest.Updates = []datastructs.StorageUpdate{update}
	storageRequest.Process.ProcessData = requestObj.SellingProcess
	storageRequest.Process.ProcessSpecRelationship = specRelationship

	fmt.Printf("Store TXN START")
	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	fmt.Printf("Store TXN END \n")
	if err != nil {
		fmt.Printf("Store TXN Err %v\n", err)
		return shim.Error(err.Error())
	}

	var response datastructs.SellItemResponse
	response.StorageNetworkResponse = storageNWResponse
	response.SellingItemID = requestObj.SellingItem.ItemID
	response.SellingQuantity = sellingQty
	response.SellingProcessID = generatedProcessID
	response.SellingQuantityKey = generatedItemID
	response.SupplyChainResponse.ResponseType = "sell_raw"
	response.SupplyChainResponse.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(responseBytes)
}
