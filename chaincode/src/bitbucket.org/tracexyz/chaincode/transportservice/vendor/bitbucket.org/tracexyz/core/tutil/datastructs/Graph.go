package datastructs

type Node struct {
	ItemID   string `json:"id"`
	ItemType string `json:"type"`
	Enabled  bool   `json:"enabled"`
}

type Edge struct {
	RelationshipType  string `json:"type"`
	RelationshipState string `json:"state"`
	SourceItemID      string `json:"source"`
	DestItemID        string `json:"destination"`
	Enabled           bool   `json:"enabled"`
}
