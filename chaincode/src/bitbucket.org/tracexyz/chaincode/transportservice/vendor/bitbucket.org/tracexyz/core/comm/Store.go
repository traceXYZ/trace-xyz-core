package comm

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
	"fmt"
)

func toChaincodeArgs(args ...string) [][]byte {
	bargs := make([][]byte, len(args))
	for i, arg := range args {
		bargs[i] = []byte(arg)
	}
	return bargs
}

func StoreTxn(stub shim.ChaincodeStubInterface, request datastructs.StoreTxnRequest) (datastructs.StoreTxnResponse, error) {
	var storageResponse datastructs.StoreTxnResponse
	requestBytes, err := json.Marshal(request)
	if err != nil {
		return storageResponse, fmt.Errorf(err.Error())
	}
	args := toChaincodeArgs(datastructs.StoreTxn)
	args = append(args, requestBytes)
	fmt.Printf("ARGS %v \n", args)
	response := stub.InvokeChaincode("storage_cc", args, "")
	if response.Status == shim.OK {
		err := json.Unmarshal(response.Payload, &storageResponse)
		fmt.Printf("RESP : %v \n", storageResponse)
		if err != nil {
			return storageResponse, fmt.Errorf(err.Error())
		}
		//storageResponse.TxnID = stub.GetTxID()
		return storageResponse, nil
	} else {
		return storageResponse, fmt.Errorf(response.Message)
	}
}

func AuthorityTransfer(stub shim.ChaincodeStubInterface, request datastructs.AuthorityTransferRequest) (datastructs.AuthorityUpdateResponse, error) {
	requestBytes, err := json.Marshal(request)
	var storageResponse datastructs.AuthorityUpdateResponse
	if err != nil {
		return storageResponse, fmt.Errorf(err.Error())
	}
	args := toChaincodeArgs(datastructs.AuthorityTransfer)
	args = append(args, requestBytes)
	fmt.Printf("ARGS %v \n", args)
	args = append(args, requestBytes)
	response := stub.InvokeChaincode("storage_cc", args, "")
	if response.Status == shim.OK {
		err := json.Unmarshal(response.Payload, &storageResponse)
		if err != nil {
			return storageResponse, fmt.Errorf(err.Error())
		}
		return storageResponse, nil
	} else {
		return storageResponse, fmt.Errorf(response.Message)
	}
}
