package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/supplychain"
	"bitbucket.org/tracexyz/core/tutil"
)

type CheeseProducer struct {
}

// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(CheeseProducer))
	if err != nil {
		fmt.Printf("Error starting TraceXZ chaincode - %s", err)
	}
}

// ============================================================================================================================
// Init - initialize the chaincode
//
// ============================================================================================================================
func (t *CheeseProducer) Init(stub shim.ChaincodeStubInterface) pb.Response {

	return shim.Success([]byte("Success"))
}

// ============================================================================================================================
// Invoke - Our entry point for Invocations
// MilkProducer Chaincode is used to perform following functions
// *Produce milk Item
// *Sell milk Item
// *Read a quota value
// ============================================================================================================================
func (t *CheeseProducer) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Printf("F %v \n", function)
	fmt.Printf("A %v \n", args)
	// Handle different functions
	switch function {
	case "init":
		return t.Init(stub)
	case "produce_milk":
		fmt.Printf("1%v", "PASS")
		if len(args) < 1 {
			return shim.Error(tutil.GenerateResponse("Fail",
				"minimum of 1 arguments required to execute this function"))
		}
		return t.ManufactureCheese(stub, args[0])
	case "sell_milk":
		if len(args) < 1 {
			return shim.Error(tutil.GenerateResponse("Fail",
				"minimum of 1 arguments required to execute this function"))
		}
		return t.SellCheese(stub, args[0])
	case "send_for_transport":
		if len(args) < 1 {
			return shim.Error(tutil.GenerateResponse("Fail",
				"minimum of 1 arguments required to execute this function"))
		}
		return t.SendForTransport(stub, args[0])
	}
	fmt.Println("Received unknown invoke function name - " + function)
	return shim.Error("Received unknown invoke function name - '" + function + "'")
}

// ============================================================================================================================
// Register - Smart Contract for Registering a User
// ============================================================================================================================
func (t *CheeseProducer) ManufactureCheese(stub shim.ChaincodeStubInterface, request string) pb.Response {
	return supplychain.CreateItem(stub, request)
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *CheeseProducer) SellCheese(stub shim.ChaincodeStubInterface, request string) pb.Response {
	return supplychain.OwnershipTransfer(stub, request)
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *CheeseProducer) SendForTransport(stub shim.ChaincodeStubInterface, request string) pb.Response {
	return supplychain.HandlingRightsTransfer(stub, request)
}
