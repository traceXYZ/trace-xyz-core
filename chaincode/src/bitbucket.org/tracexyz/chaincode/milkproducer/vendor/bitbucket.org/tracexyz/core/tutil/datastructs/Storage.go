package datastructs

import (
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
)

type UpdateType int

const (
	Add      UpdateType = 1
	Subtract UpdateType = 2
	Exact    UpdateType = 3
)

type User struct {
	Id        string `json:"id"`
	Enabled   bool   `json:"enabled"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	EmailAddr string `json:"email_addr"`
	NIC       string `json:"nic"`
}

type StorageItem struct {
	ID               string             `json:"id"`
	Ownership        Authority          `json:"ownership"`
	Handlers         []HandlerAuthority `json:"handlers"`
	HandlerChallenge string             `json:"handler_challenge"`
	HandlerPublicKey interface{}        `json:"handler_public_key"`
	HandlerIdentity  string             `json:"handler_identity"`
	PAYLOAD          []byte             `json:"payload"`
}

type StoredItemToken struct {
	ItemID              string `json:"item_id"`
	NextUpdateChallenge string `json:"next_update_challenge"`
}

type HandlerAuthority struct {
	AuthorityPublicKey string `json:"public_key"`
	PayloadSignature   []byte `json:"payload_signature"`
}

type Authority struct {
	AuthorityPublicKey string `json:"public_key"`
	PayloadSignature   []byte `json:"payload_signature"`
	NextChallenge      string `json:"challenge"`
}

type StorageUpdate struct {
	UpdateAuthentication UpdateAuthentication `json:"update_authentication"`
	ItemID               string               `json:"item_id"`
	UpdatePath           string               `json:"update_path"`
	UpdateType           UpdateType           `json:"update_type"`
	UpdateValue          float64              `json:"update_value"`
}

type AuthorityTransferRequest struct {
	OwnershipToken InputToken
	AuthorityType  int       `json:"authority_type"`
	Authority      Authority `json:"authority"`
}

type StoreTxnRequest struct {
	Nodes             []Node          `json:"nodes"`
	Edges             []Edge          `json:"edges"`
	SourceToItemEdges []EdgeUpdate    `json:"source_to_item_edges"`
	ItemToSourceEdges []EdgeUpdate    `json:"item_to_source_edges"`
	Items             []StorageItem   `json:"items"`
	Updates           []StorageUpdate `json:"updates"`
	Process           Process         `json:"process"`
}

type UpdateAuthentication struct {
	ItemID            string `json:"item_id"`
	ChallengeResponse []byte `json:"challenge_response"`
}

type Process struct {
	ProcessData             StorageItem `json:"process"`
	ProcessSpecRelationship Edge        `json:"process_spec"`
}

type EdgeUpdate struct {
	UpdateAuthentication UpdateAuthentication `json:"update_authentication"`
	Edge                 Edge                 `json:"edge"`
}

type RetrievalType string

const (
	ImmediateHistory RetrievalType = "ImmediateHistory"
	AllHistory       RetrievalType = "AllHistory"
	ImmediateFuture  RetrievalType = "ImmediateFuture"
	AllFuture        RetrievalType = "AllFuture"
	Immediate        RetrievalType = "Immediate"
	All              RetrievalType = "All"
	CustomWalk       RetrievalType = "CustomWalk"
)

type AssetRetrieveRequest struct {
	ItemID                 string                                `json:"item_id"`
	RelationshipType       relationships.GraphRelationshipType   `json:"relationship_type"`
	RetrieveType           RetrievalType                         `json:"retrieve_type"`
	WalkPath               []relationships.GraphRelationshipType `json:"walk_path"`
	CustWalkIncludeFuture  bool                                  `json:"cust_walk_include_future"`
	CustWalkIncludeHistory bool                                  `json:"cust_walk_include_history"`
}
