package validation

import (
	"reflect"
	"github.com/oliveagle/jsonpath"
	"fmt"
)

func processSpecCompare(checkingSpec, checkingValue interface{}) (bool, error) {
	checkingSpecList, ok := checkingSpec.([]interface{})
	if !ok {
		return false, nil
	}
	var retValue = true
	for _, specItemIter := range checkingSpecList {
		specItem, ok := specItemIter.(map[string]interface{})
		if !ok {
			return false, nil
		}
		var eValue = specItem["val"]
		var operation = specItem["op"]
		var fieldPath = specItem["field_path"]
		var payloadPath = specItem["payload_path"]
		if fieldPath == nil || operation == nil || eValue == nil || payloadPath == nil {
			return false, nil
		}
		fieldPathS, ok := fieldPath.(string)
		if !ok {
			return false, nil
		}
		operationS, ok := operation.(string)
		if !ok {
			return false, nil
		}
		checkingItemMap, err := extractPayloadMap(checkingValue, payloadPath)
		if err != nil {
			return false, err
		}
		rValue, err := jsonpath.JsonPathLookup(checkingItemMap, fieldPathS)
		if err != nil {
			fmt.Printf("ERR JSN : %v \n", err)
			return false, err
		}
		if reflect.TypeOf(rValue) == reflect.TypeOf([]interface{}{}) {
			retValue = retValue && validateFieldFiltered(operationS, eValue, rValue)
		} else {
			retValue = retValue && validateField(operationS, eValue, rValue)
		}
	}
	return retValue, nil
}
