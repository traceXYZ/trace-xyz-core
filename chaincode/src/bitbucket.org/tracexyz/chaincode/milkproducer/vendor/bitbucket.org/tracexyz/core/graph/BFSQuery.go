package graph

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"fmt"
	"github.com/golang-collections/collections/queue"
)

func GetSourceNodesBFS(stub shim.ChaincodeStubInterface, itemID string, relationshipType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	var nodesInPath []datastructs.Node
	var edgesInPath []datastructs.Edge
	var visitedMap = make(map[string]bool)
	queueObj := queue.New()

	rootNode, isAvailable, err := GetNode(stub, itemID)

	if !isAvailable {
		return nil, nil, fmt.Errorf("cannot find the root node")
	}
	if err != nil {
		return nil, nil, err
	}
	queueObj.Enqueue(rootNode)
	for queueObj.Len() > 0 {
		currentNode := queueObj.Dequeue().(datastructs.Node)
		if visitedMap[currentNode.ItemID] {
			continue
		}
		nodesInPath = append(nodesInPath, currentNode)

		nodesD, edgesD, err := GetSourcesList(stub, itemID, relationshipType)
		if err != nil {
			return nil, nil, err
		}

		edgesInPath = append(edgesInPath, edgesD...)

		for _, node := range nodesD {
			if visitedMap[node.ItemID] {
				continue
			}
			queueObj.Enqueue(node)
		}
		visitedMap[currentNode.ItemID] = true
	}
	return nodesInPath, edgesInPath, nil
}

func GetDestNodesBFS(stub shim.ChaincodeStubInterface, itemID string, relationshipType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	var nodesInPath []datastructs.Node
	var edgesInPath []datastructs.Edge
	var visitedMap = make(map[string]bool)
	queueObj := queue.New()

	rootNode, isAvailable, err := GetNode(stub, itemID)

	if !isAvailable {
		return nil, nil, fmt.Errorf("cannot find the root node")
	}
	if err != nil {
		return nil, nil, err
	}
	queueObj.Enqueue(rootNode)
	for queueObj.Len() > 0 {
		currentNode := queueObj.Dequeue().(datastructs.Node)
		if visitedMap[currentNode.ItemID] {
			continue
		}
		nodesInPath = append(nodesInPath, currentNode)

		nodesD, edgesD, err := GetDestinationsList(stub, itemID, relationshipType)
		if err != nil {
			return nil, nil, err
		}

		edgesInPath = append(edgesInPath, edgesD...)

		for _, node := range nodesD {
			if visitedMap[node.ItemID] {
				continue
			}
			queueObj.Enqueue(node)
		}
		visitedMap[currentNode.ItemID] = true
	}
	return nodesInPath, edgesInPath, nil
}
