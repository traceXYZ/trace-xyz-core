package supplychain

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
	"bitbucket.org/tracexyz/core/comm"
	"bitbucket.org/tracexyz/core/supplychain/validation"
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
)

func ManufactureItem(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var requestObj datastructs.ManufactureItemRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Read the spec from storage network
	specStorageItem, err := comm.ReadStorageItemDirect(stub, requestObj.SpecificationID)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Validate the spec
	success, err := validation.ValidateInputsWithSpec(stub, specStorageItem.PAYLOAD, request)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !success {
		return shim.Error("the request fails to comply with the specification ID:" + requestObj.SpecificationID)
	}
	// Update the quota
	// todo: get the quota update token

	//Create the items for storage

	generatedProcessID := util.GenerateUUID()
	generatedItemID := util.GenerateUUID()

	itemNode := graph.NewNode(generatedItemID, "Item")
	processNode := graph.NewNode(generatedProcessID, "ManufactureProcess")

	processItemRelationship := graph.NewEdge(generatedProcessID, generatedItemID, relationships.PublicTrace, states.Active)

	specRelationship := graph.NewEdge(requestObj.SpecificationID, generatedProcessID, relationships.Specification, states.Active)

	var inputRelationshipUpdates []datastructs.EdgeUpdate
	var inputItemUpdates []datastructs.StorageUpdate
	for _, inputItem := range requestObj.InputItems {
		// Create storage item update request
		var update datastructs.StorageUpdate
		update.UpdateType = datastructs.Subtract
		update.ItemID = inputItem.ItemID
		update.UpdateValue = inputItem.UsedQty
		update.UpdatePath = "$.qty"
		update.UpdateAuthentication.ChallengeResponse = inputItem.InputToken.InputChallengeResponse
		inputItemUpdates = append(inputItemUpdates, update)

	}
	for _, inputItem := range requestObj.InputRelationships {
		// Create input item to process relationship
		tempRelationship := graph.NewEdge(inputItem.ItemID, generatedProcessID, relationships.PublicTrace, states.Active)
		var tempRelationshipUpdate datastructs.EdgeUpdate
		tempRelationshipUpdate.Edge = tempRelationship
		tempRelationshipUpdate.UpdateAuthentication.ChallengeResponse = inputItem.InputChallengeResponse
		inputRelationshipUpdates = append(inputRelationshipUpdates, tempRelationshipUpdate)
	}

	requestObj.ManufacturingProcess.ID = generatedProcessID
	requestObj.ManufacturedItem.ID = generatedItemID

	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = []datastructs.Node{itemNode, processNode}
	storageRequest.Edges = []datastructs.Edge{processItemRelationship}
	storageRequest.SourceToItemEdges = inputRelationshipUpdates
	storageRequest.Items = []datastructs.StorageItem{requestObj.ManufacturedItem}
	storageRequest.Updates = inputItemUpdates
	storageRequest.Process.ProcessData = requestObj.ManufacturingProcess
	storageRequest.Process.ProcessSpecRelationship = specRelationship

	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	// todo: commit the creation item

	var response datastructs.ManufactureItemResponse
	response.StorageNetworkResponse = storageNWResponse
	response.ManufacturedItemID = generatedItemID
	response.ProcessID = generatedProcessID
	response.ProcessSpecID = requestObj.SpecificationID
	response.Inputs = requestObj.InputItems
	response.SupplyChainResponse.ResponseType = "manufacture"
	response.SupplyChainResponse.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(responseBytes)
}
