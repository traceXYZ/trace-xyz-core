package graph

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
)

func GetSourceNodesRecursive(stub shim.ChaincodeStubInterface, itemID string, relationshipType relationships.GraphRelationshipType, maxDepth int, visitedMap map[string]bool) ([]datastructs.Node, []datastructs.Edge, error) {
	nodes, edges, err := GetSourcesList(stub, itemID, relationshipType)
	fmt.Printf("\n SNodes: %v \n R:%v \n", nodes, maxDepth)
	if err != nil {
		return nil, nil, fmt.Errorf(err.Error())
	}
	if nodes == nil || maxDepth < 0 {
		return nodes, edges, nil
	} else {
		for _, node := range nodes {
			if visitedMap[node.ItemID] {
				continue
			}
			tempNodes, tempEdges, err := GetSourceNodesRecursive(stub, node.ItemID, relationshipType, maxDepth-1, visitedMap)
			if err != nil {
				return nil, nil, fmt.Errorf(err.Error())
			}
			nodes = append(nodes, tempNodes...)
			edges = append(edges, tempEdges...)
			visitedMap[node.ItemID] = true
		}
		return nodes, edges, nil
	}
}

func GetDestNodesRecursive(stub shim.ChaincodeStubInterface, itemID string, relationshipType relationships.GraphRelationshipType, maxDepth int, visitedMap map[string]bool) ([]datastructs.Node, []datastructs.Edge, error) {
	var nodes []datastructs.Node
	nodes, edges, err := GetDestinationsList(stub, itemID, relationshipType)
	fmt.Printf("\n DNodes%v \n R:%v \n", nodes, maxDepth)
	if err != nil {
		return nil, nil, fmt.Errorf(err.Error())
	}
	if nodes == nil || maxDepth < 0 {
		return nodes, edges, nil
	} else {
		for _, node := range nodes {
			if visitedMap[node.ItemID] {
				continue
			}
			var tempNodes []datastructs.Node
			tempNodes, tempEdges, err := GetDestNodesRecursive(stub, node.ItemID, relationshipType, maxDepth-1, visitedMap)
			if err != nil {
				return nil, nil, fmt.Errorf(err.Error())
			}
			nodes = append(nodes, tempNodes...)
			edges = append(edges, tempEdges...)
			visitedMap[node.ItemID] = true

		}
		return nodes, edges, nil
	}
}
