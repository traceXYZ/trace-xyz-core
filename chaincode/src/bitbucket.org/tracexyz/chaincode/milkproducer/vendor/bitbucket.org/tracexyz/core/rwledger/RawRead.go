package rwledger

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"encoding/json"
	"fmt"
)

func Read(stub shim.ChaincodeStubInterface, id string, obj interface{}) (bool, error) {
	var err, errJson error
	objByteArr, err := stub.GetState(id)
	if err != nil || objByteArr == nil {
		return false, err
	}
	fmt.Printf("\nRead Item: %v \n", string(objByteArr))
	errJson = json.Unmarshal(objByteArr, &obj)
	if errJson != nil {
		return false, errJson
	}
	return true, nil
}

func ReadHistory(stub shim.ChaincodeStubInterface, id string) ([]interface{}, error) {
	var err error
	queryIterator, err := stub.GetHistoryForKey(id)
	if err != nil {
		return nil, err
	}
	var history []interface{}
	for queryIterator.HasNext() {
		historyItem, err := queryIterator.Next()
		if err != nil {
			return nil, err
		}
		history = append(history, historyItem)
	}
	return history, nil
}

func KeyExists(stub shim.ChaincodeStubInterface, id string) (bool, error) {
	objByteArr, err := stub.GetState(id)
	if err != nil {
		return false, err
	}
	if objByteArr != nil {
		return true, nil
	}
	return false, nil
}

func IfKeyExistsReturnValue(stub shim.ChaincodeStubInterface, id string) ([]byte, error) {
	objByteArr, err := stub.GetState(id)
	if err != nil {
		return nil, err
	}
	if objByteArr != nil {
		return objByteArr, nil
	}
	return nil, nil
}

func ReadWithPartialKey(stub shim.ChaincodeStubInterface, objectType string, id [] string) (shim.StateQueryIteratorInterface, error) {
	var err error
	objArr, err := stub.GetStateByPartialCompositeKey(objectType, id)
	if err != nil {
		return nil, err
	}
	return objArr, nil
}

func ReadAllWithoutKey(stub shim.ChaincodeStubInterface, objectType string) (shim.StateQueryIteratorInterface, error) {
	var err error
	objArr, err := stub.GetStateByRange("", "")
	if err != nil {
		return nil, err
	}
	return objArr, nil
}
