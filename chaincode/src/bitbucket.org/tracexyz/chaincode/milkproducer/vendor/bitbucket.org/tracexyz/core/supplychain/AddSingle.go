package supplychain

import (
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/comm"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"encoding/json"
	"github.com/hyperledger/fabric/protos/peer"
	"fmt"
)

func AddSingleItem(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var item datastructs.StorageItem
	err := json.Unmarshal([]byte(request), &item)
	if err != nil {
		return shim.Error(err.Error())
	}

	generatedItemID := util.GenerateUUID()

	itemNode := graph.NewNode(generatedItemID, "Specification")
	item.ID = generatedItemID

	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = []datastructs.Node{itemNode}
	storageRequest.Items = []datastructs.StorageItem{item}

	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	if err != nil {
		return shim.Error(err.Error())
	}

	responseBytes, err := json.Marshal(storageNWResponse)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Printf("PASSSSSSSSSSSSSSSSS \n")
	return shim.Success(responseBytes)
}
