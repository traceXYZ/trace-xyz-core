/*
TraceXYZ MilkProducer Chaincode
*/

package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/supplychain"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
)

type MilkProducer struct {
}

var milkProductionSpecID = "MPS001"
var milkSellingSpecID = "MSS001"

// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(MilkProducer))
	if err != nil {
		fmt.Printf("Error starting TraceXZ chaincode - %s", err)
	}
}

// ============================================================================================================================
// Init - initialize the chaincode
//
// ============================================================================================================================
func (t *MilkProducer) Init(stub shim.ChaincodeStubInterface) pb.Response {
	_, args := stub.GetFunctionAndParameters()
	if len(args) < 2 {
		return shim.Error(tutil.GenerateResponse("Fail", "not enough arguments to init chaincode"))
	}
	if args[0] == "" || args[1] == "" {
		return shim.Error(tutil.GenerateResponse("Fail", "milk producer default specIDs must be specified in order to initialize chaincode"))
	}
	milkProductionSpecID = args[0]
	milkSellingSpecID = args[1]
	return shim.Success([]byte("Success"))
}

// ============================================================================================================================
// Invoke - Our entry point for Invocations
// MilkProducer Chaincode is used to perform following functions
// *Produce milk Item
// *Sell milk Item
// *Read a quota value
// ============================================================================================================================
func (t *MilkProducer) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Printf("F %v \n", function)
	fmt.Printf("A %v \n", args)
	// Handle different functions
	if len(args) < 1 {
		return shim.Error(tutil.GenerateResponse("Fail",
			"minimum of 1 arguments required to execute this function"))
	}
	switch function {
	case "init":
		return t.Init(stub)
	case "produce_milk":
		fmt.Printf("1%v", "PASS")
		return t.ProduceMilk(stub, args[0])
	case "sell_milk":
		return t.SellMilk(stub, args[0])
	}
	fmt.Println("Received unknown invoke function name - " + function)
	return shim.Error("Received unknown invoke function name - '" + function + "'")
}

type MilkProductionRequest struct {
	MilkProductionSpecID           string `json:"milk_production_spec_id"`
	MilkProducerPublicKey          string `json:"milk_producer_public_key"`
	MilkItem                       []byte `json:"milk_item"`
	MilkItemSignature              []byte `json:"milk_item_signature"`
	MilkProductionProcess          []byte `json:"milk_production_process"`
	MilkProductionProcessSignature []byte `json:"milk_production_process_signature"`
}

// ============================================================================================================================
// Register - Smart Contract for Registering a User
// ============================================================================================================================
func (t *MilkProducer) ProduceMilk(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var requestObj MilkProductionRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		fmt.Printf("\n ERR %v \n", err)
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var createItemRequest datastructs.CreateItemRequest
	if requestObj.MilkProductionSpecID != "" {
		createItemRequest.SpecificationID = requestObj.MilkProductionSpecID
	} else {
		createItemRequest.SpecificationID = milkProductionSpecID
	}
	createItemRequest.CreatedItem.Ownership.AuthorityPublicKey = requestObj.MilkProducerPublicKey
	createItemRequest.CreatedItem.Ownership.PayloadSignature = requestObj.MilkItemSignature
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.MilkProducerPublicKey
	handlerAuthority.PayloadSignature = requestObj.MilkItemSignature
	createItemRequest.CreatedItem.Handlers = append(createItemRequest.CreatedItem.Handlers, handlerAuthority)

	fmt.Printf("\n MI SIGN %v \n", requestObj.MilkItemSignature)
	createItemRequest.CreatedItem.PAYLOAD = requestObj.MilkItem
	fmt.Printf("\n MILK ITEM SIGN %v \n", requestObj.MilkItem)
	createItemRequest.CreationProcess.Ownership.AuthorityPublicKey = requestObj.MilkProducerPublicKey
	createItemRequest.CreationProcess.Ownership.PayloadSignature = requestObj.MilkProductionProcessSignature

	var processHandlerAuthority datastructs.HandlerAuthority
	processHandlerAuthority.AuthorityPublicKey = requestObj.MilkProducerPublicKey
	processHandlerAuthority.PayloadSignature = requestObj.MilkProductionProcessSignature
	createItemRequest.CreationProcess.Handlers = append(createItemRequest.CreationProcess.Handlers, processHandlerAuthority)
	createItemRequest.CreationProcess.PAYLOAD = requestObj.MilkProductionProcess
	fmt.Printf("\n MPP SIGN %v \n", requestObj.MilkProductionProcessSignature)
	createItemRequestBytes, err := json.Marshal(createItemRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.CreateItem(stub, string(createItemRequestBytes))
}

type MilkSellingRequest struct {
	MilkSellingSpecID           string                     `json:"milk_selling_spec_id"`
	SellingSourceOwnershipToken datastructs.InputItemToken `json:"selling_source_ownership_token"`
	SellingSourceHandlingToken  datastructs.InputToken     `json:"selling_source_handling_token"`
	SellingQuantity             []byte                     `json:"selling_quantity_object"`
	MilkSellerPublicKey         string                     `json:"milk_seller_public_key"`
	SellerSignatureForQty       []byte                     `json:"seller_signature_for_qty"`
	MilkBuyerPublicKey          string                     `json:"milk_buyer_public_key"`
	BuyerSignatureForQty        []byte                     `json:"buyer_signature_for_qty"`
	MilkSellingProcess          []byte                     `json:"milk_selling_process"`
	MilkSellingProcessSignature []byte                     `json:"milk_selling_process_signature"`
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *MilkProducer) SellMilk(stub shim.ChaincodeStubInterface, request string) pb.Response {
	fmt.Printf("\n Milk Selling request :%v", string(request))
	var requestObj MilkSellingRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var sellingRequest datastructs.SellingRequest
	if requestObj.MilkSellingSpecID != "" {
		sellingRequest.SpecificationID = requestObj.MilkSellingSpecID
	} else {
		sellingRequest.SpecificationID = milkSellingSpecID
	}
	sellingRequest.SellingItem = requestObj.SellingSourceOwnershipToken
	sellingRequest.SellingRelationship = requestObj.SellingSourceHandlingToken
	sellingRequest.SaleAgreement.SellerPublicKey = requestObj.MilkSellerPublicKey
	sellingRequest.SaleAgreement.BuyerPublicKey = requestObj.MilkBuyerPublicKey
	sellingRequest.SaleAgreement.SellerSignatureForSoldQty = requestObj.SellerSignatureForQty
	sellingRequest.SaleAgreement.BuyerSignatureForSoldQty = requestObj.BuyerSignatureForQty
	sellingRequest.SaleAgreement.Item = requestObj.SellingQuantity

	sellingRequest.SoldQuantity.Ownership.AuthorityPublicKey = requestObj.MilkBuyerPublicKey
	sellingRequest.SoldQuantity.Ownership.PayloadSignature = requestObj.BuyerSignatureForQty
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.MilkBuyerPublicKey
	handlerAuthority.PayloadSignature = requestObj.BuyerSignatureForQty
	sellingRequest.SoldQuantity.Handlers = append(sellingRequest.SoldQuantity.Handlers, handlerAuthority)
	sellingRequest.SoldQuantity.PAYLOAD = requestObj.SellingQuantity

	sellingRequest.SellingProcess.Ownership.AuthorityPublicKey = requestObj.MilkSellerPublicKey
	sellingRequest.SellingProcess.Ownership.PayloadSignature = requestObj.MilkSellingProcessSignature
	var processHandlerAuthority datastructs.HandlerAuthority
	processHandlerAuthority.AuthorityPublicKey = requestObj.MilkSellerPublicKey
	processHandlerAuthority.PayloadSignature = requestObj.MilkSellingProcessSignature
	sellingRequest.SellingProcess.Handlers = append(sellingRequest.SellingProcess.Handlers, processHandlerAuthority)
	sellingRequest.SellingProcess.PAYLOAD = requestObj.MilkSellingProcess

	sellingRequestBytes, err := json.Marshal(sellingRequest)
	fmt.Printf("\n Selling request :%v", string(sellingRequestBytes))
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.SellRawMaterial(stub, string(sellingRequestBytes))
}
