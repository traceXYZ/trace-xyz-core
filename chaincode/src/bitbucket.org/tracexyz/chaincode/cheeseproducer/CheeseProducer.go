package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/supplychain"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
)

type CheeseProducer struct {
}

// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(CheeseProducer))
	if err != nil {
		fmt.Printf("Error starting TraceXZ chaincode - %s", err)
	}
}

var cheeseManufacturingSpecID = ""
var cheeseSellingSpecID = ""
var cheeseTransportSpecID = ""
var transportSpecID = ""
var cheesePackagingSpecID = ""
var cheeseBoxingSpecID = ""

// ============================================================================================================================
// Init - initialize the chaincode
//
// ============================================================================================================================
func (t *CheeseProducer) Init(stub shim.ChaincodeStubInterface) pb.Response {
	_, args := stub.GetFunctionAndParameters()
	if len(args) < 6 {
		return shim.Error(tutil.GenerateResponse("Fail", "not enough arguments to init chaincode"))
	}
	if args[0] == "" || args[1] == "" || args[2] == "" || args[3] == "" {
		return shim.Error(tutil.GenerateResponse("Fail", "cheese manufacturer default specIDs must be specified in order to initialize chaincode"))
	}
	cheeseManufacturingSpecID = args[0]
	cheeseSellingSpecID = args[1]
	cheeseTransportSpecID = args[2]
	transportSpecID = args[3]
	cheesePackagingSpecID = args[4]
	cheeseBoxingSpecID = args[5]
	return shim.Success([]byte("Success"))
}

// ============================================================================================================================
// Invoke - Our entry point for Invocations
// MilkProducer Chaincode is used to perform following functions
// *Produce milk Item
// *Sell milk Item
// *Read a quota value
// ============================================================================================================================
func (t *CheeseProducer) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Printf("F %v \n", function)
	fmt.Printf("A %v \n", args)
	// Handle different functions
	if len(args) < 1 {
		return shim.Error(tutil.GenerateResponse("Fail",
			"minimum of 1 arguments required to execute this function"))
	}
	switch function {
	case "init":
		return t.Init(stub)
	case "produce_cheese":
		return t.ManufactureCheese(stub, args[0])
	case "transfer_item":
		return t.TransferItem(stub, args[0])
	case "send_for_transport":
		return t.SendForTransport(stub, args[0])
	case "item_transport":
		return t.ItemTransport(stub, args[0])
	case "add_process_update":
		return t.AddProcessUpdate(stub, args[0])
	case "cut_and_package_cheese":
		return t.SplitCheese(stub, args[0])
	case "box_cheese":
		return t.BoxCheese(stub, args[0])
	case "unbox_container":
		return t.UnboxContainer(stub, args[0])
	case "iot_update":
		return t.AddProcessUpdate(stub, args[0])
	}
	fmt.Println("Received unknown invoke function name - " + function)
	return shim.Error("Received unknown invoke function name - '" + function + "'")
}

type CheeseManufacturingRequest struct {
	ManufacturingSpecID         string                       `json:"manufacturing_spec_id"`
	InputOwnershipTokens        []datastructs.InputItemToken `json:"input_ownership_tokens"`
	InputHandlingTokens         []datastructs.InputToken     `json:"input_handling_tokens"`
	OwnerPublicKey              string                       `json:"owner_public_key"`
	ManufacturingProcess        []byte                       `json:"manufacturing_process"`
	ManufactureProcessSignature []byte                       `json:"manufacture_process_signature"`
	CheeseItem                  []byte                       `json:"cheese_item"`
	CheeseItemSignature         []byte                       `json:"cheese_item_signature"`
}

// ============================================================================================================================
// Register - Smart Contract for Registering a User
// ============================================================================================================================
func (t *CheeseProducer) ManufactureCheese(stub shim.ChaincodeStubInterface, request string) pb.Response {
	fmt.Printf("Request :%v \n", string(request))
	var requestObj CheeseManufacturingRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}

	var manufactureRequest datastructs.ManufactureItemRequest
	manufactureRequest.InputItems = requestObj.InputOwnershipTokens
	manufactureRequest.InputRelationships = requestObj.InputHandlingTokens
	if requestObj.ManufacturingSpecID != "" {
		manufactureRequest.SpecificationID = requestObj.ManufacturingSpecID
	} else {
		manufactureRequest.SpecificationID = cheeseManufacturingSpecID
	}
	manufactureRequest.ManufacturedItem.Ownership.AuthorityPublicKey = requestObj.OwnerPublicKey
	manufactureRequest.ManufacturedItem.Ownership.PayloadSignature = requestObj.CheeseItemSignature
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.OwnerPublicKey
	handlerAuthority.PayloadSignature = requestObj.CheeseItemSignature
	manufactureRequest.ManufacturedItem.Handlers = append(manufactureRequest.ManufacturedItem.Handlers, handlerAuthority)
	manufactureRequest.ManufacturedItem.PAYLOAD = requestObj.CheeseItem

	manufactureRequest.ManufacturingProcess.Ownership.AuthorityPublicKey = requestObj.OwnerPublicKey
	manufactureRequest.ManufacturingProcess.Ownership.PayloadSignature = requestObj.ManufactureProcessSignature
	var processHandlerAuthority datastructs.HandlerAuthority
	processHandlerAuthority.AuthorityPublicKey = requestObj.OwnerPublicKey
	processHandlerAuthority.PayloadSignature = requestObj.ManufactureProcessSignature
	manufactureRequest.ManufacturingProcess.Handlers = append(manufactureRequest.ManufacturingProcess.Handlers, processHandlerAuthority)
	manufactureRequest.ManufacturingProcess.PAYLOAD = requestObj.ManufacturingProcess

	manufactureRequestBytes, err := json.Marshal(manufactureRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.ManufactureItem(stub, string(manufactureRequestBytes))
}

type TransferRequest struct {
	SpecID                   string                 `json:"spec_id"`
	OwnershipToken           datastructs.InputToken `json:"ownership_token"`
	HandlingToken            datastructs.InputToken `json:"handling_token"`
	OwnerPublicKey           string                 `json:"owner_public_key"`
	ReceiverPublicKey        string                 `json:"receiver_public_key"`
	ReceiverSignatureForItem []byte                 `json:"receiver_signature_for_item"`
	TransferProcess          []byte                 `json:"transfer_process"`
	TransferProcessSignature []byte                 `json:"transfer_process_signature"`
	TransferType             int                    `json:"transfer_type"`
}

// ============================================================================================================================
// TransferItem - Smart Contract for Transferring ownership to another party
// ============================================================================================================================
func (t *CheeseProducer) TransferItem(stub shim.ChaincodeStubInterface, request string) pb.Response {
	fmt.Printf("Request :%v \n", string(request))
	var requestObj TransferRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var sellingRequest datastructs.TransferRequest
	sellingRequest.SpecificationID = requestObj.SpecID
	sellingRequest.TransferProcess.PAYLOAD = requestObj.TransferProcess
	sellingRequest.TransferProcess.Ownership.AuthorityPublicKey = requestObj.OwnerPublicKey
	sellingRequest.TransferProcess.Ownership.PayloadSignature = requestObj.TransferProcessSignature
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.OwnerPublicKey
	handlerAuthority.PayloadSignature = requestObj.TransferProcessSignature
	sellingRequest.TransferProcess.Handlers = append(sellingRequest.TransferProcess.Handlers, handlerAuthority)

	sellingRequest.RelationshipChallengeResponse = requestObj.HandlingToken.InputChallengeResponse

	sellingRequest.AuthorityTransferRequest.OwnershipToken.ItemID = requestObj.OwnershipToken.ItemID
	sellingRequest.AuthorityTransferRequest.OwnershipToken.InputChallengeResponse = requestObj.OwnershipToken.InputChallengeResponse
	sellingRequest.AuthorityTransferRequest.Authority.AuthorityPublicKey = requestObj.ReceiverPublicKey
	sellingRequest.AuthorityTransferRequest.Authority.PayloadSignature = requestObj.ReceiverSignatureForItem
	sellingRequest.AuthorityTransferRequest.AuthorityType = requestObj.TransferType

	sellingRequestBytes, err := json.Marshal(sellingRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.OwnershipTransfer(stub, string(sellingRequestBytes))
}

type CheeseTransportRequest struct {
	TransportSpecID                   string                     `json:"transport_spec_id"`
	CheeseOwnershipToken              datastructs.InputItemToken `json:"cheese_ownership_token"`
	CheeseHandlingToken               datastructs.InputToken     `json:"cheese_handling_token"`
	CheeseOwnerPublicKey              string                     `json:"cheese_owner_public_key"`
	TransporterPublicKey              string                     `json:"transporter_public_key"`
	TransporterSignatureForCheeseItem []byte                     `json:"transporter_signature_for_cheese_item"`
	TransportProcess                  []byte                     `json:"transport_process"`
	TransportProcessSignature         []byte                     `json:"transport_process_signature"`
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *CheeseProducer) SendForTransport(stub shim.ChaincodeStubInterface, request string) pb.Response {
	fmt.Printf("Request :%v \n", string(request))
	var requestObj CheeseTransportRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var transportRequest datastructs.TransferRequest
	if requestObj.TransportSpecID != "" {
		transportRequest.SpecificationID = requestObj.TransportSpecID
	} else {
		transportRequest.SpecificationID = cheeseTransportSpecID
	}
	transportRequest.TransferProcess.PAYLOAD = requestObj.TransportProcess
	transportRequest.TransferProcess.Ownership.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
	transportRequest.TransferProcess.Ownership.PayloadSignature = requestObj.TransportProcessSignature
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
	handlerAuthority.PayloadSignature = requestObj.TransportProcessSignature
	transportRequest.TransferProcess.Handlers = append(transportRequest.TransferProcess.Handlers, handlerAuthority)

	transportRequest.RelationshipChallengeResponse = requestObj.CheeseHandlingToken.InputChallengeResponse

	transportRequest.AuthorityTransferRequest.OwnershipToken.ItemID = requestObj.CheeseOwnershipToken.ItemID
	transportRequest.AuthorityTransferRequest.OwnershipToken.InputChallengeResponse = requestObj.CheeseOwnershipToken.InputChallengeResponse
	transportRequest.AuthorityTransferRequest.Authority.AuthorityPublicKey = requestObj.TransporterPublicKey
	transportRequest.AuthorityTransferRequest.Authority.PayloadSignature = requestObj.TransporterSignatureForCheeseItem

	transportRequest.AuthorityTransferRequest.AuthorityType = 2

	transportRequestBytes, err := json.Marshal(transportRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.HandlingRightsTransfer(stub, string(transportRequestBytes))
}

type TransportRequest struct {
	TransportSpecID           string                 `json:"transport_spec_id"`
	HandlingToken             datastructs.InputToken `json:"transport_item_handling_token"`
	TransporterPublicKey      string                 `json:"transporter_public_key"`
	TransportProcess          []byte                 `json:"transport_process"`
	TransportProcessSignature []byte                 `json:"transport_process_signature"`
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *CheeseProducer) ItemTransport(stub shim.ChaincodeStubInterface, request string) pb.Response {
	fmt.Printf("Request :%v \n", string(request))
	var requestObj TransportRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var transportRequest datastructs.TransportRequest
	if requestObj.TransportSpecID != "" {
		transportRequest.SpecificationID = requestObj.TransportSpecID
	} else {
		return shim.Error(tutil.GenerateResponse("Fail", "transport specification id cannot be blank"))
	}
	transportRequest.TransportProcess.PAYLOAD = requestObj.TransportProcess
	transportRequest.TransportProcess.Ownership.AuthorityPublicKey = requestObj.TransporterPublicKey
	transportRequest.TransportProcess.Ownership.PayloadSignature = requestObj.TransportProcessSignature
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.TransporterPublicKey
	handlerAuthority.PayloadSignature = requestObj.TransportProcessSignature
	transportRequest.TransportProcess.Handlers = append(transportRequest.TransportProcess.Handlers, handlerAuthority)

	transportRequest.RelationshipChallengeResponse = requestObj.HandlingToken.InputChallengeResponse
	transportRequest.ItemID = requestObj.HandlingToken.ItemID

	transportRequestBytes, err := json.Marshal(transportRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.TransportItem(stub, string(transportRequestBytes))
}

type AddProcessUpdateRequest struct {
	ProcessToken       datastructs.InputToken              `json:"process_token"`
	IOTDevicePublicKey string                              `json:"iot_device_public_key"`
	Update             []byte                              `json:"update"`
	UpdateSignature    []byte                              `json:"update_signature"`
	UpdateType         relationships.GraphRelationshipType `json:"update_type"`
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *CheeseProducer) AddProcessUpdate(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var requestObj AddProcessUpdateRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}

	var updateRequest datastructs.ProcessUpdate
	updateRequest.Update.PAYLOAD = requestObj.Update
	updateRequest.Update.Ownership.AuthorityPublicKey = requestObj.IOTDevicePublicKey
	updateRequest.Update.Ownership.PayloadSignature = requestObj.UpdateSignature
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.IOTDevicePublicKey
	handlerAuthority.PayloadSignature = requestObj.UpdateSignature
	updateRequest.Update.Handlers = append(updateRequest.Update.Handlers, handlerAuthority)
	updateRequest.ProcessToken = requestObj.ProcessToken
	updateRequest.UpdateType = requestObj.UpdateType
	updateRequestBytes, err := json.Marshal(updateRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}

	return supplychain.AddUpdate(stub, string(updateRequestBytes))
}

type CheeseSplittingRequest struct {
	PackagingSpecID           string                 `json:"splitting_spec_id"`
	CheeseOwnershipToken      datastructs.InputToken `json:"cheese_ownership_token"`
	CheeseHandlingToken       datastructs.InputToken `json:"cheese_handling_token"`
	CheeseOwnerPublicKey      string                 `json:"cheese_owner_public_key"`
	PackagingProcess          []byte                 `json:"splitting_process"`
	PackagingProcessSignature []byte                 `json:"splitting_process_signature"`
	CheesePackages            [][]byte               `json:"cheese_packages"`
	PackageSignatures         [][]byte               `json:"package_signatures"`
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *CheeseProducer) SplitCheese(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var requestObj CheeseSplittingRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var splitItemRequest datastructs.SplitItemRequest
	if requestObj.PackagingSpecID != "" {
		splitItemRequest.SpecificationID = requestObj.PackagingSpecID
	} else {
		splitItemRequest.SpecificationID = cheesePackagingSpecID
	}
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
	handlerAuthority.PayloadSignature = requestObj.PackagingProcessSignature
	splitItemRequest.SplittingProcess.Handlers = append(splitItemRequest.SplittingProcess.Handlers, handlerAuthority)
	splitItemRequest.SplittingProcess.Ownership.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
	splitItemRequest.SplittingProcess.Ownership.PayloadSignature = requestObj.PackagingProcessSignature
	splitItemRequest.SplittingProcess.PAYLOAD = requestObj.PackagingProcess

	splitItemRequest.ItemToken = requestObj.CheeseOwnershipToken
	splitItemRequest.RelationshipToken = requestObj.CheeseHandlingToken
	var cheesePackages []datastructs.StorageItem
	for index, cheesepackage := range requestObj.CheesePackages {
		var storageItem datastructs.StorageItem
		storageItem.Ownership.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
		storageItem.Ownership.PayloadSignature = requestObj.PackageSignatures[index]
		var handlerAuthority datastructs.HandlerAuthority
		handlerAuthority.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
		handlerAuthority.PayloadSignature = requestObj.PackageSignatures[index]
		storageItem.Handlers = append(storageItem.Handlers, handlerAuthority)
		storageItem.PAYLOAD = cheesepackage
		cheesePackages = append(cheesePackages, storageItem)
	}
	splitItemRequest.SplitItems = cheesePackages

	splitItemRequestBytes, err := json.Marshal(splitItemRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.DivideItem(stub, string(splitItemRequestBytes))
}

type CheeseBoxingRequest struct {
	BoxingSpecID                string                   `json:"boxing_spec_id"`
	CheesePackageHandlingTokens []datastructs.InputToken `json:"cheese_package_handling_tokens"`
	CheeseOwnerPublicKey        string                   `json:"cheese_owner_public_key"`
	MergingProcess              []byte                   `json:"boxing_process"`
	MergingProcessSignature     []byte                   `json:"boxing_process_signature"`
	CheeseBox                   []byte                   `json:"cheese_box"`
	OwnerSignatureForCheeseBox  []byte                   `json:"owner_signature_for_cheese_box"`
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *CheeseProducer) BoxCheese(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var requestObj CheeseBoxingRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var mergeRequest datastructs.MergeIntoContainerRequest
	if requestObj.BoxingSpecID != "" {
		mergeRequest.SpecificationID = requestObj.BoxingSpecID
	} else {
		fmt.Errorf("specification ID must be specified")
	}
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
	handlerAuthority.PayloadSignature = requestObj.MergingProcessSignature
	mergeRequest.MergingProcess.Handlers = append(mergeRequest.MergingProcess.Handlers, handlerAuthority)
	mergeRequest.MergingProcess.Ownership.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
	mergeRequest.MergingProcess.Ownership.PayloadSignature = requestObj.MergingProcessSignature
	mergeRequest.MergingProcess.PAYLOAD = requestObj.MergingProcess
	mergeRequest.InputItems = requestObj.CheesePackageHandlingTokens

	var itemHandlerAuthority datastructs.HandlerAuthority
	itemHandlerAuthority.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
	itemHandlerAuthority.PayloadSignature = requestObj.MergingProcessSignature
	mergeRequest.Container.Handlers = append(mergeRequest.Container.Handlers, itemHandlerAuthority)
	mergeRequest.Container.Ownership.AuthorityPublicKey = requestObj.CheeseOwnerPublicKey
	mergeRequest.Container.Ownership.PayloadSignature = requestObj.OwnerSignatureForCheeseBox
	mergeRequest.Container.PAYLOAD = requestObj.CheeseBox

	mergeRequestBytes, err := json.Marshal(mergeRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.MergeIntoContainer(stub, string(mergeRequestBytes))
}

type CheeseUnBoxingRequest struct {
	SpecID                 string                   `json:"spec_id"`
	ItemHandlingTokens     []datastructs.InputToken `json:"item_handling_tokens"`
	ContainerHandlingToken datastructs.InputToken   `json:"container_handling_token"`
	OwnerPublicKey         string                   `json:"owner_public_key"`
	UnboxingProcess        []byte                   `json:"unboxing_process"`
	ProcessSignature       []byte                   `json:"process_signature"`
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *CheeseProducer) UnboxContainer(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var requestObj CheeseUnBoxingRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var splitRequest datastructs.SplitFromContainerRequest
	if requestObj.SpecID != "" {
		splitRequest.SpecificationID = requestObj.SpecID
	} else {
		fmt.Errorf("specification ID must be specified")
	}
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.OwnerPublicKey
	handlerAuthority.PayloadSignature = requestObj.ProcessSignature
	splitRequest.SplittingProcess.Handlers = append(splitRequest.SplittingProcess.Handlers, handlerAuthority)
	splitRequest.SplittingProcess.Ownership.AuthorityPublicKey = requestObj.OwnerPublicKey
	splitRequest.SplittingProcess.Ownership.PayloadSignature = requestObj.ProcessSignature
	splitRequest.SplittingProcess.PAYLOAD = requestObj.UnboxingProcess
	splitRequest.OutputItems = requestObj.ItemHandlingTokens
	splitRequest.Container = requestObj.ContainerHandlingToken

	mergeRequestBytes, err := json.Marshal(splitRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.SplitFromContainer(stub, string(mergeRequestBytes))
}
