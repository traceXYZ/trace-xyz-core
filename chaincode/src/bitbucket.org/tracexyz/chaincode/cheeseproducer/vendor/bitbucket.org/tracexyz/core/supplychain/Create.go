package supplychain

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/comm"
	"github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/supplychain/validation"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
	"fmt"
)

func CreateItem(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var requestObj datastructs.CreateItemRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Read the spec from storage network
	fmt.Printf("READ SPEC START")
	specStorageItem, err := comm.ReadStorageItemDirect(stub, requestObj.SpecificationID)
	fmt.Printf("READ SPEC END")
	if err != nil {
		return shim.Error(err.Error())
	}
	// Validate the spec
	fmt.Printf("VALID SPEC START \n")
	fmt.Printf("PAYLD : %v \n", specStorageItem.PAYLOAD)
	fmt.Printf("REQ : %v \n", requestObj)
	fmt.Printf("REQ JSON : %v \n", request)

	success, err := validation.ValidateInputsWithSpec(stub, specStorageItem.PAYLOAD, request)
	fmt.Printf("VALID SPEC END")
	if err != nil {
		return shim.Error(err.Error())
	}
	if !success {
		return shim.Error("the request fails to comply with the specification ID:" + requestObj.SpecificationID)
	}
	// Update the quota
	// todo: get the quota update token

	//Create the items for storage

	generatedProcessID := util.GenerateUUID()
	generatedItemID := util.GenerateUUID()

	itemNode := graph.NewNode(generatedItemID, "RawMaterial")
	processNode := graph.NewNode(generatedProcessID, "CreateRawMaterialProcess")

	processItemRelationship := graph.NewEdge(generatedProcessID, generatedItemID, relationships.PublicTrace, states.Active)

	specRelationship := graph.NewEdge(requestObj.SpecificationID, generatedProcessID, relationships.Specification, states.Active)

	requestObj.CreationProcess.ID = generatedProcessID
	requestObj.CreatedItem.ID = generatedItemID

	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = []datastructs.Node{itemNode, processNode}
	storageRequest.Edges = []datastructs.Edge{processItemRelationship}
	storageRequest.Items = []datastructs.StorageItem{requestObj.CreatedItem}
	storageRequest.Process.ProcessData = requestObj.CreationProcess
	storageRequest.Process.ProcessSpecRelationship = specRelationship

	fmt.Printf("Store TXN START")
	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	fmt.Printf("Store TXN END")
	if err != nil {
		return shim.Error(err.Error())
	}
	// todo: commit the creation item

	var response datastructs.CreateItemResponse
	response.StorageNetworkResponse = storageNWResponse
	response.ItemID = generatedItemID
	response.ProcessID = generatedProcessID
	response.ProcessSpecID = requestObj.SpecificationID
	response.SupplyChainResponse.ResponseType = "create"
	response.SupplyChainResponse.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(responseBytes)
}
