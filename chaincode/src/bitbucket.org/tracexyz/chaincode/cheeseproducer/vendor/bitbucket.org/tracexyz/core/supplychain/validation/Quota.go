package validation

import (
	"reflect"
	"github.com/oliveagle/jsonpath"
	"fmt"
	"bitbucket.org/tracexyz/core/comm"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func processSpecQuota(stub shim.ChaincodeStubInterface, checkingSpec, checkingValue interface{}) (bool, error) {
	checkingSpecList, ok := checkingSpec.([]interface{})
	if !ok {
		return false, nil
	}
	for _, specItemIter := range checkingSpecList {
		specItem, ok := specItemIter.(map[string]interface{})
		if !ok {
			return false, nil
		}
		var fieldPath = specItem["field_path"]
		var payloadPath = specItem["payload_path"]
		if fieldPath == nil || payloadPath == nil {
			return false, nil
		}
		fieldPathS, ok := fieldPath.(string)
		if !ok {
			return false, nil
		}

		payloadPathS, ok := payloadPath.(string)
		if !ok {
			return false, nil
		}

		checkingItemMap, err := extractPayloadMap(checkingValue, payloadPath)
		if err != nil {
			return false, err
		}
		rValue, err := jsonpath.JsonPathLookup(checkingItemMap, fieldPathS)
		if err != nil {
			fmt.Printf("ERR JSN : %v \n", err)
			return false, err
		}
		if reflect.TypeOf(rValue) == reflect.TypeOf([]interface{}{}) {
			return false, fmt.Errorf("cannot validate quota for the field")
		} else {
			var path = payloadPathS + "." + fieldPathS[2:len(fieldPathS)]
			rValueF, ok := rValue.(float64)
			if !ok {
				return false, nil
			}
			_, err := comm.SpendQuota(stub, rValueF, path)
			if err != nil {
				return false, err
			}
		}
	}
	return true, nil
}
