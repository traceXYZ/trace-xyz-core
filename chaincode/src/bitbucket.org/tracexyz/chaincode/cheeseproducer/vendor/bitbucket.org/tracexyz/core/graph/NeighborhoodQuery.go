package graph

import (
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
)

// Returns all the source nodes of a given node and for a given edge type
// The edge type is used for efficiency of queries
func GetSourcesList(stub shim.ChaincodeStubInterface,
	itemID string, edgeType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	queryIterator, err := stub.GetStateByPartialCompositeKey("Edge", []string{string(edgeType), itemID})
	if err != nil {
		return nil, nil, err
	}
	var nodes []datastructs.Node
	var edges []datastructs.Edge
	for queryIterator.HasNext() {
		var tempEdge datastructs.Edge
		nextEdge, err := queryIterator.Next()
		if err != nil {
			return nil, nil, err
		}
		json.Unmarshal(nextEdge.Value, &tempEdge)
		if tempEdge.DestItemID == itemID {
			tempNode, isAvailable, err := GetNode(stub, tempEdge.SourceItemID)
			if err != nil {
				return nil, nil, err
			}
			if !isAvailable {
				return nil, nil, fmt.Errorf("the node %s is not available in the graph", tempEdge.SourceItemID)
			}

			nodes = append(nodes, tempNode)
			edges = append(edges, tempEdge)
			fmt.Printf("TN %v \n", tempNode)
			fmt.Printf("N %v \n", nodes)
			fmt.Printf("TE %v \n", tempEdge)
			fmt.Printf("E %v \n", edges)
		}
	}
	fmt.Printf("\n SNodesNQ: %v \n R:%v \n", nodes, 0)
	return nodes, edges, nil
}

// Returns all the destination nodes from a given node and for a given edge type
// The edge type is used for efficiency of queries
func GetDestinationsList(stub shim.ChaincodeStubInterface, itemID string, relationshipType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	queryIterator, err := stub.GetStateByPartialCompositeKey("Edge", []string{string(relationshipType), itemID})
	if err != nil {
		return nil, nil, err
	}
	var nodes []datastructs.Node
	var edges []datastructs.Edge
	for queryIterator.HasNext() {
		var tempEdge datastructs.Edge
		nextEdge, err := queryIterator.Next()
		if err != nil {
			return nil, nil, err
		}
		json.Unmarshal(nextEdge.Value, &tempEdge)
		if tempEdge.SourceItemID == itemID {
			tempNode, isAvailable, err := GetNode(stub, tempEdge.DestItemID)
			if err != nil {
				return nil, nil, err
			}
			if !isAvailable {
				return nil, nil, fmt.Errorf("the node %s is not available in the graph", tempEdge.DestItemID)
			}
			nodes = append(nodes, tempNode)
			edges = append(edges, tempEdge)
			fmt.Printf("TN %v \n", tempNode)
			fmt.Printf("N %v \n", nodes)
			fmt.Printf("TE %v \n", tempEdge)
			fmt.Printf("E %v \n", edges)
		}
	}
	fmt.Printf("\n DNodesNQ: %v \n R:%v \n", nodes, 0)
	return nodes, edges, nil
}
