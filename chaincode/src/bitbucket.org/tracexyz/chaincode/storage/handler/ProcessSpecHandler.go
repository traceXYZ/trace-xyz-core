package handler

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"fmt"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/graph"
)

func StoreProcess(stub shim.ChaincodeStubInterface, process datastructs.Process, txnID string) (bool, datastructs.StoredItemToken, error) {
	success, token, err := StoreItem(stub, process.ProcessData, txnID)
	if err != nil {
		return false, token, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !success {
		return false, token, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot store the item in the storage network"))
	}
	var processSpec = process.ProcessSpecRelationship
	processSpec.DestItemID = process.ProcessSpecRelationship.DestItemID + txnID
	fmt.Sprintf("Process Spec: %v \n", processSpec)
	success, err = graph.AddEdge(stub, processSpec)
	if err != nil {
		return false, token, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !success {
		return false, token, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot store a graph edge in storage network. check graph edge mapping"))
	}
	return true, token, nil
}
