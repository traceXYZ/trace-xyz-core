package handler

import "github.com/hyperledger/fabric/core/chaincode/shim"
import (
	"fmt"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
)

// Stores a set of nodes in the blockchain network
func StoreNodes(stub shim.ChaincodeStubInterface, nodes []datastructs.Node, txnID string) (bool, error) {
	for _, node := range nodes {
		node.ItemID = node.ItemID + txnID
		success, err := graph.AddNode(stub, node)
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot store a graph node in storage network. check graph node mapping"))
		}
	}
	return true, nil
}

func StoreEdges(stub shim.ChaincodeStubInterface, edges []datastructs.Edge, txnID string) (bool, error) {
	for _, edge := range edges {
		edge.SourceItemID = edge.SourceItemID + txnID
		edge.DestItemID = edge.DestItemID + txnID
		success, err := graph.AddEdge(stub, edge)
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot store a graph edge in storage network. check graph edge mapping"))
		}
	}
	return true, nil
}

func StoreSourceToItemEdges(stub shim.ChaincodeStubInterface, edgeUpdates []datastructs.EdgeUpdate, txnID string) (bool, error) {
	for _, edgeUpdate := range edgeUpdates {
		edgeUpdate.Edge.DestItemID = edgeUpdate.Edge.DestItemID + txnID
		success, err := graph.AddEdge(stub, edgeUpdate.Edge)
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot store a graph edgeUpdate in storage network. check graph edgeUpdate mapping"))
		}

	}
	return true, nil
}

func StoreItemToSourceEdges(stub shim.ChaincodeStubInterface, edgeUpdates []datastructs.EdgeUpdate, txnID string) (bool, error) {
	for _, edgeUpdate := range edgeUpdates {
		edgeUpdate.Edge.SourceItemID = edgeUpdate.Edge.SourceItemID + txnID
		success, err := graph.AddEdge(stub, edgeUpdate.Edge)
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot store a graph edgeUpdate in storage network. check graph edgeUpdate mapping"))
		}

	}
	return true, nil
}
