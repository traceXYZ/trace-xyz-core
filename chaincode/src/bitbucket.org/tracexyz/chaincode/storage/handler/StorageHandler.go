package handler

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"fmt"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/chaincode/storage/storageutils"
	"bitbucket.org/tracexyz/core/rwledger"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"github.com/hyperledger/fabric/common/util"
)

var maxSizeOfPayload = 512000 //Equal to 512KB

func StoreItems(stub shim.ChaincodeStubInterface, items []datastructs.StorageItem, txnID string) (bool, []datastructs.StoredItemToken, error) {
	var storedItemTokens []datastructs.StoredItemToken
	for _, storageItem := range items {
		success, token, err := StoreItem(stub, storageItem, txnID)
		if err != nil {
			return false, nil, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return false, nil, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot store the item in the storage network"))
		}

		storedItemTokens = append(storedItemTokens, token)
	}
	return true, storedItemTokens, nil
}

func StoreItem(stub shim.ChaincodeStubInterface, storageItem datastructs.StorageItem, txnID string) (bool, datastructs.StoredItemToken, error) {
	var token datastructs.StoredItemToken
	if len(storageItem.PAYLOAD) > maxSizeOfPayload {
		return false, token, fmt.Errorf("'payload' can have maximum of 512KB in order to store in blockchain")
	}
	fmt.Printf("STRGITM: %v \n", storageItem)
	fmt.Printf("PASS2SDE")
	err := tutil.VerifyOwnership(storageItem)
	fmt.Printf("PASS3")
	storageItem.ID = storageItem.ID + txnID
	storageItem.Ownership.NextChallenge = util.GenerateUUID()
	storageItem.HandlerChallenge = storageItem.Ownership.NextChallenge
	if err != nil {
		return false, token, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	storageItem, err = storageutils.PopulateHandlerInformation(stub, storageItem)
	if err != nil {
		return false, token, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	success, err := rwledger.CreateOrUpdate(stub, storageItem.ID, storageItem)
	if err != nil {
		return false, token, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !success {
		return false, token, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot store the item in the storage network"))
	}
	token.ItemID = storageItem.ID
	token.NextUpdateChallenge = storageItem.Ownership.NextChallenge
	return true, token, nil
}

func RetrieveGraphSources(stub shim.ChaincodeStubInterface, itemID string,
	relationshipType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	return graph.GetSourcesList(stub, itemID, relationshipType)
}

func RetrieveGraphSourcesHistory(stub shim.ChaincodeStubInterface, itemID string,
	relationshipType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	var visitedMap = make(map[string]bool)
	return graph.GetSourceNodesRecursive(stub, itemID, relationshipType, 200, visitedMap)
}

func RetrieveGraphDestinations(stub shim.ChaincodeStubInterface, itemID string,
	relationshipType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	return graph.GetDestinationsList(stub, itemID, relationshipType)
}

func RetrieveGraphDestinationsHistory(stub shim.ChaincodeStubInterface, itemID string,
	relationshipType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	var visitedMap = make(map[string]bool)
	return graph.GetDestNodesRecursive(stub, itemID, relationshipType, 200, visitedMap)
}

func RetrieveCustomWalk(stub shim.ChaincodeStubInterface, itemID string, walkPath []relationships.GraphRelationshipType,
	includeHistory, includeFuture bool) ([]datastructs.Node, []datastructs.Edge, error) {
	return graph.CustomWalk(stub, itemID, walkPath, includeHistory, includeFuture)
}
