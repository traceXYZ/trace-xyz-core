package handler

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"fmt"
	"github.com/oliveagle/jsonpath"
	"reflect"
	"github.com/tidwall/sjson"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
	"bitbucket.org/tracexyz/chaincode/storage/storageutils"
)

func StoreUpdates(stub shim.ChaincodeStubInterface, updates []datastructs.StorageUpdate, cache map[string]datastructs.StorageItem) (bool, error) {

	for _, storageUpdate := range updates {
		// bring in the original item
		storageItem, err := storageutils.PopulateStorageItemFromCache(stub, storageUpdate.ItemID, cache)

		fmt.Printf("\nstorageUpdate: %v \n", storageUpdate)
		// Populate handler information
		storageItem, err = storageutils.PopulateHandlerInformation(stub, storageItem)
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		var item = storageItem.PAYLOAD
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		var itemMap map[string]interface{}
		err = json.Unmarshal(item, &itemMap)
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		updateValue, err := jsonpath.JsonPathLookup(itemMap, storageUpdate.UpdatePath)
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		fmt.Printf("\nstorageItem: %v \n", itemMap)
		if reflect.TypeOf(updateValue) == reflect.TypeOf([]interface{}{}) {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", "updating multiple values is not supported"))
		} else {
		}
		updateValueF, ok := updateValue.(float64)
		if !ok {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", "the specified path does not have an updatable value"))
		}
		switch storageUpdate.UpdateType {
		case datastructs.Add:
			updateValueF = updateValueF + storageUpdate.UpdateValue
			break
		case datastructs.Subtract:
			fmt.Printf("\nupdateValueF: %v \n", updateValueF)
			fmt.Printf("\nstorageUpdate.UpdateValue: %v \n", storageUpdate.UpdateValue)
			updateValueF = updateValueF - storageUpdate.UpdateValue
			if updateValueF < 0 {
				return false, fmt.Errorf(tutil.GenerateResponse("Fail", "the input source resource quantity "+
					"cannot be negative after processing!"))
			}
			break
		case datastructs.Exact:
			break
		default:
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", "unsupported update operation"))

		}
		strippedPath := storageUpdate.UpdatePath[2:len(storageUpdate.UpdatePath)]
		//itemBytes,err := json.Marshal(item)
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if storageUpdate.UpdateType != datastructs.Exact {
			item, err = sjson.SetBytes(item, strippedPath, updateValueF)
		} else {
			item, err = sjson.SetBytes(item, strippedPath, updateValue)
		}
		if err != nil {
			return false, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		storageItem.PAYLOAD = item

		cache[storageUpdate.ItemID] = storageItem
	}
	return true, nil
}
