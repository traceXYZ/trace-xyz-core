package storageutils

import (
	"fmt"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/tutil"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/rwledger"
	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
)

func RetrieveItems(stub shim.ChaincodeStubInterface, nodes []datastructs.Node) ([]datastructs.StorageItem, error) {
	var items []datastructs.StorageItem
	//var nilCtrlObject datastructs.AccessControl
	for _, node := range nodes {
		var item datastructs.StorageItem
		success, err := rwledger.Read(stub, node.ItemID, &item)
		if err != nil {
			return nil, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return nil, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot retrieve items from the storage network"))
		}
		//item.AccessControl = nilCtrlObject
		items = append(items, item)
	}
	return items, nil
}

func PopulateHandlerInformation(stub shim.ChaincodeStubInterface, storageItem datastructs.StorageItem) (datastructs.StorageItem, error) {
	handlerPK, err := cid.GetX509Certificate(stub)
	if err != nil {
		return storageItem, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	storageItem.HandlerPublicKey = handlerPK
	handlerID, err := cid.GetID(stub)
	if err != nil {
		return storageItem, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
	}
	storageItem.HandlerIdentity = handlerID
	fmt.Printf("%v \n", storageItem)
	return storageItem, nil
}

func PopulateStorageItemFromCache(stub shim.ChaincodeStubInterface, storageItemID string,
	cache map[string]datastructs.StorageItem) (datastructs.StorageItem, error) {
	var storageItem datastructs.StorageItem
	if cache[storageItemID].ID == "" {
		success, err := rwledger.Read(stub, storageItemID, &storageItem)
		if err != nil {
			return storageItem, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return storageItem, fmt.Errorf(tutil.GenerateResponse("Fail", "cannot read the ledger item"+storageItemID))
		}
		cache[storageItemID] = storageItem
	} else {
		storageItem = cache[storageItemID]
	}
	return storageItem, nil
}
