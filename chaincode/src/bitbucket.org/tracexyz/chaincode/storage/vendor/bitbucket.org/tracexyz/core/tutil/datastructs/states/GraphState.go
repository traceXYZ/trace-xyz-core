package states

type GraphRelationshipState string

const (
	Active   GraphRelationshipState = "Active"
	Inactive GraphRelationshipState = "Inactive"
)
