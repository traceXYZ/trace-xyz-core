package datastructs

import "crypto/ecdsa"

const (
	Days         ResetMethod = 1
	OnUpdate     ResetMethod = 2
	OnBlockCount ResetMethod = 3
)

type ResetMethod int

type Quota struct {
	FieldPath   string      `json:"field_path"`
	Quota       float64     `json:"quota"`
	ResetMethod ResetMethod `json:"reset_method"`
	Nonce       []string    `json:"nonce"`
}

type UserContext struct {
	UserID string  `json:"user"`
	Quota  []Quota `json:"quota"`
}

type QuotaUpdate struct {
	UserID string `json:"user_id"`
	Quota  Quota  `json:"quota"`
}

type QuotaRead struct {
	UserID    string `json:"user_id"`
	FieldPath string `json:"field_path"`
}

type QuotaSpend struct {
	QuotaRead
	SpendingQty float64 `json:"spending_qty"`
}

type QuotaRollback struct {
	QuotaSpend
	Nonce string `json:"nonce"`
}

type KeyPair struct {
	PublicKey  ecdsa.PublicKey  `json:"public_key"`
	PrivateKey ecdsa.PrivateKey `json:"private_key"`
}
