package rwledger

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func CreateOrUpdate(stub shim.ChaincodeStubInterface, id string, obj interface{}) (bool, error) {
	var err error
	objJson, err := json.Marshal(obj)
	if err != nil {
		return false, err
	}
	fmt.Printf("\nWrite Item: %v \n", string(objJson))
	err = stub.PutState(id, objJson)
	if err != nil {
		return false, err
	}

	return true, nil
}

func Remove(stub shim.ChaincodeStubInterface, id string) (bool, error) {
	var err error
	fmt.Println("Remove {")

	err = stub.DelState(id)
	if err != nil {
		return false, err
	}

	fmt.Println("} Remove")
	return true, nil
}
