/*
Basic Implementation for TraceXYZ Asset Chaincode
*/

package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/chaincode/storage/handler"
	"encoding/json"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/rwledger"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/chaincode/storage/storageutils"
	"bitbucket.org/tracexyz/chaincode/storage/auth"
)

// Asset Chaincode implementation
type StorageNetwork struct {
}

// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(StorageNetwork))
	if err != nil {
		fmt.Printf("Error starting TraceXZ chaincode - %s", err)
	}
}

// ============================================================================================================================
// Init - initialize the chaincode
//
// ============================================================================================================================
func (t *StorageNetwork) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success([]byte("Success"))
}

// ============================================================================================================================
// Invoke - Our entry point for Invocations
// Retailer Chaincode is used to perform following functions
// *Produce Milk
// *Sell Milk
// ============================================================================================================================
func (t *StorageNetwork) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("Invoke request" + args[0]) //todo remove this. for debug purposes only
	fmt.Println("starting invoke, for - " + function)
	if len(args) < 1 {
		return shim.Error(tutil.GenerateResponse("Fail",
			"minimum of 1 arguments required to execute this function"))
	}
	// Handle different functions
	switch function {
	case "init":
		return t.Init(stub)
	case datastructs.StoreTxn:
		return t.StoreTxn(stub, args[0])
	case datastructs.Update:
		return t.Update(stub, args[0])
	case datastructs.Retrieve:
		return t.Retrieve(stub, args[0])
	case datastructs.AuthorityTransfer:
		return t.UpdateAuthority(stub, args[0])
	case datastructs.RetrieveDirect:
		return t.RetrieveDirect(stub, args[0])
	case datastructs.RetrieveHistory:
		return t.RetrieveHistory(stub, args[0])
	}
	fmt.Println("Received unknown invoke function name - " + function)
	return shim.Error("Received unknown invoke function name - '" + function + "'")
}

//// ============================================================================================================================
//// StoreSpec - Smart Contract for Storing a specification
//// ============================================================================================================================
//func (t *StorageNetwork) StoreSpec(stub shim.ChaincodeStubInterface, request string) pb.Response {
//	isAdmin,err := tutil.IsAdmin(stub)
//	if err != nil {
//		return shim.Error(err.Error())
//	}
//	if !isAdmin {
//		return shim.Error(tutil.GenerateResponse("Fail","only admin users can register other users"))
//	}
//	specTraceKey,err := storageutils.StoreSpec(stub,request)
//	if err != nil {
//		return shim.Error(tutil.GenerateResponse("Fail",err.Error()))
//	}
//	specID, err := json.Marshal(specTraceKey)
//	if err != nil {
//		return shim.Error(tutil.GenerateResponse("Fail",err.Error()))
//	}
//	return shim.Success([]byte(tutil.GenerateResponse("Success","Spec ID:"+string(specID))))
//}

//// ============================================================================================================================
//// StoreSpec - Smart Contract for Storing a specification
//// ============================================================================================================================
//func (t *StorageNetwork) StoreUser(stub shim.ChaincodeStubInterface, user,mspID,enrollmentID string) pb.Response {
//	isAdmin,err := tutil.IsAdmin(stub)
//	if err != nil {
//		return shim.Error(err.Error())
//	}
//	if !isAdmin {
//		return shim.Error(tutil.GenerateResponse("Fail","only admin users can register other users"))
//	}
//	userID,err := handler.StoreUser(stub,user,mspID,enrollmentID)
//	if err != nil {
//		return shim.Error(tutil.GenerateResponse("Fail",err.Error()))
//	}
//	return shim.Success([]byte(tutil.GenerateResponse("Success","User ID:"+userID)))
//}

// ============================================================================================================================
// StoreTxn - Smart Contract for Storing a Transaction
// ============================================================================================================================
func (t *StorageNetwork) StoreTxn(stub shim.ChaincodeStubInterface, request string) pb.Response {
	txnID := util.GenerateUUID()
	fmt.Printf("\n STORE REQ: %v \n", request)
	var storeTxnRequest datastructs.StoreTxnRequest
	err := json.Unmarshal([]byte(request), &storeTxnRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	fmt.Printf("\n STORE REQ UNMARSHALLED \n")

	itemUpdateTokens, relationshipUpdateTokens, err := auth.HandleTxnUpdates(stub, storeTxnRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}

	var processToken datastructs.StoredItemToken
	var success = true

	fmt.Printf("%v \n", "PASS 0")
	if storeTxnRequest.Nodes != nil {
		success, err := handler.StoreNodes(stub, storeTxnRequest.Nodes, txnID)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return shim.Error(tutil.GenerateResponse("Fail", "cannot store graph nodes in storage network. check graph node mapping"))
		}
	}
	fmt.Printf("%v \n", "PASS 1")
	if storeTxnRequest.SourceToItemEdges != nil {
		success, err := handler.StoreSourceToItemEdges(stub, storeTxnRequest.SourceToItemEdges, txnID)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return shim.Error(tutil.GenerateResponse("Fail", "cannot store source edges in storage network. check graph source edges mapping"))
		}
	}
	fmt.Printf("%v \n", "PASS 1")
	if storeTxnRequest.ItemToSourceEdges != nil {
		success, err := handler.StoreItemToSourceEdges(stub, storeTxnRequest.ItemToSourceEdges, txnID)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return shim.Error(tutil.GenerateResponse("Fail", "cannot store source edges in storage network. check graph source edges mapping"))
		}
	}
	fmt.Printf("%v \n", "PASS 2X")
	if storeTxnRequest.Edges != nil {
		success, err := handler.StoreEdges(stub, storeTxnRequest.Edges, txnID)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return shim.Error(tutil.GenerateResponse("Fail", "cannot store graph edges in storage network. check graph edges mapping"))
		}
	}
	fmt.Printf("%v \n", "PASS 3")
	var storedItemTokens []datastructs.StoredItemToken
	if storeTxnRequest.Items != nil {
		success, itemTokenTemp, err := handler.StoreItems(stub, storeTxnRequest.Items, txnID)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return shim.Error(tutil.GenerateResponse("Fail", "cannot store items in storage network. check graph items mapping"))
		}
		storedItemTokens = itemTokenTemp
	}

	fmt.Printf("%v \n", "PASS 5")
	if storeTxnRequest.Process.ProcessData.ID != "" {
		fmt.Printf("\n PROCESS START \n")
		fmt.Printf("\n PROCESS %v \n", storeTxnRequest.Process)
		success, processToken, err = handler.StoreProcess(stub, storeTxnRequest.Process, txnID)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !success {
			return shim.Error(tutil.GenerateResponse("Fail", "cannot store graph nodes in storage network. check graph node mapping"))
		}
		fmt.Printf("\n PROCESS END \n")
	}

	fmt.Printf("%v \n", "PASS 6")
	response := datastructs.StoreTxnResponse{
		Response: datastructs.Response{
			Status:  "Success",
			Message: "All items stored in the storage network",
		},
		StoredItemTokens:          storedItemTokens,
		UpdatedItemTokens:         itemUpdateTokens,
		UpdatedRelationshipTokens: relationshipUpdateTokens,
		ProcessToken:              processToken,
		TxnID:                     txnID,
	}
	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return shim.Success(responseBytes)
}

// ============================================================================================================================
// Update - Smart Contract for Updating values in blockchain
// ============================================================================================================================
func (t *StorageNetwork) Update(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var updates []datastructs.StorageUpdate
	err := json.Unmarshal([]byte(request), &updates)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}

	tokens, err := auth.HandleDirectUpdates(stub, updates)

	response := datastructs.UpdateTxnResponse{
		Response: datastructs.Response{
			Status:  "Success",
			Message: "All items updated in the storage network",
		},
		UpdatedItemTokens: tokens,
	}
	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return shim.Success(responseBytes)
}

// ============================================================================================================================
// Update - Smart Contract for Updating values in blockchain
// ============================================================================================================================
func (t *StorageNetwork) UpdateAuthority(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var requestObj datastructs.AuthorityTransferRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	fmt.Printf("REQUEST: %v \n", request)

	var storageItem datastructs.StorageItem
	success, err := rwledger.Read(stub, requestObj.OwnershipToken.ItemID, &storageItem)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !success {
		return shim.Error(tutil.GenerateResponse("Fail", "cannot read the ledger"))
	}
	isVerified, err := tutil.VerifyItemToken(storageItem, requestObj.OwnershipToken.InputChallengeResponse)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !isVerified {
		return shim.Error(tutil.GenerateResponse("Fail", "authentication failed for the owner"))
	}

	nextChallenge := util.GenerateUUID()
	// Transfer full authority. Revoke old handler authorities
	if requestObj.AuthorityType == 0 {
		// update storage Item
		storageItem.Ownership.AuthorityPublicKey = requestObj.Authority.AuthorityPublicKey
		storageItem.Ownership.PayloadSignature = requestObj.Authority.PayloadSignature
		var handlerAuthority datastructs.HandlerAuthority
		handlerAuthority.AuthorityPublicKey = requestObj.Authority.AuthorityPublicKey
		handlerAuthority.PayloadSignature = requestObj.Authority.PayloadSignature
		storageItem.Handlers = []datastructs.HandlerAuthority{}
		storageItem.Handlers = append(storageItem.Handlers, handlerAuthority)
		err = tutil.VerifyOwnership(storageItem)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		storageItem.Ownership.NextChallenge = nextChallenge
		storageItem.HandlerChallenge = nextChallenge

	} else if requestObj.AuthorityType == 1 {
		// Transfer partial authority, only ownership will transfer
		// update storage Item
		storageItem.Ownership.AuthorityPublicKey = requestObj.Authority.AuthorityPublicKey
		storageItem.Ownership.PayloadSignature = requestObj.Authority.PayloadSignature
		err = tutil.VerifyHandler(storageItem)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		storageItem.Ownership.NextChallenge = nextChallenge
		storageItem.HandlerChallenge = nextChallenge
	} else if requestObj.AuthorityType == 2 {
		// Transfer partial authority, add a new handler
		// update storage Item
		var handlerAuthority datastructs.HandlerAuthority
		handlerAuthority.AuthorityPublicKey = requestObj.Authority.AuthorityPublicKey
		handlerAuthority.PayloadSignature = requestObj.Authority.PayloadSignature
		storageItem.Handlers = append(storageItem.Handlers, handlerAuthority)
		err = tutil.VerifyHandler(storageItem)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		storageItem.Ownership.NextChallenge = nextChallenge
		storageItem.HandlerChallenge = nextChallenge
	} else {
		return shim.Error(tutil.GenerateResponse("Fail", "unsupported transfer request type"))
	}

	success, err = rwledger.CreateOrUpdate(stub, requestObj.OwnershipToken.ItemID, storageItem)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !success {
		return shim.Error(tutil.GenerateResponse("Fail", "cannot write to the ledger"))
	}

	response := datastructs.AuthorityUpdateResponse{
		Response: datastructs.Response{
			Status:  "Success",
			Message: "Authority transferred in the storage network",
		},
		UpdatedItemToken: datastructs.StoredItemToken{
			ItemID:              storageItem.ID,
			NextUpdateChallenge: nextChallenge,
		},
	}
	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return shim.Success(responseBytes)
}

// ============================================================================================================================
// Retrieve - Smart Contract for Retrieving historical Values for an item ID from the blockchain
// ============================================================================================================================
func (t *StorageNetwork) Retrieve(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var requestObj datastructs.AssetRetrieveRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var nodes []datastructs.Node
	var edges []datastructs.Edge
	nodes = []datastructs.Node{}
	edges = []datastructs.Edge{}
	fmt.Printf("Ret Type %v\n", requestObj.RetrieveType)
	fmt.Printf("Rel Type %v\n", requestObj.RelationshipType)
	switch requestObj.RetrieveType {
	case datastructs.ImmediateHistory:
		nodes, edges, err = handler.RetrieveGraphSources(stub, requestObj.ItemID, requestObj.RelationshipType)
		break
	case datastructs.AllHistory:
		nodes, edges, err = handler.RetrieveGraphSourcesHistory(stub, requestObj.ItemID, requestObj.RelationshipType)
		break
	case datastructs.ImmediateFuture:
		nodes, edges, err = handler.RetrieveGraphDestinations(stub, requestObj.ItemID, requestObj.RelationshipType)
		break
	case datastructs.AllFuture:
		nodes, edges, err = handler.RetrieveGraphDestinationsHistory(stub, requestObj.ItemID, requestObj.RelationshipType)
		break
	case datastructs.Immediate:
		nodes, edges, err = handler.RetrieveGraphSources(stub, requestObj.ItemID, requestObj.RelationshipType)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		nodesTmp, edgesTmp, err := handler.RetrieveGraphDestinations(stub, requestObj.ItemID, requestObj.RelationshipType)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		nodes = append(nodes, nodesTmp...)
		edges = append(edges, edgesTmp...)
		break
	case datastructs.All:
		nodes, edges, err = handler.RetrieveGraphSourcesHistory(stub, requestObj.ItemID, requestObj.RelationshipType)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		nodesTmp, edgesTmp, err := handler.RetrieveGraphDestinationsHistory(stub, requestObj.ItemID, requestObj.RelationshipType)
		if err != nil {
			return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
		}
		nodes = append(nodes, nodesTmp...)
		edges = append(edges, edgesTmp...)
		break
	case datastructs.CustomWalk:
		nodes, edges, err = handler.RetrieveCustomWalk(stub, requestObj.ItemID, requestObj.WalkPath,
			requestObj.CustWalkIncludeHistory, requestObj.CustWalkIncludeFuture)
		break
	}
	fmt.Printf("NNN %v \n", nodes)
	fmt.Printf("EEE %v \n", edges)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	items, err := storageutils.RetrieveItems(stub, nodes)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var response datastructs.AssetRetrievalResponse
	response.Status = "Success"
	response.Message = "All items retrieved from the blockchain network"
	response.Nodes = nodes
	response.Edges = edges
	response.StorageItems = items
	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return shim.Success(responseBytes)
}

// ============================================================================================================================
// Retrieve - Smart Contract for Retrieving Values for an item ID from the blockchain
// ============================================================================================================================
func (t *StorageNetwork) RetrieveDirect(stub shim.ChaincodeStubInterface, itemID string) pb.Response {
	var item datastructs.StorageItem
	fmt.Printf("\n ITEMID: " + itemID + "\n")
	success, err := rwledger.Read(stub, itemID, &item)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	if !success {
		return shim.Error(tutil.GenerateResponse("Fail", "cannot read in storage network"))
	}
	var response datastructs.DirectRetrieveResponse
	response.Status = "Success"
	response.Message = "Item retrieved from the blockchain network"
	response.StorageItem = item
	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return shim.Success(responseBytes)
}

// ============================================================================================================================
// Retrieve - Smart Contract for Retrieving historical Values for an item ID from the blockchain
// ============================================================================================================================
func (t *StorageNetwork) RetrieveHistory(stub shim.ChaincodeStubInterface, itemID string) pb.Response {
	fmt.Printf("\n ITEMID: " + itemID + "\n")
	history, err := rwledger.ReadHistory(stub, itemID)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var response datastructs.HistoryRetrieveResponse
	response.Status = "Success"
	response.Message = "Item History retrieved from the blockchain network"
	response.History = history
	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return shim.Success(responseBytes)
}
