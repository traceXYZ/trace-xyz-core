package auth

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"fmt"
	"bitbucket.org/tracexyz/core/tutil"
	"github.com/hyperledger/fabric/common/util"
	"encoding/json"
	"bitbucket.org/tracexyz/chaincode/storage/storageutils"
)

func AuthenticateItemUpdates(stub shim.ChaincodeStubInterface, updateAuthentications []datastructs.UpdateAuthentication,
	cache map[string]datastructs.StorageItem) (bool, []datastructs.StoredItemToken, error) {
	var updatedItemTokens []datastructs.StoredItemToken
	var alreadyAuthenticated = make(map[string]bool)
	for _, updateAuthentication := range updateAuthentications {
		if alreadyAuthenticated[updateAuthentication.ItemID] {
			continue
		}
		alreadyAuthenticated[updateAuthentication.ItemID] = true
		storageItem, err := storageutils.PopulateStorageItemFromCache(stub, updateAuthentication.ItemID, cache)
		if err != nil {
			return false, updatedItemTokens, err
		}

		strBytes, _ := json.Marshal(storageItem)
		fmt.Printf("ITEM %v\n", updateAuthentication.ItemID)
		fmt.Printf("CR %v\n", updateAuthentication.ChallengeResponse)
		fmt.Printf("STR ITEM %v\n", string(strBytes))
		isVerified, err := tutil.VerifyItemToken(storageItem, updateAuthentication.ChallengeResponse)
		if err != nil {
			return false, updatedItemTokens, err
		}
		if !isVerified {
			return false, updatedItemTokens, fmt.Errorf("authentication failed for the item")
		}
		newChallenge := util.GenerateUUID()
		storageItem.Ownership.NextChallenge = newChallenge

		var token datastructs.StoredItemToken
		token.ItemID = storageItem.ID
		token.NextUpdateChallenge = newChallenge
		updatedItemTokens = append(updatedItemTokens, token)
		cache[updateAuthentication.ItemID] = storageItem

	}
	return true, updatedItemTokens, nil
}

func AuthenticateRelationshipUpdates(stub shim.ChaincodeStubInterface, updateAuthentications []datastructs.UpdateAuthentication,
	cache map[string]datastructs.StorageItem) (bool, []datastructs.StoredItemToken, error) {
	var updatedItemTokens []datastructs.StoredItemToken
	var alreadyAuthenticated = make(map[string]bool)
	for _, updateAuthentication := range updateAuthentications {
		if alreadyAuthenticated[updateAuthentication.ItemID] {
			continue
		}
		alreadyAuthenticated[updateAuthentication.ItemID] = true
		storageItem, err := storageutils.PopulateStorageItemFromCache(stub, updateAuthentication.ItemID, cache)

		isVerified, err := tutil.VerifyRelationshipToken(storageItem, updateAuthentication.ChallengeResponse)
		if err != nil {
			return false, updatedItemTokens, fmt.Errorf(tutil.GenerateResponse("Fail", err.Error()))
		}
		if !isVerified {
			return false, updatedItemTokens, fmt.Errorf(tutil.GenerateResponse("Fail", "authentication failed for the item"+updateAuthentication.ItemID))
		}
		newChallenge := util.GenerateUUID()
		storageItem.HandlerChallenge = newChallenge
		var token datastructs.StoredItemToken
		token.ItemID = storageItem.ID
		token.NextUpdateChallenge = newChallenge
		updatedItemTokens = append(updatedItemTokens, token)
		cache[updateAuthentication.ItemID] = storageItem
	}
	return true, updatedItemTokens, nil
}
