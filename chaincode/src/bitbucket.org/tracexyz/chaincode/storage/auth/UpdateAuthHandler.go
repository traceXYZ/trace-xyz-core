package auth

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"fmt"
	"bitbucket.org/tracexyz/chaincode/storage/handler"
	"bitbucket.org/tracexyz/core/rwledger"
)

func HandleTxnUpdates(stub shim.ChaincodeStubInterface, request datastructs.StoreTxnRequest) ([]datastructs.StoredItemToken,
	[]datastructs.StoredItemToken, error) {
	var itemUpdateAuthentications []datastructs.UpdateAuthentication
	var relationshipUpdateAuthentications []datastructs.UpdateAuthentication
	for _, sourceToItemEdge := range request.SourceToItemEdges {
		sourceToItemEdge.UpdateAuthentication.ItemID = sourceToItemEdge.Edge.SourceItemID
		relationshipUpdateAuthentications = append(relationshipUpdateAuthentications, sourceToItemEdge.UpdateAuthentication)
	}
	for _, itemToSourceEdge := range request.ItemToSourceEdges {
		itemToSourceEdge.UpdateAuthentication.ItemID = itemToSourceEdge.Edge.DestItemID
		relationshipUpdateAuthentications = append(relationshipUpdateAuthentications, itemToSourceEdge.UpdateAuthentication)
	}
	for _, update := range request.Updates {
		update.UpdateAuthentication.ItemID = update.ItemID
		itemUpdateAuthentications = append(itemUpdateAuthentications, update.UpdateAuthentication)
	}
	fmt.Printf("READY FOR ITEM UPDATES AUTH\n")
	var storageItemCache = make(map[string]datastructs.StorageItem)
	isVerified, itemUpdateTokens, err := AuthenticateItemUpdates(stub, itemUpdateAuthentications, storageItemCache)
	if err != nil {
		return nil, nil, err
	}
	fmt.Printf("ITEM UPDATES AUTH")
	if !isVerified {
		return nil, nil, fmt.Errorf("ownership updates not verified")
	}
	isVerified, relationshipUpdateTokens, err := AuthenticateRelationshipUpdates(stub, relationshipUpdateAuthentications, storageItemCache)
	if err != nil {
		return nil, nil, err
	}
	if !isVerified {
		return nil, nil, fmt.Errorf("relationship updates not verified")
	}

	fmt.Printf("%v \n", "PASS 4")
	if request.Updates != nil {
		var success bool
		success, err := handler.StoreUpdates(stub, request.Updates, storageItemCache)
		if err != nil {
			return nil, nil, err
		}
		if !success {
			return nil, nil, fmt.Errorf("cannot store updates in storage network. check graph updates mapping")
		}
	}
	for _, storageItem := range storageItemCache {
		success, err := rwledger.CreateOrUpdate(stub, storageItem.ID, storageItem)
		if err != nil {
			return nil, nil, err
		}
		if !success {
			return nil, nil, fmt.Errorf("cannot store updates in storage network. check graph updates mapping")
		}
	}

	return itemUpdateTokens, relationshipUpdateTokens, nil
}

func HandleDirectUpdates(stub shim.ChaincodeStubInterface, updates []datastructs.StorageUpdate) ([]datastructs.StoredItemToken, error) {

	var updateAuthentications = make([]datastructs.UpdateAuthentication, len(updates))
	for index, update := range updates {
		updateAuthentications[index] = update.UpdateAuthentication
	}
	var storageItemCache = make(map[string]datastructs.StorageItem)
	isVerified, tokens, err := AuthenticateItemUpdates(stub, updateAuthentications, storageItemCache)
	if err != nil {
		return tokens, err
	}
	if !isVerified {
		return tokens, fmt.Errorf("cannot store a graph nodes in storage network. check graph node mapping")
	}

	success, err := handler.StoreUpdates(stub, updates, storageItemCache)
	if err != nil {
		return tokens, err
	}
	if !success {
		return tokens, fmt.Errorf("cannot store a graph nodes in storage network. check graph node mapping")
	}
	return tokens, nil
}
