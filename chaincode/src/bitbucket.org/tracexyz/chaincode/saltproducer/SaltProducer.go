/*
TraceXYZ SaltProducer Chaincode
*/

package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/supplychain"
	"bitbucket.org/tracexyz/core/tutil"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
)

type SaltProducer struct {
}

var saltProductionSpecID = "SPS001"
var saltSellingSpecID = "SSS001"

// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(SaltProducer))
	if err != nil {
		fmt.Printf("Error starting TraceXZ chaincode - %s", err)
	}
}

// ============================================================================================================================
// Init - initialize the chaincode
//
// ============================================================================================================================
func (t *SaltProducer) Init(stub shim.ChaincodeStubInterface) pb.Response {
	_, args := stub.GetFunctionAndParameters()
	if len(args) < 2 {
		return shim.Error(tutil.GenerateResponse("Fail", "not enough arguments to init chaincode"))
	}
	if args[0] == "" || args[1] == "" {
		return shim.Error(tutil.GenerateResponse("Fail", "salt producer default specIDs must be specified in order to initialize chaincode"))
	}
	saltProductionSpecID = args[0]
	saltSellingSpecID = args[1]
	return shim.Success([]byte("Success"))
}

// ============================================================================================================================
// Invoke - Our entry point for Invocations
// SaltProducer Chaincode is used to perform following functions
// *Produce salt Item
// *Sell salt Item
// *Read a quota value
// ============================================================================================================================
func (t *SaltProducer) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Printf("F %v \n", function)
	fmt.Printf("A %v \n", args)
	// Handle different functions
	switch function {
	case "init":
		return t.Init(stub)
	case "produce_salt":
		fmt.Printf("1%v", "PASS")
		if len(args) < 1 {
			return shim.Error(tutil.GenerateResponse("Fail",
				"minimum of 1 arguments required to execute this function"))
		}
		return t.ProduceSalt(stub, args[0])
	case "sell_salt":
		if len(args) < 1 {
			return shim.Error(tutil.GenerateResponse("Fail",
				"minimum of 1 arguments required to execute this function"))
		}
		return t.SellSalt(stub, args[0])
	}
	fmt.Println("Received unknown invoke function name - " + function)
	return shim.Error("Received unknown invoke function name - '" + function + "'")
}

type SaltProductionRequest struct {
	SaltProductionSpecID           string `json:"salt_production_spec_id"`
	SaltProducerPublicKey          string `json:"salt_producer_public_key"`
	SaltItem                       []byte `json:"salt_item"`
	SaltItemSignature              []byte `json:"salt_item_signature"`
	SaltProductionProcess          []byte `json:"salt_production_process"`
	SaltProductionProcessSignature []byte `json:"salt_production_process_signature"`
}

// ============================================================================================================================
// Register - Smart Contract for Registering a User
// ============================================================================================================================
func (t *SaltProducer) ProduceSalt(stub shim.ChaincodeStubInterface, request string) pb.Response {
	var requestObj SaltProductionRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		fmt.Printf("\n ERR %v \n", err)
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var createItemRequest datastructs.CreateItemRequest
	if requestObj.SaltProductionSpecID != "" {
		createItemRequest.SpecificationID = requestObj.SaltProductionSpecID
	} else {
		createItemRequest.SpecificationID = saltProductionSpecID
	}
	createItemRequest.CreatedItem.Ownership.AuthorityPublicKey = requestObj.SaltProducerPublicKey
	createItemRequest.CreatedItem.Ownership.PayloadSignature = requestObj.SaltItemSignature
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.SaltProducerPublicKey
	handlerAuthority.PayloadSignature = requestObj.SaltItemSignature
	createItemRequest.CreatedItem.Handlers = append(createItemRequest.CreatedItem.Handlers, handlerAuthority)
	fmt.Printf("\n MI SIGN %v \n", requestObj.SaltItemSignature)
	createItemRequest.CreatedItem.PAYLOAD = requestObj.SaltItem
	fmt.Printf("\n SALT ITEM SIGN %v \n", requestObj.SaltItem)
	createItemRequest.CreationProcess.Ownership.AuthorityPublicKey = requestObj.SaltProducerPublicKey
	createItemRequest.CreationProcess.Ownership.PayloadSignature = requestObj.SaltProductionProcessSignature

	var processHandlerAuthority datastructs.HandlerAuthority
	processHandlerAuthority.AuthorityPublicKey = requestObj.SaltProducerPublicKey
	processHandlerAuthority.PayloadSignature = requestObj.SaltProductionProcessSignature
	createItemRequest.CreationProcess.Handlers = append(createItemRequest.CreationProcess.Handlers, processHandlerAuthority)
	createItemRequest.CreationProcess.PAYLOAD = requestObj.SaltProductionProcess
	fmt.Printf("\n MPP SIGN %v \n", requestObj.SaltProductionProcessSignature)
	createItemRequestBytes, err := json.Marshal(createItemRequest)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.CreateItem(stub, string(createItemRequestBytes))
}

type SaltSellingRequest struct {
	SaltSellingSpecID           string                     `json:"salt_selling_spec_id"`
	SellingSourceOwnershipToken datastructs.InputItemToken `json:"selling_source_ownership_token"`
	SellingSourceHandlingToken  datastructs.InputToken     `json:"selling_source_handling_token"`
	SellingQuantity             []byte                     `json:"selling_quantity_object"`
	SaltSellerPublicKey         string                     `json:"salt_seller_public_key"`
	SellerSignatureForQty       []byte                     `json:"seller_signature_for_qty"`
	SaltBuyerPublicKey          string                     `json:"salt_buyer_public_key"`
	BuyerSignatureForQty        []byte                     `json:"buyer_signature_for_qty"`
	SaltSellingProcess          []byte                     `json:"salt_selling_process"`
	SaltSellingProcessSignature []byte                     `json:"salt_selling_process_signature"`
}

// ============================================================================================================================
// QuotaUpdate - Smart Contract for Updating a resource quota of a user
// ============================================================================================================================
func (t *SaltProducer) SellSalt(stub shim.ChaincodeStubInterface, request string) pb.Response {
	fmt.Printf("\n Salt Selling request :%v", string(request))
	var requestObj SaltSellingRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	var sellingRequest datastructs.SellingRequest
	if requestObj.SaltSellingSpecID != "" {
		sellingRequest.SpecificationID = requestObj.SaltSellingSpecID
	} else {
		sellingRequest.SpecificationID = saltSellingSpecID
	}
	sellingRequest.SellingItem = requestObj.SellingSourceOwnershipToken
	sellingRequest.SellingRelationship = requestObj.SellingSourceHandlingToken
	sellingRequest.SaleAgreement.SellerPublicKey = requestObj.SaltSellerPublicKey
	sellingRequest.SaleAgreement.BuyerPublicKey = requestObj.SaltBuyerPublicKey
	sellingRequest.SaleAgreement.SellerSignatureForSoldQty = requestObj.SellerSignatureForQty
	sellingRequest.SaleAgreement.BuyerSignatureForSoldQty = requestObj.BuyerSignatureForQty
	sellingRequest.SaleAgreement.Item = requestObj.SellingQuantity

	sellingRequest.SoldQuantity.Ownership.AuthorityPublicKey = requestObj.SaltBuyerPublicKey
	sellingRequest.SoldQuantity.Ownership.PayloadSignature = requestObj.BuyerSignatureForQty
	var handlerAuthority datastructs.HandlerAuthority
	handlerAuthority.AuthorityPublicKey = requestObj.SaltBuyerPublicKey
	handlerAuthority.PayloadSignature = requestObj.BuyerSignatureForQty
	sellingRequest.SoldQuantity.Handlers = append(sellingRequest.SoldQuantity.Handlers, handlerAuthority)
	sellingRequest.SoldQuantity.PAYLOAD = requestObj.SellingQuantity

	sellingRequest.SellingProcess.Ownership.AuthorityPublicKey = requestObj.SaltSellerPublicKey
	sellingRequest.SellingProcess.Ownership.PayloadSignature = requestObj.SaltSellingProcessSignature
	var processHandlerAuthority datastructs.HandlerAuthority
	processHandlerAuthority.AuthorityPublicKey = requestObj.SaltSellerPublicKey
	processHandlerAuthority.PayloadSignature = requestObj.SaltSellingProcessSignature
	sellingRequest.SellingProcess.Handlers = append(sellingRequest.SellingProcess.Handlers, processHandlerAuthority)
	sellingRequest.SellingProcess.PAYLOAD = requestObj.SaltSellingProcess

	sellingRequestBytes, err := json.Marshal(sellingRequest)
	fmt.Printf("\n Selling request :%v", string(sellingRequestBytes))
	if err != nil {
		return shim.Error(tutil.GenerateResponse("Fail", err.Error()))
	}
	return supplychain.SellRawMaterial(stub, string(sellingRequestBytes))
}
