package supplychain

import (
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/comm"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"encoding/json"
	"github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/supplychain/validation"
	"fmt"
)

func MergeIntoContainer(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var requestObj datastructs.MergeIntoContainerRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Read the spec from storage network
	specStorageItem, err := comm.ReadStorageItemDirect(stub, requestObj.SpecificationID)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Validate the spec
	success, err := validation.ValidateInputsWithSpec(stub, specStorageItem.PAYLOAD, request)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !success {
		return shim.Error("the request fails to comply with the specification ID:" + requestObj.SpecificationID)
	}
	// Update the quota
	// todo: get the quota update token

	generatedProcessID := util.GenerateUUID()
	generatedPackageID := util.GenerateUUID()

	processNode := graph.NewNode(generatedProcessID, "MergeProcessData")
	packageNode := graph.NewNode(generatedPackageID, "Container")

	specRelationship := graph.NewEdge(requestObj.SpecificationID, generatedProcessID, relationships.Specification, states.Active)

	tempProcessToItemRelationship := graph.NewEdge(generatedProcessID, generatedPackageID, relationships.PublicTrace, states.Active)

	var itemToProcessEdgeUpdates []datastructs.EdgeUpdate

	for _, inputToken := range requestObj.InputItems {
		isAlreadyInsidePackage, err := CheckIfInsidePackage(stub, inputToken.ItemID)
		if err != nil {
			return shim.Error(err.Error())
		}
		if isAlreadyInsidePackage {
			return shim.Error("the item is already inside a package! ID" + inputToken.ItemID)
		}

		itemToProcessEdge := graph.NewEdge(inputToken.ItemID, generatedProcessID, relationships.PublicTrace, states.Active)
		var itemToProcessEdgeUpdate datastructs.EdgeUpdate
		itemToProcessEdgeUpdate.Edge = itemToProcessEdge
		itemToProcessEdgeUpdate.UpdateAuthentication.ChallengeResponse = inputToken.InputChallengeResponse
		itemToProcessEdgeUpdates = append(itemToProcessEdgeUpdates, itemToProcessEdgeUpdate)
	}

	requestObj.Container.ID = generatedPackageID
	requestObj.MergingProcess.ID = generatedProcessID
	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = []datastructs.Node{processNode, packageNode}
	storageRequest.Edges = []datastructs.Edge{tempProcessToItemRelationship}
	storageRequest.SourceToItemEdges = itemToProcessEdgeUpdates
	storageRequest.Items = []datastructs.StorageItem{requestObj.Container}
	storageRequest.Process.ProcessData = requestObj.MergingProcess
	storageRequest.Process.ProcessSpecRelationship = specRelationship

	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	// todo: commit the creation item

	var response datastructs.MergeItemsResponse
	response.StorageNetworkResponse = storageNWResponse
	response.InputItems = requestObj.InputItems
	response.ProcessID = generatedProcessID
	response.ProcessSpecID = requestObj.SpecificationID
	response.ContainerID = generatedPackageID
	response.SupplyChainResponse.ResponseType = "merge"
	response.SupplyChainResponse.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(responseBytes)
}

func CheckIfInsidePackage(stub shim.ChaincodeStubInterface, itemID string) (bool, error) {
	var assetRetrievalRequest datastructs.AssetRetrieveRequest
	assetRetrievalRequest.ItemID = itemID
	assetRetrievalRequest.RetrieveType = datastructs.CustomWalk
	assetRetrievalRequest.WalkPath = []relationships.GraphRelationshipType{relationships.PublicTrace}
	assetRetrievalRequest.CustWalkIncludeFuture = true
	assetRetrievalRequest.CustWalkIncludeHistory = false
	assetsRetrieved, err := comm.RetrieveWithRelationships(stub, assetRetrievalRequest)
	fmt.Printf("Assets A: %v", assetsRetrieved)
	var isInsidePackage = false
	if err != nil {
		fmt.Printf("Err A: %v", err)
		isInsidePackage = false
	}
	for _, node := range assetsRetrieved.Nodes {
		if node.ItemType == "MergeProcessData" {
			isInsidePackage = true
		}
	}
	if isInsidePackage {
		assetRetrievalRequest.WalkPath = []relationships.GraphRelationshipType{relationships.PublicTrace,
			relationships.PublicTrace, relationships.PublicTrace}
		assetsRetrieved, err := comm.RetrieveWithRelationships(stub, assetRetrievalRequest)
		fmt.Printf("Assets B: %v", assetsRetrieved)
		if err != nil {
			fmt.Printf("Err B: %v", err)
			isInsidePackage = true
		}
		for _, node := range assetsRetrieved.Nodes {
			if node.ItemType == "SplitProcessData" {
				isInsidePackage = false
			}
		}

	}
	return isInsidePackage, nil
}
