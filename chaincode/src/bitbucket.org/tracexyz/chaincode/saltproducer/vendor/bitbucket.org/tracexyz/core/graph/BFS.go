package graph

import (
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/golang-collections/collections/queue"
)

// Returns all the discovered nodes after performing a breadth first search
func BFS(stub shim.ChaincodeStubInterface, itemID string, relationshipType relationships.GraphRelationshipType) ([]datastructs.Node, []datastructs.Edge, error) {
	var nodesInPath []datastructs.Node
	var visitedMap = make(map[string]bool)
	queueObj := queue.New()

	rootNode, isAvailable, err := GetNode(stub, itemID)

	if !isAvailable {
		return nil, nil, fmt.Errorf("cannot find the root node")
	}
	if err != nil {
		return nil, nil, err
	}
	queueObj.Enqueue(rootNode)
	for queueObj.Len() > 0 {
		currentNode := queueObj.Dequeue().(datastructs.Node)
		if visitedMap[currentNode.ItemID] {
			continue
		}
		nodesInPath = append(nodesInPath, currentNode)
		nodesD, _, err := GetDestinationsList(stub, itemID, relationshipType)
		if err != nil {
			return nil, nil, err
		}
		for _, node := range nodesD {
			if visitedMap[node.ItemID] {
				continue
			}
			queueObj.Enqueue(node)
		}
		nodesS, _, err := GetSourcesList(stub, itemID, relationshipType)
		if err != nil {
			return nil, nil, err
		}
		for _, node := range nodesS {
			if visitedMap[node.ItemID] {
				continue
			}
			queueObj.Enqueue(node)
		}
		visitedMap[currentNode.ItemID] = true
	}
	return nodesInPath, nil, nil
}
