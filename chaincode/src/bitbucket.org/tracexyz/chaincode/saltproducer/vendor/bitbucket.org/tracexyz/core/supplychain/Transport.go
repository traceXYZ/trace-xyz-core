package supplychain

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
	"bitbucket.org/tracexyz/core/comm"
	"bitbucket.org/tracexyz/core/supplychain/validation"
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
)

func TransportItem(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var requestObj datastructs.TransportRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Read the spec from storage network
	specStorageItem, err := comm.ReadStorageItemDirect(stub, requestObj.SpecificationID)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Validate the spec
	success, err := validation.ValidateInputsWithSpec(stub, specStorageItem.PAYLOAD, request)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !success {
		return shim.Error("the request fails to comply with the specification ID:" + requestObj.SpecificationID)
	}

	//Create the items for storage

	generatedProcessID := util.GenerateUUID()
	processNode := graph.NewNode(generatedProcessID, "TransportProcess")

	specRelationship := graph.NewEdge(requestObj.SpecificationID, generatedProcessID, relationships.Specification, states.Active)

	itemToProcessEdge := graph.NewEdge(requestObj.ItemID, generatedProcessID, relationships.PublicTrace, states.Active)
	var itemToProcessEdgeUpdate datastructs.EdgeUpdate
	itemToProcessEdgeUpdate.Edge = itemToProcessEdge
	itemToProcessEdgeUpdate.UpdateAuthentication.ItemID = requestObj.ItemID
	itemToProcessEdgeUpdate.UpdateAuthentication.ChallengeResponse = requestObj.RelationshipChallengeResponse
	processToItemEdge := graph.NewEdge(generatedProcessID, requestObj.ItemID, relationships.PublicTrace, states.Active)
	var processToItemEdgeUpdate datastructs.EdgeUpdate
	processToItemEdgeUpdate.Edge = processToItemEdge
	processToItemEdgeUpdate.UpdateAuthentication.ItemID = requestObj.ItemID
	processToItemEdgeUpdate.UpdateAuthentication.ChallengeResponse = requestObj.RelationshipChallengeResponse

	requestObj.TransportProcess.ID = generatedProcessID
	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = []datastructs.Node{processNode}
	storageRequest.ItemToSourceEdges = []datastructs.EdgeUpdate{processToItemEdgeUpdate}
	storageRequest.SourceToItemEdges = []datastructs.EdgeUpdate{itemToProcessEdgeUpdate}
	storageRequest.Process.ProcessData = requestObj.TransportProcess
	storageRequest.Process.ProcessSpecRelationship = specRelationship

	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	// todo: commit the creation item

	var response datastructs.TransportResponse
	response.StorageNetworkResponse = storageNWResponse
	response.ProcessID = generatedProcessID
	response.ProcessSpecID = requestObj.SpecificationID
	response.SupplyChainResponse.ResponseType = "transport"
	response.SupplyChainResponse.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(responseBytes)
}
