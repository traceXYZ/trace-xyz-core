package supplychain

import (
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"bitbucket.org/tracexyz/core/comm"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"encoding/json"
	"github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/supplychain/validation"
)

func DivideItem(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var requestObj datastructs.SplitItemRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Read the spec from storage network
	specStorageItem, err := comm.ReadStorageItemDirect(stub, requestObj.SpecificationID)
	if err != nil {
		return shim.Error(err.Error())
	}
	success, err := validation.ValidateInputsWithSpec(stub, specStorageItem.PAYLOAD, request)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !success {
		return shim.Error("the request fails to comply with the specification ID:" + requestObj.SpecificationID)
	}
	// Update the quota
	// todo: get the quota update token

	//Create the items for storage

	generatedProcessID := util.GenerateUUID()

	processNode := graph.NewNode(generatedProcessID, "DivideProcess")

	specRelationship := graph.NewEdge(requestObj.SpecificationID, generatedProcessID, relationships.Specification, states.Active)
	cuttingItemToProcessRelationship := graph.NewEdge(requestObj.ItemToken.ItemID, generatedProcessID, relationships.PublicTrace, states.Active)
	var cuttingItemToProcessRelationshipUpdate datastructs.EdgeUpdate
	cuttingItemToProcessRelationshipUpdate.Edge = cuttingItemToProcessRelationship
	cuttingItemToProcessRelationshipUpdate.UpdateAuthentication.ChallengeResponse = requestObj.RelationshipToken.InputChallengeResponse

	var packageItemNodes []datastructs.Node
	var processToPackageEdges []datastructs.Edge
	var packagedItems []datastructs.StorageItem
	var splitQty float64
	for _, packagedItem := range requestObj.SplitItems {
		var pack = packagedItem
		pack.ID = util.GenerateUUID()
		packageNode := graph.NewNode(pack.ID, "Item")
		processToPackageEdge := graph.NewEdge(generatedProcessID, pack.ID, relationships.PublicTrace, states.Active)
		packageItemNodes = append(packageItemNodes, packageNode)
		processToPackageEdges = append(processToPackageEdges, processToPackageEdge)
		packagedItems = append(packagedItems, pack)
		var itemMap map[string]interface{}
		err = json.Unmarshal(pack.PAYLOAD, &itemMap)
		if err != nil {
			return shim.Error(err.Error())
		}
		packageQty, ok := itemMap["qty"].(float64)
		if !ok {
			return shim.Error("divided quantity must have 'qty' field defined")
		}
		splitQty += packageQty
	}

	// Generate update request for source item
	var update datastructs.StorageUpdate
	update.UpdateType = datastructs.Subtract
	update.ItemID = requestObj.ItemToken.ItemID
	//sellingQty, ok := requestObj.SoldQuantity.Item["qty"].(float64)
	update.UpdateValue = splitQty
	update.UpdatePath = "$.qty"
	update.UpdateAuthentication.ChallengeResponse = requestObj.ItemToken.InputChallengeResponse

	requestObj.SplittingProcess.ID = generatedProcessID

	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = append(packageItemNodes, processNode)
	storageRequest.Edges = processToPackageEdges
	storageRequest.SourceToItemEdges = []datastructs.EdgeUpdate{cuttingItemToProcessRelationshipUpdate}
	storageRequest.Items = packagedItems
	storageRequest.Process.ProcessData = requestObj.SplittingProcess
	storageRequest.Process.ProcessSpecRelationship = specRelationship

	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	// todo: commit the creation item

	var response datastructs.CreateItemResponse
	response.StorageNetworkResponse = storageNWResponse
	response.ItemID = requestObj.ItemToken.ItemID
	response.ProcessID = generatedProcessID
	response.ProcessSpecID = requestObj.SpecificationID
	response.SupplyChainResponse.ResponseType = "divide"
	response.SupplyChainResponse.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(responseBytes)
}
