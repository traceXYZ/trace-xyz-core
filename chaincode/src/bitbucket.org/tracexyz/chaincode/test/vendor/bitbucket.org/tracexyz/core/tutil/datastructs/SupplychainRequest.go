package datastructs

type CreateItemRequest struct {
	SpecificationID string      `json:"specification_id"`
	OwnerID         string      `json:"owner_id"`
	CreationProcess StorageItem `json:"creation_process"`
	CreatedItem     StorageItem `json:"created_item"`
}
