package datastructs

type UpdateType int

const (
	Add      UpdateType = 1
	Subtract UpdateType = 2
	Exact    UpdateType = 3
)

type User struct {
	Id        string `json:"id"`
	Enabled   bool   `json:"enabled"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	EmailAddr string `json:"email_addr"`
	NIC       string `json:"nic"`
}

type StorageItem struct {
	ID      string                 `json:"id"`
	PAYLOAD map[string]interface{} `json:"payload"`
}

type StorageUpdate struct {
	ItemID      string     `json:"item_id"`
	UpdatePath  string     `json:"update_path"`
	UpdateType  UpdateType `json:"update_type"`
	UpdateValue float64    `json:"update_value"`
}

type StoreTxnRequest struct {
	Nodes             []Node          `json:"nodes"`
	Edges             []Edge          `json:"edges"`
	SourceToItemEdges []Edge          `json:"source_to_item_edges"`
	ItemToSourceEdges []Edge          `json:"item_to_source_edges"`
	Items             []StorageItem   `json:"items"`
	Updates           []StorageUpdate `json:"updates"`
}

type RetrievalType string

const (
	ImmediateHistory RetrievalType = "ImmediateHistory"
	AllHistory       RetrievalType = "AllHistory"
	ImmediateFuture  RetrievalType = "ImmediateFuture"
	AllFuture        RetrievalType = "AllFuture"
	Immediate        RetrievalType = "Immediate"
	All              RetrievalType = "All"
	CustomWalk       RetrievalType = "CustomWalk"
)

type RetrieveRequest struct {
	ItemID           string        `json:"item_id"`
	RelationshipType string        `json:"relationship_type"`
	RetrieveType     RetrievalType `json:"retrieve_type"`
	WalkPath         []string      `json:"walk_path"`
}
