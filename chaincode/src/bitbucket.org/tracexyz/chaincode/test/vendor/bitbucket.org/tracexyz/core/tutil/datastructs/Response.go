package datastructs

type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type StoreTxnResponse struct {
	Response
	ItemIDs []string `json:"item_ids"`
	TxnID   string   `json:"txn_id"`
}

type GetSourcesResponse struct {
	Response
	Nodes        []Node        `json:"nodes"`
	Edges        []Edge        `json:"edges"`
	StorageItems []StorageItem `json:"storage_items"`
}

type DirectRetrieveResponse struct {
	Response
	StorageItem StorageItem `json:"storage_item"`
}

type CreateItemResponse struct {
	StorageNetworkResponse string `json:"storage_network_response"`
	ItemID                 string `json:"item_id"`
	ProcessID              string `json:"process_id"`
	ProcessSpecID          string `json:"process_spec_id"`
}

type SellItemResponse struct {
	StorageNetworkResponse string  `json:"storage_network_response"`
	SellingItemID          string  `json:"selling_item_id"`
	SellingQuantity        float64 `json:"selling_quantity"`
	SellingProcessID       string  `json:"selling_process_id"`
	SellingQuantityKey     string  `json:"selling_quantity_key"`
	SellingQuantityToken   string  `json:"selling_quantity_token"`
}
