/*
TraceXYZ MilkProducer Chaincode
*/

package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"github.com/hyperledger/fabric/bccsp"
	"github.com/hyperledger/fabric/bccsp/factory"
	"github.com/pkg/errors"
	"encoding/pem"
	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
)

type TestChaincode struct {
	bccspInst bccsp.BCCSP
}

func main() {
	err := shim.Start(new(TestChaincode))
	if err != nil {
		fmt.Printf("Error starting TraceXZ chaincode - %s", err)
	}
}

func (t *TestChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	t.bccspInst = factory.GetDefault()
	return shim.Success([]byte("Success"))
}

func (t *TestChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Printf("F %v \n", function)
	fmt.Printf("A %v \n", args)
	// Handle different functions
	switch function {
	case "init":
		return t.Init(stub)
	case "test":
		if len(args) < 2 {
			return shim.Error("ERR")
		}
		var msgSign = []byte(args[0])
		fmt.Printf("SIGNP %v \n", msgSign)
		var msgPK = []byte(args[1])
		fmt.Printf("PKEYP %v \n", msgPK)

		c, _ := cid.GetX509Certificate(stub)
		fmt.Printf("KEYA %v \n", c.PublicKey)
		verif, err := Verify(msgPK, []byte("ABCDEF"), msgSign)

		return shim.Success([]byte(fmt.Sprintf("SIGN:%v \nPK:%v \nVERIF:%v \nERR:%v", msgSign, msgPK, verif, err)))
	}
	fmt.Println("Received unknown invoke function name - " + function)
	return shim.Error("Received unknown invoke function name - '" + function + "'")
}

func Verify(publicKey, msg, signature []byte) (bool, error) {
	bccspInst := factory.GetDefault()
	key, _ := pem.Decode(publicKey)
	if len(key.Bytes) == 0 {
		return false, fmt.Errorf("public key decode failed")
	}

	signKey, err := bccspInst.KeyImport(key.Bytes, &bccsp.ECDSAPKIXPublicKeyImportOpts{Temporary: true})

	if err != nil {
		return false, errors.WithMessage(err, "bccspInst.KeyImport failed")
	}

	if err != nil {
		return false, errors.WithMessage(err, "sign error: bccsp.Hash returned")
	}
	return bccspInst.Verify(signKey, signature, msg, nil)
}
