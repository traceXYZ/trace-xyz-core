package validation

import (
	"github.com/oliveagle/jsonpath"
)

func processSpecRatio(checkingSpec, checkingValue interface{}) (bool, error) {
	checkingSpecList, ok := checkingSpec.([]interface{})
	if !ok {
		return false, nil
	}
	var retValue = true
	for _, specItemIter := range checkingSpecList {
		specItem, ok := specItemIter.(map[string]interface{})
		if !ok {
			return false, nil
		}
		var eValuePaths = specItem["field_paths"]
		var eRatio = specItem["ratio"]
		var eTolerance = specItem["tolerance"]
		var payloadPath = specItem["payload_path"]
		if eValuePaths == nil || eRatio == nil || eTolerance == nil || payloadPath == nil {
			return false, nil
		}
		eValuePathsL, ok := eValuePaths.([]interface{})
		if !ok || len(eValuePathsL) != 2 {
			return false, nil
		}
		eRatioF, ok := eRatio.(float64)
		if !ok {
			return false, nil
		}
		eToleranceF, ok := eTolerance.(float64)
		if !ok {
			return false, nil
		}
		eValuePathsL0S, ok := eValuePathsL[0].(string)
		if !ok {
			return false, nil
		}

		checkingItemMap, err := extractPayloadMap(checkingValue, payloadPath)
		if err != nil {
			return false, err
		}

		retValue1, err1 := jsonpath.JsonPathLookup(checkingItemMap, eValuePathsL0S)
		if err1 != nil {
			return false, err1
		}
		retValue1F, err1 := addFloatArrayByInterface(retValue1)
		if err1 != nil {
			return false, err1
		}

		eValuePathsL1S, ok := eValuePathsL[1].(string)
		if !ok {
			return false, nil
		}

		retValue2, err2 := jsonpath.JsonPathLookup(checkingItemMap, eValuePathsL1S)
		if err2 != nil {
			return false, err2
		}
		retValue2F, err2 := addFloatArrayByInterface(retValue2)
		if err2 != nil {
			return false, err1
		}

		rRatio := retValue1F / retValue2F
		retValue = retValue && eRatioF-eToleranceF < rRatio && eRatioF+eToleranceF > rRatio
	}
	return retValue, nil
}
