package validation

import (
	"fmt"
	"reflect"
	"github.com/oliveagle/jsonpath"
	"encoding/json"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
)

func addFloatArrayByInterface(valueArray interface{}) (float64, error) {
	var retValueF float64
	valueAbs, ok := valueArray.(float64)
	if ok {
		return valueAbs, nil
	}
	valueArrayA, ok := valueArray.([]interface{})
	if !ok {
		return 0, fmt.Errorf("interface is not an array")
	}
	if len(valueArrayA) < 1 {
		return 0, fmt.Errorf("empty array found in results")
	}
	for _, value := range valueArrayA {
		retValue, ok := value.(float64)
		if !ok {
			return 0, fmt.Errorf("the value is not numeric")
		}
		retValueF += retValue
	}
	return retValueF, nil
}

func validateField(operation string, eValue, rValue interface{}) bool {
	if reflect.TypeOf(eValue) != reflect.TypeOf(rValue) {
		return false
	}
	switch operation {
	case ">":
		rValueF, ok := rValue.(float64)
		if !ok {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF > eValueF
	case ">=":
		rValueF, ok := rValue.(float64)
		if !ok {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF >= eValueF
	case "<":
		rValueF, ok := rValue.(float64)
		if !ok {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF < eValueF
	case "<=":
		rValueF, ok := rValue.(float64)
		if !ok {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF <= eValueF
	case "=":
		rValueI, ok := rValue.(int64)
		if !ok {
			return false
		}
		eValueI, ok := eValue.(int64)
		if !ok {
			return false
		}
		return rValueI == eValueI
	case "EQ":
		rValueS, ok := rValue.(string)
		if !ok {
			return false
		}
		eValueS, ok := eValue.(string)
		if !ok {
			return false
		}
		return rValueS == eValueS
	default:
		return false
	}
}

func validateFieldFiltered(operation string, eValue, rValue interface{}) bool {
	switch operation {
	case ">":
		rValueF, err := addFloatArrayByInterface(rValue)
		if err != nil {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF > eValueF
	case ">=":
		rValueF, err := addFloatArrayByInterface(rValue)
		if err != nil {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF >= eValueF
	case "<":
		rValueF, err := addFloatArrayByInterface(rValue)
		if err != nil {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF > eValueF
	case "<=":
		rValueF, err := addFloatArrayByInterface(rValue)
		if err != nil {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF <= eValueF
	case "=":
		rValueF, err := addFloatArrayByInterface(rValue)
		if err != nil {
			return false
		}
		eValueF, ok := eValue.(float64)
		if !ok {
			return false
		}
		return rValueF == eValueF
	case "EQ":
		rValueS, ok := rValue.([]string)
		if !ok || len(rValueS) > 0 {
			return false
		}
		eValueS, ok := eValue.(string)
		if !ok {
			return false
		}
		return rValueS[0] == eValueS
	default:
		return false
	}
}

func extractPayloadMap(request, itemPath interface{}) (map[string]interface{}, error) {
	itemPathS, ok := itemPath.(string)
	if !ok {
		return nil, fmt.Errorf("item path cannot be found in the request")
	}
	storageItem, err := jsonpath.JsonPathLookup(request, itemPathS)
	fmt.Printf("Storage Item From path : %v \n", storageItem)
	fmt.Printf("Item Path : %v \n", itemPathS)
	if err != nil {
		fmt.Printf("ERR JSN : %v \n", err)
		return nil, err
	}
	storageItemBytes, err := json.Marshal(storageItem)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Storage Item Extracted : %v \n", string([]byte(storageItemBytes)))
	var storageItemObj datastructs.StorageItem
	err = json.Unmarshal([]byte(storageItemBytes), &storageItemObj)
	if err != nil {
		return nil, err
	}
	var checkingItemMap map[string]interface{}
	err = json.Unmarshal(storageItemObj.PAYLOAD, &checkingItemMap)
	if err != nil {
		return nil, err
	}
	return checkingItemMap, nil
}
