package supplychain

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"encoding/json"
	"bitbucket.org/tracexyz/core/comm"
	"bitbucket.org/tracexyz/core/supplychain/validation"
	"github.com/hyperledger/fabric/common/util"
	"bitbucket.org/tracexyz/core/graph"
	"bitbucket.org/tracexyz/core/tutil/datastructs/relationships"
	"bitbucket.org/tracexyz/core/tutil/datastructs/states"
)

func SplitFromContainer(stub shim.ChaincodeStubInterface, request string) peer.Response {
	// Parse the request
	var requestObj datastructs.SplitFromContainerRequest
	err := json.Unmarshal([]byte(request), &requestObj)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Read the spec from storage network
	specStorageItem, err := comm.ReadStorageItemDirect(stub, requestObj.SpecificationID)
	if err != nil {
		return shim.Error(err.Error())
	}
	// Validate the spec
	success, err := validation.ValidateInputsWithSpec(stub, specStorageItem.PAYLOAD, request)
	if err != nil {
		return shim.Error(err.Error())
	}
	if !success {
		return shim.Error("the request fails to comply with the specification ID:" + requestObj.SpecificationID)
	}
	// Update the quota
	// todo: get the quota update token

	isInside, err := CheckIfInsidePackage(stub, requestObj.Container.ItemID)
	if err != nil {
		return shim.Error(err.Error())
	}
	if isInside {
		return shim.Error("the container cannot be split because its inside another container")
	}

	//Create the items for storage

	generatedProcessID := util.GenerateUUID()

	processNode := graph.NewNode(generatedProcessID, "SplitProcess")

	specRelationship := graph.NewEdge(requestObj.SpecificationID, generatedProcessID, relationships.Specification, states.Active)
	containerToProcessRelationship := graph.NewEdge(requestObj.Container.ItemID, generatedProcessID, relationships.PublicTrace, states.Active)
	var containerToProcessRelationshipUpdate datastructs.EdgeUpdate
	containerToProcessRelationshipUpdate.Edge = containerToProcessRelationship
	containerToProcessRelationshipUpdate.UpdateAuthentication.ChallengeResponse = requestObj.Container.InputChallengeResponse

	var processToSourcePackageUpdates []datastructs.EdgeUpdate

	requestObj.SplittingProcess.ID = generatedProcessID
	for _, packagedItem := range requestObj.OutputItems {
		processToPackageEdge := graph.NewEdge(generatedProcessID, packagedItem.ItemID, relationships.PublicTrace, states.Active)
		var processToPackageEdgeUpdate datastructs.EdgeUpdate
		processToPackageEdgeUpdate.Edge = processToPackageEdge
		processToPackageEdgeUpdate.UpdateAuthentication.ItemID = packagedItem.ItemID
		processToPackageEdgeUpdate.UpdateAuthentication.ChallengeResponse = packagedItem.InputChallengeResponse
		processToSourcePackageUpdates = append(processToSourcePackageUpdates, processToPackageEdgeUpdate)
	}

	var storageRequest datastructs.StoreTxnRequest
	storageRequest.Nodes = []datastructs.Node{processNode}
	storageRequest.ItemToSourceEdges = processToSourcePackageUpdates
	storageRequest.SourceToItemEdges = []datastructs.EdgeUpdate{containerToProcessRelationshipUpdate}
	storageRequest.Process.ProcessData = requestObj.SplittingProcess
	storageRequest.Process.ProcessSpecRelationship = specRelationship

	storageNWResponse, err := comm.StoreTxn(stub, storageRequest)
	if err != nil {
		return shim.Error(err.Error())
	}
	// todo: commit the creation item

	var response datastructs.SplitItemsResponse
	response.StorageNetworkResponse = storageNWResponse
	response.OutputItems = requestObj.OutputItems
	response.ProcessID = generatedProcessID
	response.ProcessSpecID = requestObj.SpecificationID
	response.Container = requestObj.Container
	response.SupplyChainResponse.ResponseType = "split"
	response.SupplyChainResponse.TxnID = stub.GetTxID()

	responseBytes, err := json.Marshal(response)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(responseBytes)
}

func CheckIfInsideContainer(stub shim.ChaincodeStubInterface, itemIDs []string, containerID string, ) (bool, error) {
	var assetRetrievalRequest datastructs.AssetRetrieveRequest
	assetRetrievalRequest.ItemID = containerID
	assetRetrievalRequest.RetrieveType = datastructs.CustomWalk
	assetRetrievalRequest.WalkPath = []relationships.GraphRelationshipType{relationships.PublicTrace, relationships.PublicTrace}
	assetRetrievalRequest.CustWalkIncludeFuture = false
	assetRetrievalRequest.CustWalkIncludeHistory = true
	assetsRetrieved, err := comm.RetrieveWithRelationships(stub, assetRetrievalRequest)
	if err != nil {
		return false, err
	}
	isAllInsideContainer := false
	for _, itemID := range itemIDs {
		isItemInsideContainer := false
		for _, node := range assetsRetrieved.Nodes {
			if node.ItemID == itemID {
				isItemInsideContainer = true
			}
		}
		isAllInsideContainer = isAllInsideContainer && isItemInsideContainer
	}
	return isAllInsideContainer, nil
}
