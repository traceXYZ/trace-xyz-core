package tutil

import (
	"encoding/json"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"fmt"
	"bitbucket.org/tracexyz/core/tutil/datastructs"
	"strings"
)

func GenerateResponse(status, message string) string {
	response := datastructs.Response{
		Status:  status,
		Message: message,
	}
	responseBytes, _ := json.Marshal(response)
	return string(responseBytes)
}

func GetInvokerID(stub shim.ChaincodeStubInterface) (string, error) {
	enrollmentID, err := cid.GetID(stub)
	if err != nil {
		return "", fmt.Errorf(err.Error())
	}
	mspID, err := cid.GetMSPID(stub)
	if err != nil {
		return "", fmt.Errorf(err.Error())
	}
	return mspID + ":" + enrollmentID, nil
}

func GetEnrollmentID(stub shim.ChaincodeStubInterface) (string, error) {
	enrollmentID, found, err := cid.GetAttributeValue(stub, "hf.EnrollmentID")
	if err != nil {
		return "", fmt.Errorf(GenerateResponse("Fail", err.Error()))
	}
	if !found {
		return "", fmt.Errorf(GenerateResponse("Fail", "please include hf.EnrollmentID attribute value when using the certificate"))
	}
	return enrollmentID, err
}

// ============================================================================================================================
// isAdmin - checks the request sender is an admin user
// ============================================================================================================================
func IsAdmin(stub shim.ChaincodeStubInterface) (bool, error) {
	affiliation, found, err := cid.GetAttributeValue(stub, "hf.Affiliation")
	if err != nil {
		return false, fmt.Errorf(GenerateResponse("Fail", err.Error()))
	}
	if !found {
		return false, fmt.Errorf(GenerateResponse("Fail", "please include hf.Affiliation attribute value when using the certificate"))
	}
	if strings.Split(affiliation, ".")[1] != "admin" {
		return false, fmt.Errorf(GenerateResponse("Fail", "the user is not enrolled as an admin"))
	}
	return true, nil
}

// ============================================================================================================================
// IsValidUser - checks the request sender is an admin or a user
// ============================================================================================================================
func IsValidUser(stub shim.ChaincodeStubInterface, userID string) (bool, error) {
	affiliation, found, err := cid.GetAttributeValue(stub, "hf.Affiliation")
	if err != nil {
		return false, fmt.Errorf(GenerateResponse("Fail", err.Error()))
	}
	if !found {
		return false, fmt.Errorf(GenerateResponse("Fail", "please include hf.Affiliation attribute value when using the certificate"))
	}
	isValid := false
	if strings.Split(affiliation, ".")[1] == "user" {
		isValid = true
	}
	if strings.Split(affiliation, ".")[1] == "admin" {
		return true, nil
	}

	enrollmentID, found, err := cid.GetAttributeValue(stub, "hf.EnrollmentID")
	if err != nil {
		return false, fmt.Errorf(GenerateResponse("Fail", err.Error()))
	}
	if !found {
		return false, fmt.Errorf(GenerateResponse("Fail", "please include hf.EnrollmentID attribute value when using the certificate"))
	}
	return (userID == enrollmentID) && isValid, nil
}

// ============================================================================================================================
// IsValidInvokerUser - checks the request sender is the real invoker
// ============================================================================================================================
func IsValidInvokerUser(stub shim.ChaincodeStubInterface, userID string) (bool, error) {
	enrollmentID, found, err := cid.GetAttributeValue(stub, "hf.EnrollmentID")
	if err != nil {
		return false, fmt.Errorf(GenerateResponse("Fail", err.Error()))
	}
	if !found {
		return false, fmt.Errorf(GenerateResponse("Fail", "please include hf.EnrollmentID attribute value when using the certificate"))
	}
	return userID == enrollmentID, nil
}
