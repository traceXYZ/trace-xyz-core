// ============================================================================================================================
// 													 Get config file fields
// ============================================================================================================================

module.exports = function (cp, logger) {
    var helper = {};

    // get the trace owner names
    helper.getTraceUsernamesConfig = function () {
        return cp.getTraceField('usernames');
    };

    // get the trace trading company name from config file
    helper.getCompanyNameFromFile = function () {
        return cp.getTraceField('company');
    };

    // get the trace's server port number
    helper.getTracePort = function () {
        return cp.getTraceField('port');
    };

    // get the status of trace previous startup
    helper.getEventsSetting = function () {
        if (cp.config['use_events']) {
            return cp.config['use_events'];
        }
        return false;
    };

    // get the re-enrollment period in seconds
    helper.getKeepAliveMs = function () {
        var sec = cp.getTraceField('keep_alive_secs');
        if (!sec) sec = 30;									// default to 30 seconds
        return (sec * 1000);
    };

    return helper;
};
