#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1

docker-compose -f docker-compose.yml down

docker-compose -f docker-compose.yml up -d ca.tracexyz.com orderer.tracexyz.com
docker-compose -f docker-compose.yml up -d peer0.org1.tracexyz.com peer1.org1.tracexyz.com peer2.org1.tracexyz.com peer3.org1.tracexyz.com peer4.org1.tracexyz.com peer5.org1.tracexyz.com peer6.org1.tracexyz.com peer7.org1.tracexyz.com peer8.org1.tracexyz.com peer9.org1.tracexyz.com

## wait for Hyperledger Fabric to start
## incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
#export FABRIC_START_TIMEOUT=10
##echo ${FABRIC_START_TIMEOUT}
#sleep ${FABRIC_START_TIMEOUT}
#
## Create the channel
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer0.org1.tracexyz.com peer channel create -o orderer.tracexyz.com:7050 -c cheeseproduction -f /etc/hyperledger/configtx/channel.tx
## Join peer0.org1.example.com to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer1.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer1.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer2.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer2.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer3.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer3.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer4.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer4.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer5.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer5.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer6.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer6.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer7.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer7.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer8.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer8.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block

docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" peer9.org1.tracexyz.com peer channel fetch config -o orderer.tracexyz.com:7050 -c cheeseproduction

docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.tracexyz.com/msp" -e "CORE_PEER_ADDRESS=peer9.org1.tracexyz.com:7051" peer0.org1.tracexyz.com peer channel join -b cheeseproduction.block
