var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");

var app = express();
app.use(cors());

var port = 8000;

var api = require("./routes/api");
var auth = require("./src/auth");

var x = require("./src/user");

app.use(bodyParser.json());

app.use("/api", api);

app.listen(port, function() {
  console.log("app started");
});

app.get("/ping", (req, res) => {
  res.send(new Date().toString());
});

app.post("/echo", (req, res) => {
  res.send(req.body);
});

app.post("/register", async (req, res) => {
  const user = await auth.registerUser(req.body.username, req.body.password);

  if (user) {
    res.status(201);
    res.send(user);
  } else {
    res.status(400);
    res.send();
  }
});

app.post("/login", async (req, res) => {
  const token = await auth.createToken(req.body.username, req.body.password);

  if (token) {
    res.status(202);
    res.send({ token });
  } else {
    res.status(400);
    res.send();
  }
});
