const FabricClient = require("fabric-client");
const util = require("util");

const hlfConfig = require("../config/hlf.config");

const db = require("./database");

let fabricClient;
let channel;

async function setup() {
  fabricClient = FabricClient.loadFromConfig(hlfConfig.connectionProfilePath);
  channel = fabricClient.getChannel();

  const stateStore = await FabricClient.newDefaultKeyValueStore({
    path: hlfConfig.keyStorePath
  });
  fabricClient.setStateStore(stateStore);

  const cryptoSuite = FabricClient.newCryptoSuite();
  const keyStore = FabricClient.newCryptoKeyStore({
    path: hlfConfig.keyStorePath
  });
  cryptoSuite.setCryptoKeyStore(keyStore);
  fabricClient.setCryptoSuite(cryptoSuite);
}

setup();

async function invoke(request, username) {
  await fabricClient.getUserContext(username, true);

  const txId = fabricClient.newTransactionID();

  const proposalRequest = {
    ...request, // chaincodeId, fcn, chainId, args <- data here
    txId
  };

  request.txId = txId.getTransactionID();
  request.username = username;
  await db.proposalRequest.insertOne(request);

  const [proposalResponses, proposal] = await channel.sendTransactionProposal(
    proposalRequest,
    25000
  );

  if (
    proposalResponses &&
    proposalResponses[0] &&
    proposalResponses[0].response.status === 200
  ) {
    const responsePayload = JSON.parse(proposalResponses[0].response.payload);

    const txIdString = responsePayload.txn_id;
    // await findProposalRequestObject(txIdString);

    var commitRequest = {
      proposalResponses: proposalResponses,
      proposal: proposal
    };
    await db.proposalResponse.insertOne(proposalResponse);

    let eventHub = fabricClient.newEventHub();
    eventHub.setPeerAddr(hlfConfig.eventHubPeerAddress);

    let txPromise = new Promise((resolve, reject) => {
      let handle = setTimeout(() => {
        eventHub.disconnect();
        reject({ status: "timeout" });
      }, 3000);

      eventHub.connect();
      eventHub.registerTxEvent(
        txIdString,
        (txIdS, code) => {
          clearTimeout(handle);
          eventHub.unregisterTxEvent(txIdS);
          eventHub.disconnect();

          const returnStatus = { txId: txIdS, code };

          if (code === "VALID") {
            resolve({ ...returnStatus, status: "success" });
          } else {
            reject({ ...returnStatus, status: "invalid" });
          }
        },

        err => reject({ status: "error", error: err })
      );
    });

    try {
      const [transactionResult, eventHubResult] = await Promise.all([
        channel.sendTransaction(commitRequest),
        txPromise
      ]);

      if (transactionResult.status !== "SUCCESS") {
        throw Error(transactionResult.status);
      }

      // TODO Process and store response as needed
    } catch (error) {
      if (error) {
        if (error.status === "error") {
          throw new Error("Event Hub error. ");
        } else if (error.status === "invalid") {
          throw new Error("Invalid event hub response. ");
        } else if (error.status === "timeout") {
          throw new Error("Event Hub timed out. ");
        } else {
          throw new Error(
            `Transaction error: ${error.status ||
              error.error ||
              error.message ||
              error}`
          );
        }
      } else {
        throw new Error("Unknown transaction error. ");
      }
    }
  } else {
    // Transaction proposal was not good
    throw new Error("Transaction proposal was bad. ");
  }
}

module.exports = invoke;
