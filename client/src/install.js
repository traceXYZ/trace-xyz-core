var util = require("util");
var winston = require("winston");
var logger = winston.createLogger({
  level: "debug",
  transports: [new winston.transports.Console({ colorize: true })]
});

var config_file = "trace_local.json";

var cp = require("../../utils/connection_profile_lib")(config_file, logger);
var fcw = require("../../utils/fc_wrangler")(
  { block_delay: cp.getBlockDelay() },
  logger
);

async function install(chaincode_id, chaincode_version, chaincode_location) {
  winston.info(
    "Starting to install chaincode ",
    chaincode_id,
    chaincode_version,
    chaincode_location
  );

  try {
    const enrollResponse = await util.promisify(fcw.enrollWithAdminCert)(
      cp.makeEnrollmentOptionsUsingCert()
    );

    const channelId = cp.getChannelId();
    const firstPeerName = cp.getFirstPeerName(channelId);

    const opts = {
      peer_urls: [cp.getPeersUrl(firstPeerName)],
      //path to chaincode from <trace root>/chaincode/src/
      path_2_chaincode: `bitbucket.org/tracexyz/chaincode/${chaincode_location}`,
      chaincode_id,
      chaincode_version,
      peer_tls_opts: cp.getPeerTlsCertOpts(firstPeerName)
    };

    const installResponse = await util.promisify(fcw.install_chaincode)(
      enrollResponse,
      opts
    );

    winston.info(
      "Installing chaincode succeeded ",
      chaincode_id,
      chaincode_version,
      chaincode_location
    );

    return { result: "success" };
  } catch (error) {
    logger.error(`Chaincode installation error: ${error}`);
    return { error: error.parsed };
  }
}

module.exports = install;
