const mongodb = require("mongodb");
const MongoClient = mongodb.MongoClient;

const databaseConfig = require("../../config/database.config");
let mongoClient = new MongoClient(databaseConfig.host);

async function getDb() {
  if (!mongoClient.isConnected()) {
    mongoClient = await MongoClient.connect(
      databaseConfig.host,
      { useNewUrlParser: true }
    );
  }

  return mongoClient.db(databaseConfig.database);
}

module.exports.getCollection = async function(collection) {
  const db = await getDb();
  return db.collection(collection);
};
