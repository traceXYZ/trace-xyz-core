const getDb = require("../connectionManager");

async function insertOne(name, uuid) {
  return await SpecificationId.create({ id: name, uuid });
}

async function findOne(name) {
  const spec = await SpecificationId.findById(name, { rejectOnEmpty: true });
  return spec.uuid;
}

module.exports = {
  insertOne,
  findOne
};
