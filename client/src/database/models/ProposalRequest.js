const db = require("../connectionManager");

async function insertOne(requestObj) {
  return await (await db.getCollection("proposalRequest")).insertOne(
    requestObj
  );
}

async function findOne(txId) {
  return await (await db.getCollection("proposalRequest")).findOne({ txId });
}

module.exports = {
  insertOne,
  findOne
};
