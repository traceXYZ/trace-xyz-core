const db = require("../connectionManager");

async function insertOne(responseObj) {
  return await (await db.getCollection("proposalRequest")).insertOne(
    responseObj
  );
}

async function findOne(txId) {
  return await (await db.getCollection("proposalRequest")).findOne({ txId });
}

module.exports = {
  insertOne,
  findOne
};
