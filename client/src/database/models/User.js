const db = require("../connectionManager");

async function insertOne(username, hash, salt) {
  return await (await db.getCollection("user")).insertOne({
    username,
    hash,
    salt
  });
}

async function findOne(username) {
  return await (await db.getCollection("user")).findOne({ username });
}

(async function() {
  await (await db.getCollection("user")).createIndex(
    { username: 1 },
    { unique: true }
  );
})();

module.exports = {
  insertOne,
  findOne
};
