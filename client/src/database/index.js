const specificationId = require("./models/SpecificationId");
const user = require("./models/User");
const proposalRequest = require("./models/ProposalRequest");
const proposalResponse = require("./models/ProposalResponse");

module.exports = { specificationId, user, proposalRequest, proposalResponse };
