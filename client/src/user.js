var FabricClient = require("fabric-client");
var FabricCaClient = require("fabric-ca-client");
var winston = require("winston");

var logger = winston.createLogger({
  level: "debug",
  transports: [new winston.transports.Console({ colorize: true })]
});

var hlfConfig = require("../config/hlf.config");
var database = require("./database");

let fabricClient; //= new FabricClient();
let fabricCaClient;
let channel; //= new FabricClient.Channel();

async function setup() {
  fabricClient = FabricClient.loadFromConfig(hlfConfig.connectionProfilePath);
  channel = fabricClient.getChannel();

  const stateStore = await FabricClient.newDefaultKeyValueStore({
    path: hlfConfig.keyStorePath
  });
  fabricClient.setStateStore(stateStore);

  const cryptoSuite = FabricClient.newCryptoSuite();
  const keyStore = FabricClient.newCryptoKeyStore({
    path: hlfConfig.keyStorePath
  });
  cryptoSuite.setCryptoKeyStore(keyStore);
  fabricClient.setCryptoSuite(cryptoSuite);

  var tlsOptions = {
    trustedRoots: [],
    verify: false
  };

  fabricCaClient = new FabricCaClient(
    "http://localhost:7054",
    tlsOptions,
    "ca.tracexyz.com",
    cryptoSuite
  );
}

setup();

async function enrollAdmin(username, password) {
  let user = fabricClient.getUserContext(username, true);
  if (user && user.isEnrolled()) {
    // User already enrolled
    return { status: "error" };
  } else {
    // Have to enroll the user
    const enrollment = await fabricCaClient.enroll({
      enrollmentID: username,
      enrollmentSecret: password
    });

    user = await fabricClient.createUser({
      username: username,
      mspid: "Org1", // Config
      cryptoContent: {
        privateKeyPEM: enrollment.key.toBytes(),
        signedCertPEM: enrollment.certificate
      }
    });

    const clientUser = await fabricClient.setUserContext(user);

    return { status: "success" };
  }
}

// later
