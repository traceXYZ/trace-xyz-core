var util = require("util");
var crypto = require("crypto");
var jwt = require("jsonwebtoken");
var jwtMiddleware = require("express-jwt");

var authConfig = require("../config/auth.config");
var database = require("./database");

const verifyMiddleware = jwtMiddleware({
  secret: authConfig.secret,
  getToken: function(req) {
    if (
      req.headers.authorization &&
      req.headers.authorization.split(" ")[0] === "Bearer"
    ) {
      return req.headers.authorization.split(" ")[1];
    } else if (req.query && req.query.token) {
      return req.query.token;
    } else if (req.cookies && req.cookies.token) {
      return req.cookies.token;
    }

    return null;
  }
});

async function createToken(username, password) {
  // details: {name, password}

  const user = await database.user.findOne(username);

  if (user) {
    const hashCalc = await util.promisify(crypto.pbkdf2)(
      password,
      Buffer.from(user.salt, "base64"),
      100000,
      64,
      "sha512"
    );

    if (user.hash !== hashCalc.toString("base64")) {
      return null;
    }
  } else {
    return null;
  }

  let token = jwt.sign(
    {
      username
    },
    authConfig.secret,
    {
      expiresIn: 60 * 60 * 24,
      algorithm: "HS256"
    }
  );

  return token;
}

async function registerUser(username, password) {
  if (!username || !password) {
    return null;
  }

  const user = await database.user.findOne(username);

  if (user) {
    // User already exists
    return null;
  }

  const salt = crypto.pseudoRandomBytes(32);

  const hashCalc = await util.promisify(crypto.pbkdf2)(
    password,
    salt,
    100000,
    64,
    "sha512"
  );

  const userCreated = await database.user.insertOne(
    username,
    hashCalc.toString("base64"),
    salt.toString("base64")
  );

  return { username };
}

module.exports = { verifyMiddleware, createToken, registerUser };
