var FabricClient = require("fabric-client");
var winston = require("winston");
var protobuf = require("protobufjs");

var logger = winston.createLogger({
  level: "debug",
  transports: [new winston.transports.Console({ colorize: true })]
});

var hlfConfig = require("../config/hlf.config");
var database = require("./database");

let fabricClient; //= new FabricClient();
let channel; //= new FabricClient.Channel();

async function setup() {
  fabricClient = FabricClient.loadFromConfig(hlfConfig.connectionProfilePath);
  channel = fabricClient.getChannel();

  const stateStore = await FabricClient.newDefaultKeyValueStore({
    path: hlfConfig.keyStorePath
  });
  fabricClient.setStateStore(stateStore);

  const cryptoSuite = FabricClient.newCryptoSuite();
  const keyStore = FabricClient.newCryptoKeyStore({
    path: hlfConfig.keyStorePath
  });
  cryptoSuite.setCryptoKeyStore(keyStore);
  fabricClient.setCryptoSuite(cryptoSuite);

  // //user = new FabricClient.User();
  // // TODO Remove
  // const user = await fabricClient.getUserContext("user_admin", true);
  // const SerializedIdentity = protobuf.Type.fromJSON("SerializedIdentity", {
  //   fields: {
  //     mspid: {
  //       type: "string",
  //       id: 1
  //     },
  //     id_bytes: {
  //       type: "bytes",
  //       id: 2
  //     }
  //   }
  // });

  // console.log(
  //   SerializedIdentity.decode(
  //     user.getIdentity().serialize()
  //   ).id_bytes.toString()
  // );

  // const key = await fabricClient
  //   .getCryptoSuite()
  //   .getKey("008b0fa994d3c20502d93d8fb82228efaea9c261a62b1c4ea19236fd03761813");
  // console.log(key.getPublicKey().toBytes());
}

setup();

async function addSpecification(specObject, name) {
  // TODO: Find active user, either using a login mechanism or config file
  const userName = "user_admin";

  /*
   * Don't let the `get` in `getUserContext` fool you. This internally calls 
   * `loadUserFromStateStore` and `setUserContext`. 
   */
  const user = await fabricClient.getUserContext(userName, true);
  if (!user.isEnrolled()) {
    // error
  }

  // const privateKeyString = ""; // TODO: Hardcode it here
  // // TODO: Use a better way to store private keys (and users). Maybe tokens?
  // const privateKey = await fabricClient
  //   .getCryptoSuite()
  //   .importKey(privateKeyString);

  // // TODO: Find why isn't public key stored in the keyStore.
  // const publicKeyString = ""; // TODO: Hardcode it here
  // const publicKey = await fabricClient
  //   .getCryptoSuite()
  //   .importKey(publicKeyString);

  const specObjectBinary = new Buffer(JSON.stringify(specObject), "utf8");
  const specSignature = user.getSigningIdentity().sign(specObjectBinary);
  // const specSignature = fabricClient
  //   .getCryptoSuite()
  //   .sign(privateKey, specObjectBinary);

  const specStoreRequestBody = {
    specifying_authority_public_key: user.getIdentity().serialize(), // publicKey.toBytes(),
    specification: Array.prototype.slice.call(specObjectBinary, 0),
    specification_signature: specSignature
  };

  const txId = fabricClient.newTransactionID();

  const specStoreRequest = {
    chaincodeId: "admin_cc",
    fcn: "store_spec",
    args: [JSON.stringify(specStoreRequestBody)],
    chainId: "cheeseproduction",
    txId
  };

  const specStoreResponse = await channel.sendTransactionProposal(
    specStoreRequest,
    2000
  );
  const [proposalResponses, proposal] = specStoreResponse;

  if (
    !(
      proposalResponses &&
      proposalResponses[0] &&
      proposalResponses[0].response.status === 200
    )
  ) {
    // TODO: Throw error
  }

  const response = JSON.parse(proposalResponses[0].response.payload);
  await database.specificationId.createSpecificationId(
    name,
    response.stored_item_tokens[0].item_id.toString()
  );

  const commitRequest = {
    proposalResponses: proposalResponses,
    proposal: proposal
  };

  const txIdString = txId.getTransactionID();

  let eventHub = fabricClient.newEventHub();
  eventHub.setPeerAddr("grpc://localhost:7053"); // TODO: Add to config

  let txPromise = new Promise((resolve, reject) => {
    let handle = setTimeout(() => {
      eventHub.disconnect();
      reject({ status: "timeout" });
    }, 3000);

    eventHub.connect();
    eventHub.registerTxEvent(
      txIdString,
      (txIdS, code) => {
        clearTimeout(handle);
        eventHub.unregisterTxEvent(txIdS);
        eventHub.disconnect();

        const returnStatus = { txId: txIdS, code };

        if (code === "VALID") {
          resolve({ ...returnStatus, status: "success" });
        } else {
          reject({ ...returnStatus, status: "invalid" });
        }
      },

      err => reject({ status: "error", error: err })
    );
  });

  const results = await Promise.all([
    channel.sendTransaction(commitRequest),
    txPromise
  ]);

  if (results && results[0] && results[0].status === "SUCCESS") {
    // Successfully sent to the orderer
  }

  if (results && results[1] && results[1].event_status === "VALID") {
    // Successfully executed by the peer
  }
}

module.exports = addSpecification;
