const FabricClient = require("fabric-client");
const util = require("util");

const hlfConfig = require("../config/hlf.config");

const db = require("./database");

let fabricClient;
let channel;

async function setup() {
  fabricClient = FabricClient.loadFromConfig(hlfConfig.connectionProfilePath);
  channel = fabricClient.getChannel();

  const stateStore = await FabricClient.newDefaultKeyValueStore({
    path: hlfConfig.keyStorePath
  });
  fabricClient.setStateStore(stateStore);

  const cryptoSuite = FabricClient.newCryptoSuite();
  const keyStore = FabricClient.newCryptoKeyStore({
    path: hlfConfig.keyStorePath
  });
  cryptoSuite.setCryptoKeyStore(keyStore);
  fabricClient.setCryptoSuite(cryptoSuite);
}

setup();

async function query(request, username) {
  await fabricClient.getUserContext(username, true);

  const queryRequest = {
    ...request // chaincodeId, fcn, chainId, args <- data here
  };

  console.log(request);

  try {
    const result = await channel.queryByChaincode(request);

    if (result && result.length > 0) {
      return JSON.parse(result.toString());
    }
  } catch (error) {
    return { error: "Item not found." };
  }
}

module.exports = query;
