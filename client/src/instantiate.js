var util = require("util");
var winston = require("winston");
var logger = winston.createLogger({
  level: "debug",
  transports: [new winston.transports.Console({ colorize: true })]
});

var config_file = "trace_local.json";

var cp = require("../../utils/connection_profile_lib")(config_file, logger);
var fcw = require("../../utils/fc_wrangler")(
  { block_delay: cp.getBlockDelay() },
  logger
);

var db = require("./database");

async function instantiate(chaincode_id, chaincode_version, args) {
  winston.info(
    "Starting to instantiate chaincode ",
    chaincode_id,
    chaincode_version
  );

  const chaincodeArgs = await Promise.all(args.map(db.specificationId.findOne));

  try {
    const enrollResponse = await util.promisify(fcw.enrollWithAdminCert)(
      cp.makeEnrollmentOptionsUsingCert()
    );

    const channelId = cp.getChannelId();
    const firstPeerName = cp.getFirstPeerName(channelId);

    const opts = {
      peer_urls: [cp.getPeersUrl(firstPeerName)],
      channel_id: channelId,
      //path to chaincode from <trace root>/chaincode/src/
      chaincode_id,
      chaincode_version,
      cc_args: chaincodeArgs,
      peer_tls_opts: cp.getPeerTlsCertOpts(firstPeerName)
    };

    const instantiateResponse = await util.promisify(fcw.instantiate_chaincode)(
      enrollResponse,
      opts
    );

    winston.info(
      "instantiating chaincode succeeded ",
      chaincode_id,
      chaincode_version
    );

    return { result: "success" };
  } catch (error) {
    logger.error(`Chaincode instantiation error: ${error}`);
    return { error: error.parsed };
  }
}

module.exports = instantiate;
