var path = require("path");

module.exports = {
  connectionProfilePath: path.resolve(
    __dirname,
    "../../config/connection_profile_local.json"
  ),
  keyStorePath: path.resolve(__dirname, "../data/hfc-key-store"),
  eventHubPeerAddress: "grpc://localhost:7053"
};
