const express = require("express");
const api = express.Router();

const install = require("../src/install");
const instantiate = require("../src/instantiate");
const addSpecification = require("../src/specification");
const invoke = require("../src/invoke");
const query = require("../src/query");

const auth = require("../src/auth");

api.use(auth.verifyMiddleware);

api.get("/auth-test", async (req, res) => {
  res.send({ username: req.user.username });
});

api.post("/install", async (req, res) => {
  const { chaincodeId, chaincodeVersion, chaincodeLocation } = req.body;
  // WARNING: Method naming convention change in callee ahead
  const result = await install(
    chaincodeId,
    chaincodeVersion,
    chaincodeLocation
  );
  if (result.error) {
    res.status(400);
  } else {
    res.status(201);
  }
  res.send(result);
});

api.post("/instantiate", async (req, res) => {
  const { chaincodeId, chaincodeVersion, args } = req.body;
  // WARNING: Method naming convention change in callee ahead
  const result = await instantiate(chaincodeId, chaincodeVersion, args);
  if (result.error) {
    res.status(400);
  } else {
    res.status(200);
  }
  res.send(result);
});

api.post("/specification", async (req, res) => {
  const { specification, name } = req.body;
  const result = await addSpecification();
});

api.post("/user", (req, res) => {});

api.post("/query", async (req, res) => {
  try {
    const results = await query(req.body, req.user.username);
    res.send(results);
  } catch (error) {
    res.status(404);
    res.send({ error: error.message });
  }
});

api.post("/invoke", async (req, res) => {
  try {
    const results = await invoke(req.body, req.user.username);
    res.send(results);
  } catch (error) {
    res.status(400);
    res.send({ error: error.message });
  }
});

module.exports = api;
