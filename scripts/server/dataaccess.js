var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var isConnected = false;
var dbo;

async function connect() {
    let db = await MongoClient.connect(url, {useNewUrlParser: true});
    dbo = await db.db("tracexyz");
    isConnected = true;
}

async function close() {
    dbo.close();
}

async function insert(userContext, testID) {
    if (!isConnected) {
        await connect();
    }
    let result = await dbo.collection("userContexts" + testID).insertOne(userContext);
    return result;
}

async function query(query, testID) {
    if (!isConnected) {
        await connect();
    }
    let result = await dbo.collection("userContexts" + testID).find(query);
    return await result.toArray();
}

async function update(user_id, value, testID) {
    if (!isConnected) {
        await connect();
    }
    let result = await dbo.collection("userContexts" + testID).updateOne({user_id: user_id}, {$set: value});
    return await result;
}

async function remove(user_id, testID) {
    if (!isConnected) {
        await connect();
    }
    let result = await dbo.collection("userContexts" + testID).deleteMany({user_id: user_id});
    return await result;
}

async function resetDB() {
    for (var i = 0; i < 150; i++) {
        await remove("cheese_producer", i);
        await remove("milk_producer_1", i);
        await remove("milk_producer_2", i);
        await remove("salt_producer", i);
    }

    return {STATUS: "Reset Success"}
}

module.exports.insert = insert;
module.exports.query = query;
module.exports.close = close;
module.exports.update = update;
module.exports.remove = remove;
module.exports.resetDB = resetDB;