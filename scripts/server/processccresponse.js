const db = require('./dataaccess.js');

async function processResponse(response, testID, user_id) {
    if (response.response_type === 'create') {
        await processCreate(user_id, testID, response);
    } else if (response.response_type === 'sell_raw') {
        await processSell(user_id, testID, response);
    } else if (response.response_type === 'manufacture') {
        await processManufacture(user_id, testID, response);
    } else if (response.response_type === 'transport') {
        await processTransport(user_id, testID, response);
    } else if (response.response_type === 'divide') {
        await processCutAndPackage(user_id, testID, response);
    } else if (response.response_type === 'merge') {
        await processBoxing(user_id, testID, response);
    } else if (response.response_type === 'split') {
        await processBoxing(user_id, testID, response);
    } else if (response.response_type === 'owner_transfer') {
        await processOwnerTransfer(user_id, testID, response);
    } else if (response.response_type === 'process_update') {
        await processProcessUpdate(user_id, testID, response);
    } else {
        console.log(JSON.stringify(response))
    }
}

async function processCreate(user_id, testID, response) {
    console.log(response.txn_id);
    var txnObjects = await db.query({txn_id: response.txn_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    var txnObject = txnObjects[0];
    await createOrUpdateUserItem(user_id, testID, "", response, txnObject);

}

async function processSell(user_id, testID, response) {
    console.log(response.txn_id);
    var txnObjects = await db.query({txn_id: response.txn_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    var txnObject = txnObjects[0];

    await createOrUpdateUserItem(txnObject.buyer, testID, "", response, txnObject);
    await createOrUpdateUserItem(user_id, testID, txnObject.sold_item, response, txnObject);
}

async function processManufacture(user_id, testID, response) {
    console.log(response.txn_id);
    var txnObjects = await db.query({txn_id: response.txn_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    var txnObject = txnObjects[0];
    await createOrUpdateUserItem(user_id, testID, "", response, txnObject);

    await createOrUpdateUserItem(user_id, testID, txnObject.salt_id, response, txnObject);
    await createOrUpdateUserItem(user_id, testID, txnObject.milk_id, response, txnObject);
    await createOrUpdateProcess(user_id, testID, response, txnObject);

}

async function processTransport(user_id, testID, response) {
    console.log(response.txn_id);
    var txnObjects = await db.query({txn_id: response.txn_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    var txnObject = txnObjects[0];
    await createOrUpdateUserItem(user_id, testID, txnObject.item_id, response, txnObject);
    await createOrUpdateProcess(user_id, testID, response, txnObject);
}

async function processBoxing(user_id, testID, response) {
    console.log(response.txn_id);
    var txnObjects = await db.query({txn_id: response.txn_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    console.log(response);
    var txnObject = txnObjects[0];
    await createOrUpdateUserItem(user_id, testID, "", response, txnObject);
}

async function processCutAndPackage(user_id, testID, response) {
    console.log(response.txn_id);
    var txnObjects = await db.query({txn_id: response.txn_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    var txnObject = txnObjects[0];
    await createOrUpdateUserItem(user_id, testID, txnObject.cheese_id, response, txnObject);
    await createOrUpdateUserItem(user_id, testID, "", response, txnObject);
}

async function processOwnerTransfer(user_id, testID, response) {
    console.log(response.txn_id);
    var txnObjects = await db.query({txn_id: response.txn_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    var txnObject = txnObjects[0];
    await createOrUpdateProcess(user_id, testID, response, txnObject);
}

async function processProcessUpdate(user_id, testID, response) {
    console.log(response.txn_id);
    var txnObjects = await db.query({txn_id: response.txn_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    var txnObject = txnObjects[0];
    await createOrUpdateProcess(user_id, testID, response, txnObject);
}

async function createOrUpdateProcess(user, testID, response, txnObject) {
    var userObjects = await db.query({user_id: user}, testID);
    var userObject;
    var isUserEmpty = false;
    if (userObjects.length === 0) {
        userObject = {};
        userObject.user_id = user;
        userObject.owning_items = {};
        userObject.handling_processes = {};
        isUserEmpty = true;
        console.log('empty user object')
    } else {
        userObject = userObjects[0];
        console.log('user found');
    }

    if (response.storage_network_response.process_token !== null && txnObject.txn_type === 'transport') {
        if (userObject.handling_processes[response.storage_network_response.process_token.item_id] === undefined) {
            userObject.handling_processes[response.storage_network_response.process_token.item_id] = {}
        }
        userObject.handling_processes[response.storage_network_response.process_token.item_id].handler =
            response.storage_network_response.process_token.next_update_challenge;
        userObject.handling_processes[response.storage_network_response.process_token.item_id].process =
            user + '_' + txnObject.txn_type;
        userObject.handling_processes[response.storage_network_response.process_token.item_id].count = 0;
    }
    //console.log(response); process.exit();
    if (response.storage_network_response.process_token !== null && txnObject.txn_type === 'manufacture') {
        if (userObject.handling_processes[response.storage_network_response.process_token.item_id] === undefined) {
            userObject.handling_processes[response.storage_network_response.process_token.item_id] = {}
        }
        userObject.handling_processes[response.storage_network_response.process_token.item_id].handler =
            response.storage_network_response.process_token.next_update_challenge;
        userObject.handling_processes[response.storage_network_response.process_token.item_id].process =
            user + '_' + txnObject.txn_type + '_' + txnObject.item_type;
        userObject.handling_processes[response.storage_network_response.process_token.item_id].count = 0;
    }

    if (response.authority_update_response !== undefined && txnObject.txn_type === 'iot_transfer') {
        //console.log(response);
        //process.exit();
        if (response.authority_update_response.status === 'Success') {
            userObject.handling_processes[response.authority_update_response.updated_item_token.item_id].handler =
                response.authority_update_response.updated_item_token.next_update_challenge;
        }
    }

    if (txnObject.txn_type === 'iot_update') {
        for (var i = 0; i < response.storage_network_response.updated_relationship_tokens.length; i++) {
            userObject.handling_processes[response.storage_network_response.updated_relationship_tokens[i].item_id].handler =
                response.storage_network_response.updated_relationship_tokens[i].next_update_challenge;
            userObject.handling_processes[response.storage_network_response.updated_relationship_tokens[i].item_id].count =
                userObject.handling_processes[response.storage_network_response.updated_relationship_tokens[i].item_id].count + 1;
        }

    }

    if (isUserEmpty) {
        await db.insert(userObject, testID);
    } else {
        await db.update(user, userObject, testID);
    }

}

async function createOrUpdateUserItem(user, testID, item_id, response, txnObject) {
    var userObjects = await db.query({user_id: user}, testID);
    var userObject;
    var isUserEmpty = false;
    if (userObjects.length === 0) {
        userObject = {};
        userObject.user_id = user;
        userObject.owning_items = {};
        userObject.handling_processes = {};
        isUserEmpty = true;
        console.log('empty user object')
    } else {
        userObject = userObjects[0];
        console.log('user found');
    }

    if (response.storage_network_response.stored_item_tokens !== null && item_id === "") {
        for (var i = 0; i < response.storage_network_response.stored_item_tokens.length; i++) {
            userObject.owning_items[response.storage_network_response.stored_item_tokens[i].item_id] = {};
            userObject.owning_items[response.storage_network_response.stored_item_tokens[i].item_id].owner =
                response.storage_network_response.stored_item_tokens[i].next_update_challenge;
            userObject.owning_items[response.storage_network_response.stored_item_tokens[i].item_id].handler =
                response.storage_network_response.stored_item_tokens[i].next_update_challenge;
            userObject.owning_items[response.storage_network_response.stored_item_tokens[i].item_id].type =
                txnObject.item_type;
            userObject.owning_items[response.storage_network_response.stored_item_tokens[i].item_id].count = 0;
        }
    }

    if (response.storage_network_response.updated_relationship_tokens !== null) {
        for (var i = 0; i < response.storage_network_response.updated_relationship_tokens.length; i++) {
            const itemID = response.storage_network_response.updated_relationship_tokens[i].item_id;
            if ((userObject.owning_items)[itemID] !== undefined) {
                ((userObject.owning_items)[itemID]).handler =
                    response.storage_network_response.updated_relationship_tokens[i].next_update_challenge;
                console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa")
                console.log((userObject.owning_items)[itemID])
                console.log("relationship updated")
                userObject.owning_items[itemID].count = userObject.owning_items[itemID].count + 1;
                //process.exit(1);
            }
            if (response.storage_network_response.updated_relationship_tokens[i].item_id === item_id
                && userObject.owning_items[item_id] !== undefined) {
                userObject.owning_items[item_id].handler =
                    response.storage_network_response.updated_relationship_tokens[i].next_update_challenge;
                console.log("relationship updated")

            }
        }
    }
    if (response.storage_network_response.updated_item_tokens !== null) {


        for (var i = 0; i < response.storage_network_response.updated_item_tokens.length; i++) {
            if (response.storage_network_response.updated_item_tokens[i].item_id === item_id
                && userObject.owning_items[item_id] !== undefined) {
                userObject.owning_items[item_id].owner =
                    response.storage_network_response.updated_item_tokens[i].next_update_challenge;
                console.log("ownership updated")
            }
        }
    }

    if (item_id !== "") {
        if (userObject.owning_items[item_id].count === undefined) {
            userObject.owning_items[item_id].count = 0;
        } else {
            userObject.owning_items[item_id].count = userObject.owning_items[item_id].count + 1;
        }
    }
    if (isUserEmpty) {
        await db.insert(userObject, testID);
    } else {
        await db.update(user, userObject, testID);
    }
}


module.exports.processResponse = processResponse;