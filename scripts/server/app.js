var express = require("express");
var bodyParser = require("body-parser");
var routes = require("./routes/routes.js");
var db = require('./dataaccess.js');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

routes(app);

var args = process.argv.slice(2);


var server = app.listen(args[0], function () {
    console.log("Welcome! TraceXYZ Dev Chaincode Server - v1.00")
});

process.on('SIGTERM', shutDown);
process.on('SIGINT', shutDown);

function shutDown() {
    console.log('Received kill signal, shutting down gracefully');
    server.close(() => {
        db.close();
        console.log('Closed out remaining connections');
        process.exit(0);
    });

    setTimeout(() => {
        console.error('Could not close connections in time, forcefully shutting down');
        process.exit(1);
    }, 10000);
}