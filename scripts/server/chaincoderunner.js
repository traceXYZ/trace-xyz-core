var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');
var fs = require('fs');
var winston = require('winston');
var reqProcessor = require('./processccrequest.js');
var resProcessor = require('./processccresponse.js');
var db = require('./dataaccess.js');


const MESSAGE = Symbol.for('message');

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../../logs/performance2.log',
            level: 'info',
            json: false,
        }),

    ]
});

var userClient = {};
var eventHubs = {};


async function initrunner(user_id, test_id) {
    var fabric_client = new Fabric_Client();
    var channel = fabric_client.newChannel('cheeseproduction');
    var peer = fabric_client.newPeer('grpc://localhost:7051');
    channel.addPeer(peer);
    var order = fabric_client.newOrderer('grpc://localhost:7050');
    channel.addOrderer(order);
    var store_path = path.join(__dirname, '../hfc-key-store');

    console.log("initializing HL client");
    var state_store = await Fabric_Client.newDefaultKeyValueStore({path: store_path});
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    var user_from_store = await fabric_client.getUserContext(user_id, true);
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded user from persistence');
    } else {
        throw new Error('Failed to load user from persistance');
    }
    var event_hub = fabric_client.newEventHub();
    event_hub.setPeerAddr('grpc://localhost:7053');
    event_hub.connect();
    eventHubs[user_id + test_id] = event_hub;
    userClient[user_id + test_id] = fabric_client;
    console.log("Runner initialized!");
    return fabric_client;
}

async function getFabricClient(user_id, test_id) {
    var fabric_client = userClient[user_id + test_id];
    if (fabric_client === undefined || fabric_client === null) {
        console.log("trying to init client");
        try {
            fabric_client = await initrunner(user_id, test_id);
        } catch (e) {
            console.log(e);
            return {'STATUS': 'ERROR'}
        }

    } else {
        console.log("cached client found");
    }
    //console.log(fabric_client);
    return fabric_client;
}

async function runChaincode(user_id, testID, request) {
    var fabric_client = await getFabricClient(user_id, testID);
    var channel = fabric_client.getChannel();
    var tx_id = fabric_client.newTransactionID();
    console.log("Assigning transaction_id: ", tx_id._transaction_id);
    var ccrequest = {
        chaincodeId: request.chaincode_id,
        fcn: request.function,
        args: request.args,
        chainId: request.chain_id,
        txId: tx_id,
    };
    request.txn_id = tx_id.getTransactionID();
    reqProcessor.processRequest(user_id, testID, request);

    if (request.sync !== true) {
        try {
            console.log("sync txn started" + user_id);
            var results = await channel.sendTransactionProposal(ccrequest, 25000);
            var results1 = await processTxnProposal(user_id, testID, results);
            var a = await processPeerResponse(user_id, testID, results1);
            await Promise.all([results, results1, a]);
            return {status: 'SUBMITTED', txn_id: tx_id.getTransactionID()}
        } catch (err) {
            console.error('Failed to invoke successfully :: ' + err);
            return {status: 'ERROR'}
        }
    } else {
        channel.sendTransactionProposal(ccrequest, 25000)
            .then(
                (result) => processTxnProposal(user_id, testID, result)
            ).then(
            (result) => processPeerResponse(user_id, testID, result)
        ).catch((err) => {
            console.error('Failed to invoke successfully :: ' + err);
            return {status: 'ERROR'}
        });
        return {status: 'SUBMITTED', txn_id: tx_id.getTransactionID()}
    }
}

async function runQueryChaincode(user_id, query) {
    var fabric_client = await getFabricClient(user_id);
    var channel = fabric_client.getChannel();
    var request = {
        //targets: let default to the peer assigned to the client
        chaincodeId: 'storage_cc',
        fcn: 'retrieve',
        args: [''],
        chainId: 'cheeseproduction',
    };
    request.args = [JSON.stringify(query)];
    var result = await channel.queryByChaincode(request);
    return result[0].toString();
}


async function processPeerResponse(user_id, testID, results) {
    var txnObjects = await db.query({txn_id: results[1].tx_id}, testID);
    if (txnObjects.length === 0) {
        //console.log(txnObjects);
        throw new Error("transaction information not found")
    }
    var txnObject = txnObjects[0];
    // check the results in the order the promises were added to the promise all list
    if (results && results[0] && results[0].status === 'SUCCESS') {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnObject.start_time,
            txn_status: 'success',
            event: 'orderer_response'
        });
        console.log('Successfully sent transaction to the orderer.');
    } else {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnObject.start_time,
            txn_status: 'failure',
            event: 'orderer_response'
        });
        console.error('Failed to order the transaction. Error code: ' + response.status);
    }

    if (results && results[1] && results[1].event_status === 'VALID') {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnObject.start_time,
            txn_status: 'success',
            event: 'peer_response'
        });
        //if(results[1].tx_id)
        var responsePayload = await db.query({tx_id: results[1].tx_id}, testID);
        await resProcessor.processResponse(responsePayload[0].payload, testID, user_id);
        console.log('Successfully transaction executed by the peer');
    } else {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnObject.start_time,
            txn_status: 'failure',
            event: 'peer_response'
        });
        console.error('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
    }

    if (results && results[1] && results[1].event_status === 'TIMEOUT') {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnObject.start_time,
            txn_status: 'success',
            event: 'peer_response'
        });
        console.error('Transaction timed out');
    }
}

async function processTxnProposal(user_id, testID, results) {
    let isProposalGood = false;
    var proposalResponses = results[0];
    var proposal = results[1];
    var TxnID;
    var txnObject;
    var responsePayload;
    console.log(proposalResponses);
    if (proposalResponses && proposalResponses[0].response && proposalResponses[0].response.status === 200) {
        isProposalGood = true;
        responsePayload = JSON.parse(proposalResponses[0].response.payload);
        TxnID = responsePayload.txn_id;
        var txnObjects = await db.query({txn_id: TxnID}, testID);
        if (txnObjects.length === 0) {
            //console.log(txnObjects);
            throw new Error("transaction information not found")
        }
        txnObject = txnObjects[0];
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnObject.start_time,
            txn_status: 'success',
            event: 'proposal_response'
        });
    } else {
        var d = new Date();
        //logger.info('Insert Performance',{latency:d.getTime()-txnObject.start_time,txn_status:'fail'});
        console.error('Transaction proposal was bad');
    }
    if (isProposalGood) {
        var request = {
            proposalResponses: proposalResponses,
            proposal: proposal
        };

        await db.insert({tx_id: TxnID, payload: responsePayload}, testID);
        var promises = [];
        var fabricClient = await getFabricClient(user_id);
        var requestPromise = fabricClient.getChannel().sendTransaction(request);
        promises.push(requestPromise);
        var event_hub = eventHubs[user_id + testID];
        let txPromise = new Promise((resolve, reject) => {
            let handle = setTimeout(() => {
                resolve({event_status: 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
            }, 250000);
            event_hub.registerTxEvent(TxnID, (tx, code) => {

                clearTimeout(handle);
                event_hub.unregisterTxEvent(tx);
                // now let the application know what happened
                var return_status = {event_status: code, tx_id: tx};
                if (code !== 'VALID') {
                    console.error('The transaction was invalid, code = ' + code);
                    resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                } else {
                    console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                    resolve(return_status);
                }
            }, (err) => {
                reject(new Error('There was a problem with the eventhub ::' + err));
            });
        });
        promises.push(txPromise);
        return Promise.all(promises);
    } else {
        throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    }
}

module.exports.runChaincode = runChaincode;
module.exports.runQueryChaincode = runQueryChaincode;