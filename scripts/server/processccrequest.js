const db = require('./dataaccess.js');

async function processRequest(user_id, testID, request) {
    //console.log(request.request_type);
    if (request.request_type === 'create') {
        await processCreate(user_id, testID, request);
    } else if (request.request_type === 'sell_raw') {
        await procesSell(user_id, testID, request);
    } else if (request.request_type === 'manufacture') {
        await processManufacture(user_id, testID, request);
    } else if (request.request_type === 'divide') {
        await processCutAndPackage(user_id, testID, request);
    } else if (request.request_type === 'transport') {
        await processTransfer(user_id, testID, request);
    } else if (request.request_type === 'merge') {
        await processBoxing(user_id, testID, request);
    } else if (request.request_type === 'iot_transfer') {
        await processTransfer(user_id, testID, request);
    } else if (request.request_type === 'iot_update') {
        await processTransfer(user_id, testID, request);
    } else if (request.request_type === 'int_shipping') {
        await processTransfer(user_id, testID, request);
    } else {
        console.log(request)
    }
}

async function processCreate(user_id, testID, request) {
    var txnObject = {};
    var d = new Date();
    txnObject.start_time = d.getTime();
    txnObject.creator = user_id;
    txnObject.txn_id = request.txn_id;
    txnObject.item_type = request.item_type;
    txnObject.txn_type = request.request_type;
    //console.log(txnObject);
    await db.insert(txnObject, testID);
}

async function procesSell(user_id, testID, request) {
    var txnObject = {};
    var d = new Date();
    txnObject.start_time = d.getTime();
    txnObject.seller = user_id;
    txnObject.txn_id = request.txn_id;
    txnObject.item_type = request.item_type;
    txnObject.txn_type = request.request_type;
    txnObject.buyer = request.buyer;
    txnObject.sold_item = request.sold_item;
    //console.log(txnObject,testID);
    await db.insert(txnObject, testID);
}

async function processManufacture(user_id, testID, request) {
    var txnObject = {};
    var d = new Date();
    txnObject.start_time = d.getTime();
    txnObject.txn_id = request.txn_id;
    txnObject.item_type = request.item_type;
    txnObject.txn_type = request.request_type;
    txnObject.milk_id = request.milk_id;
    txnObject.milk_user = request.milk_user;
    txnObject.salt_id = request.salt_id;
    txnObject.salt_user = request.salt_user;
    await db.insert(txnObject, testID);
}

async function processCutAndPackage(user_id, testID, request) {
    var txnObject = {};
    var d = new Date();
    txnObject.start_time = d.getTime();
    txnObject.txn_id = request.txn_id;
    txnObject.item_type = request.item_type;
    txnObject.txn_type = request.request_type;
    txnObject.cheese_id = request.cheese_id;
    await db.insert(txnObject, testID);
}

async function processBoxing(user_id, testID, request) {
    var txnObject = {};
    var d = new Date();
    txnObject.start_time = d.getTime();
    txnObject.txn_id = request.txn_id;
    txnObject.item_type = request.item_type;
    txnObject.txn_type = request.request_type;
    await db.insert(txnObject, testID);
}

async function processTransfer(user_id, testID, request) {
    var txnObject = {};
    var d = new Date();
    txnObject.start_time = d.getTime();
    txnObject.txn_id = request.txn_id;
    txnObject.txn_type = request.request_type;
    txnObject.item_id = request.item_id;
    await db.insert(txnObject, testID);
}

module.exports.processRequest = processRequest;