const runner = require('../chaincoderunner.js');
const db = require('../dataaccess.js');

var appRouter = function (app) {
    app.get("/", function (req, res) {
        res.status(200).send("TraceXYZ Chaincode Server V1.0")
    });
    app.post("/chaincode/invoke", async function (req, res) {
        //console.log(req.body);
        var requestObj = req.body;
        var status = await runner.runChaincode(requestObj.user_id, requestObj.test_id, requestObj.ccrequest);
        res.status(200).send(status)
    });
    app.post("/chaincode/query", async function (req, res) {
        //console.log(req.body);
        var requestObj = req.body;
        var status = await runner.runQueryChaincode(requestObj.user_id, requestObj.query);
        res.status(200).send(status)
    });
    app.post("/db/insert", async function (req, res) {
        var status = await db.insert(req.body.query, req.body.test_id);
        res.status(200).send(status)
    });
    app.post("/db/query", async function (req, res) {
        var result = await db.query(req.body.query, req.body.test_id);
        res.status(200).send(result)
    });
    app.post("/db/reset", async function (req, res) {
        var result = await db.resetDB();
        res.status(200).send(result)
    });
};

module.exports = appRouter;