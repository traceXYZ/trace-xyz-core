#!/bin/bash


#node install_chaincode.js "performance_cc" "ver-DEV500021" "graphperformance"
#node instantiate_chaincode.js "performance_cc" "ver-DEV300020" "graphperformance"
#node upgrade_chaincode.js "performance_cc" "ver-DEV300020" "graphperformance"

# Starting from 10 concurrent requests to 100 concurrent requests
# Add 100 nodes and 100 edges


for j in `seq 1 10`;
do
    for i in `seq 1 20`;
    do
        node ./PerformanceEvaluation/EvaluateInvoke_SupplyChainGraph.js 1 supply_chain_simulation $((5*j))  $((i))
#        sleep 10s
        node ./PerformanceEvaluation/EvaluateQuery.js 50 bfs
#        sleep 10s
        rm -rf ./PerformanceEvaluation/nodes.json
        echo "Sleeping 30 seconds...."
    	sleep 30s
    done
    echo "Sleeping 30 seconds...."
    sleep 30s
done
