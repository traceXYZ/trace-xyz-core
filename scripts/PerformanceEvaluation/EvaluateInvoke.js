'use strict';

var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');
var fs = require('fs');
var winston = require('winston');
const MESSAGE = Symbol.for('message');

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/performance.log',
            level: 'info',
            json: false,
        }),

    ]
});

var request;
//
var fabric_client = new Fabric_Client();
var crypt = fabric_client.getCryptoSuite();

// setup the fabric network
var channel = fabric_client.newChannel('cheeseproduction');
var peer = fabric_client.newPeer('grpc://localhost:7051');
channel.addPeer(peer);
var order = fabric_client.newOrderer('grpc://localhost:7050')
channel.addOrderer(order);
var user;

var args = process.argv.slice(2);

var txnTimes = {};

//
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');
console.log('Store path:' + store_path);

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);

    // user_admin is a user with org1.admin affiliation value set
    user = 'cheese_producer';

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded user1 from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }
    // We create request after reading user's private key

    var arg2 = 10;
    var arg3 = 10;
    if (args[2]) {
        arg2 = args[2]
    }
    if (args[3]) {
        arg3 = args[3]
    }
    if (args[0]) {
        for (var i = 0; i < args[0]; i++) {
            var tx_id = fabric_client.newTransactionID();
            console.log("Assigning transaction_id: ", tx_id._transaction_id);
            request = {
                //targets: let default to the peer assigned to the client
                chaincodeId: 'performance_cc',
                fcn: args[1],
                args: [arg2, arg3],
                chainId: 'cheeseproduction',
                txId: tx_id,
            };
            var d = new Date();
            var TxnID = tx_id.getTransactionID().toString();
            txnTimes[TxnID] = d.getTime();
            channel.sendTransactionProposal(request, 10000)
                .then(
                    (result) => processTxnProposal(result)
                ).then(
                (result) => processPeerResponse(result),
            ).catch((err) => {
                console.error('Failed to invoke successfully :: ' + err);
            });
        }
    }

    // send the transaction proposal to the peers
});

function processPeerResponse(results) {
    var TxnID = results[1].tx_id;
    // check the results in the order the promises were added to the promise all list
    if (results && results[0] && results[0].status === 'SUCCESS') {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnTimes[TxnID],
            txn_status: 'success',
            event: 'orderer_response'
        });
        console.log('Successfully sent transaction to the orderer.');
    } else {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnTimes[TxnID],
            txn_status: 'failure',
            event: 'orderer_response'
        });
        console.error('Failed to order the transaction. Error code: ' + response.status);
    }

    if (results && results[1] && results[1].event_status === 'VALID') {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnTimes[TxnID],
            txn_status: 'success',
            event: 'peer_response'
        });
        console.log('Successfully transaction executed by the peer');
    } else {
        var d = new Date();
        logger.info('Insert Performance', {
            latency: d.getTime() - txnTimes[TxnID],
            txn_status: 'failure',
            event: 'peer_response'
        });
        console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
    }
}

function processTxnProposal(results) {
    let isProposalGood = false;
    var proposalResponses = results[0];
    var proposal = results[1];
    var TxnID;
    if (proposalResponses && proposalResponses[0].response && proposalResponses[0].response.status === 200) {
        var d = new Date();
        var TxnID = JSON.parse(proposalResponses[0].response.payload).txn_id;
        logger.info('Insert Performance', {
            latency: d.getTime() - txnTimes[TxnID],
            txn_status: 'success',
            event: 'proposal_response'
        });
        isProposalGood = true;
        // var fileData;
        // try {
        //     fileData = fs.readFileSync('./PerformanceEvaluation/nodes.json');
        // } catch (e) {
        //     fileData = fs.openSync('./PerformanceEvaluation/nodes.json', 'w+');
        //     fs.closeSync(fileData);
        //     fileData = fs.readFileSync('./PerformanceEvaluation/nodes.json');
        // }
        // var nodesList;
        // if (fileData.toString() == "") {
        //     nodesList = [];
        // } else {
        //     nodesList = JSON.parse(fileData.toString());
        // }
        // nodesList = nodesList.concat(JSON.parse(proposalResponses[0].response.payload).nodes);
        // fs.writeFileSync('./PerformanceEvaluation/nodes.json', JSON.stringify(nodesList));
        console.log('Transaction proposal was good');
        // console.log('\x1b[33m%s\x1b[0m','START RESULT');
        // console.log('\x1b[36m%s\x1b[0m',proposalResponses[0].response.payload);
        // console.log('\x1b[33m%s\x1b[0m','END RESULT');
    } else {
        var d = new Date();
        TxnID = JSON.parse(proposalResponses[0].response.payload).txn_id;
        logger.info('Insert Performance', {latency: d.getTime() - txnTimes[TxnID], txn_status: 'fail'});
        console.error('Transaction proposal was bad');
    }
    if (isProposalGood) {
        console.log(util.format(
            'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
            proposalResponses[0].response.status, proposalResponses[0].response.message));
        // build up the request for the orderer to have the transaction committed
        var request = {
            proposalResponses: proposalResponses,
            proposal: proposal
        };

        // set the transaction listener and set a timeout of 30 sec
        // if the transaction did not get committed within the timeout period,
        // report a TIMEOUT status
        var transaction_id_string = TxnID; //Get the transaction ID string to be used by the event processing
        var promises = [];

        var sendPromise = channel.sendTransaction(request);
        promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

        // get an eventhub once the fabric client has a user assigned. The user
        // is required bacause the event registration must be signed
        let event_hub = fabric_client.newEventHub();
        event_hub.setPeerAddr('grpc://localhost:7053');

        // using resolve the promise so that result status may be processed
        // under the then clause rather than having the catch clause process
        // the status
        let txPromise = new Promise((resolve, reject) => {
            let handle = setTimeout(() => {
                event_hub.disconnect();
                resolve({event_status: 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
            }, 10000);
            event_hub.connect();
            event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                // this is the callback for transaction event status
                // first some clean up of event listener
                // console.log('TX');
                // console.log(tx);
                clearTimeout(handle);
                event_hub.unregisterTxEvent(tx);
                event_hub.disconnect();

                // now let the application know what happened
                var return_status = {event_status: code, tx_id: tx};
                if (code !== 'VALID') {
                    console.error('The transaction was invalid, code = ' + code);
                    resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                } else {
                    console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                    resolve(return_status);
                }
            }, (err) => {
                //this is the callback if something goes wrong with the event registration or processing
                reject(new Error('There was a problem with the eventhub ::' + err));
            });
        });
        promises.push(txPromise);

        return Promise.all(promises);
    } else {

        throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    }
}
