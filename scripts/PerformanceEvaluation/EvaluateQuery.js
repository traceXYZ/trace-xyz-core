'use strict';

var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');
var fs = require('fs');
var winston = require('winston');
const MESSAGE = Symbol.for('message');

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/performance.log',
            level: 'info',
            json: false,
        }),

    ]
});
//
var fabric_client = new Fabric_Client();
var args = process.argv.slice(2);
var txnTimes = {};

// setup the fabric network
var channel = fabric_client.newChannel('cheeseproduction');
var peer = fabric_client.newPeer('grpc://localhost:7051');
channel.addPeer(peer);

//
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');
console.log('Store path:' + store_path);
var tx_id = null;


// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext('user_001', true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded user_001 from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user_001.... run registerUser.js');
    }

    var arg2 = '';
    var arg3 = '';
    if (args[2]) {
        arg2 = args[2]
    }
    var fileData;
    try {
        fileData = fs.readFileSync('./PerformanceEvaluation/nodes.json');
    } catch (e) {
        fileData = fs.openSync('./PerformanceEvaluation/nodes.json', 'w+');
        fs.closeSync(fileData);
        fileData = fs.readFileSync('./PerformanceEvaluation/nodes.json');
    }
    var nodesList;
    if (fileData.toString() == "") {
        nodesList = [];
    } else {
        nodesList = JSON.parse(fileData.toString());
    }

    //console.log(nodesList)

    if (args[0]) {
        for (var i = 0; i < args[0]; i++) {
            tx_id = fabric_client.newTransactionID();
            //console.log("Assigning transaction_id: ", tx_id.getTransactionID());
            var randomRootNode = Math.floor(Math.random() * Math.floor(nodesList.length));

            var TxnID = tx_id.getTransactionID().toString();
            var request = {
                //targets: let default to the peer assigned to the client
                chaincodeId: 'performance_cc',
                fcn: args[1],
                args: [nodesList[randomRootNode].id.toString(), TxnID],
                txId: tx_id,
            };
            var d = new Date();
            txnTimes[TxnID] = d.getTime();
            channel.queryByChaincode(request)
                .then(
                    (result) => processQuery(result, tx_id.getTransactionID())
                ).catch((err) => {
                console.error('Failed to query successfully :: ' + err);
            });
        }
    }


}).catch((err) => {
    console.error('Failed to query successfully :: ' + err);
});

function processQuery(results) {
    console.log("Query has completed, checking results");
    // query_responses could have more than one  results if there multiple peers were used as targets
    if (results && results.length == 1) {
        var d = new Date();
        var TxnID = JSON.parse(results).txn_id;
        //console.log(JSON.parse(results).txn_id);
        //console.log(txnTimes);
        logger.info('Query Performance', {
            latency: d.getTime() - txnTimes[TxnID],
            txn_status: 'success',
            event: 'query_response'
        });
        //console.log(d.getTime());
        if (results[0] instanceof Error) {
            console.error("error from query = ", results[0]);
        } else {
            // console.log('\x1b[33m%s\x1b[0m','START RESULT');
            // console.log('\x1b[36m%s\x1b[0m',results[0].toString());
            var queryResponse = JSON.parse(results).nodes;
            //console.log(queryResponse);
            logger.info('Query Performance', {
                length: queryResponse.length,
                txn_status: 'success',
                event: 'response_length'
            });
            // console.log('\x1b[33m%s\x1b[0m','END RESULT');

        }
    } else {
        console.log("No payloads were returned from query");
    }
}