#!/usr/bin/env bash

./full_dev_test_ver_1.sh $(($2)) $1 & sleep 5s & ./full_dev_test_ver_1.sh $(($2+5)) $1 & sleep 5s &
./full_dev_test_ver_1.sh $(($2+10)) $1 & sleep 5s & ./full_dev_test_ver_1.sh $(($2+15)) $1 & sleep 5s &
./full_dev_test_ver_1.sh $(($2+20)) $1 & sleep 5s & ./full_dev_test_ver_1.sh $(($2+25)) $1 & sleep 5s & ./full_dev_test_ver_1.sh $(($2+30)) $1 & sleep 5s
echo "Test Complete"
