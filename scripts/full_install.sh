#!/bin/bash

# install storage chaincode
node install_chaincode.js "storage_cc" "ver-DEV99999000160" "storage"
node instantiate_chaincode.js "storage_cc" "ver-DEV99999000160"

node install_chaincode.js "admin_cc" "ver-DEV99999000160" "admin"
node instantiate_chaincode.js "admin_cc" "ver-DEV99999000160"

# Add milk production specification
node ./AdminRequests/AddSpecification.js ./example_specs/MilkProductionSpec.json MILK_PRODUCTION_SPEC_001
# Add milk selling specification
node ./AdminRequests/AddSpecification.js ./example_specs/MilkSellingSpec.json MILK_SELLING_SPEC_001

# Add milk production specification
node ./AdminRequests/AddSpecification.js ./example_specs/SaltProductionSpec.json SALT_PRODUCTION_SPEC_001
# Add milk selling specification
node ./AdminRequests/AddSpecification.js ./example_specs/SaltSellingSpec.json SALT_SELLING_SPEC_001

# Add milk selling specification
node ./AdminRequests/AddSpecification.js ./example_specs/SaltSellingSpec.json SALT_SELLING_SPEC_001

# Add cheese manufacturing specification
node ./AdminRequests/AddSpecification.js ./example_specs/MilkTransportSpec.json MILK_TRANSPORT_SPEC_001

node ./AdminRequests/AddSpecification.js ./example_specs/SaltTransportSpec.json SALT_TRANSPORT_SPEC_001

node ./AdminRequests/AddSpecification.js ./example_specs/CheeseManufacturingSpec.json CHEESE_MANUFACTURE_SPEC_001

# Add cheese selling specification
node ./AdminRequests/AddSpecification.js ./example_specs/CheeseSellingSpec.json CHEESE_SELLING_SPEC_001

node ./AdminRequests/AddSpecification.js ./example_specs/CheeseSplittingSpec.json CHEESE_SPLITTING_SPEC_001

node ./AdminRequests/AddSpecification.js ./example_specs/CheeseBoxingSpec.json CHEESE_BOX_SPEC_001

node ./AdminRequests/AddSpecification.js ./example_specs/TransferSpec.json TRANSFER_SPEC_001

node ./AdminRequests/AddSpecification.js ./example_specs/UnboxingProcessSpec.json UNBOXING_SPEC_001


node install_chaincode.js "milkproducer_cc" "ver-DEV99999000160" "milkproducer"
node instantiate_chaincode.js "milkproducer_cc" "ver-DEV99999000160" "milkproducer" MILK_PRODUCTION_SPEC_001 MILK_SELLING_SPEC_001
#node upgrade_chaincode.js "milkproducer_cc" "ver-DEV0002" "milkproducer" MILK_PRODUCTION_SPEC_001 MILK_SELLING_SPEC_001

node install_chaincode.js "saltproducer_cc" "ver-DEV99999000160" "saltproducer"
node instantiate_chaincode.js "saltproducer_cc" "ver-DEV99999000160" "saltproducer" SALT_PRODUCTION_SPEC_001 SALT_SELLING_SPEC_001
#node upgrade_chaincode.js "saltproducer_cc" "ver-DEV0002" "saltproducer" SALT_PRODUCTION_SPEC_001 SALT_SELLING_SPEC_001

node install_chaincode.js "cheeseproducer_cc" "ver-DEV99999000160" "cheeseproducer"
node instantiate_chaincode.js "cheeseproducer_cc" "ver-DEV99999000160" "cheeseproducer" CHEESE_MANUFACTURE_SPEC_001 CHEESE_SELLING_SPEC_001 CHEESE_SELLING_SPEC_001 MILK_TRANSPORT_SPEC_001 CHEESE_SELLING_SPEC_001 CHEESE_SELLING_SPEC_001
#node upgrade_chaincode.js "cheeseproducer_cc" "ver-DEV0002" "cheeseproducer" SALT_PRODUCTION_SPEC_001 SALT_SELLING_SPEC_001

node ./AdminRequests/RegisterUser.js milk_producer_1 "$.created_item.qty" 1000

node ./AdminRequests/RegisterUser.js milk_producer_2 "$.created_item.qty" 1500

docker ps