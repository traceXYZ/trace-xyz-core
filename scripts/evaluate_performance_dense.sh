#!/bin/bash


#node install_chaincode.js "performance_cc" "ver-DEV500021" "graphperformance"
#node instantiate_chaincode.js "performance_cc" "ver-DEV300020" "graphperformance"
#node upgrade_chaincode.js "performance_cc" "ver-DEV300020" "graphperformance"

# Starting from 10 concurrent requests to 100 concurrent requests
# Add 100 nodes and 100 edges


for j in `seq 1 20`;
do
    for i in `seq 1 5`;
    do
        node ./PerformanceEvaluation/EvaluateInvoke_Dense.js 1 create_graph 100  $((1000*j))
#        sleep 10s
        node ./PerformanceEvaluation/EvaluateQuery.js 10 bfs
#        sleep 10s
        rm -rf ./PerformanceEvaluation/nodes.json
    done
    echo "Sleeping 30 seconds...."
    sleep 30s
done

 sleep 60s

for j in `seq 1 20`;
do
    for i in `seq 1 5`;
    do
        node ./PerformanceEvaluation/EvaluateInvoke_Dense.js 1 create_graph $((100*j))  100000
#        sleep 10s
        node ./PerformanceEvaluation/EvaluateQuery.js 10 bfs
#        sleep 10s
        rm -rf ./PerformanceEvaluation/nodes.json
    done
    echo "Sleeping 30 seconds...."
    sleep 30s
done

for j in `seq 10 20`;
do
    for i in `seq 1 5`;
    do
        node ./PerformanceEvaluation/EvaluateInvoke_SupplyChainGraph.js 10 supply_chain_simulation $((j))
#        sleep 10s
        node ./PerformanceEvaluation/EvaluateQuery.js 10 bfs
#        sleep 10s
        rm -rf ./PerformanceEvaluation/nodes.json
    done
    echo "Sleeping 30 seconds...."
    sleep 30s
done