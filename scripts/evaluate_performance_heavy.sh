#!/bin/bash


#node install_chaincode.js "performance_cc" "ver-DEV300020" "graphperformance"
#node instantiate_chaincode.js "performance_cc" "ver-DEV300020" "graphperformance"
#node upgrade_chaincode.js "performance_cc" "ver-DEV300020" "graphperformance"

# Starting from 10 concurrent requests to 100 concurrent requests
# Add 100 nodes and 100 edges


for j in `seq 11 12`;
do
    for i in `seq 1 5`;
    do
        node ./PerformanceEvaluation/EvaluateInvoke.js 200 create_graph $((10*j))  $((10*j))
#        sleep 10s
        node ./PerformanceEvaluation/EvaluateQuery.js 200 bfs
#        sleep 10s
        rm -rf ./PerformanceEvaluation/nodes.json
    done
    echo "Sleeping 30 seconds...."
    sleep 30s
done

