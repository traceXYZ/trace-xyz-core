'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var request = require('request-promise-native');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user = 'milk_producer_1';
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgytuYw+D/jKbaM0nJ\n" +
        "7xnBqxyH3By7LvijbHERQUnexCShRANCAAT5vIdqAlz9zNzRiIJHQLSE0P9tVge9\n" +
        "uL//lpRbcH/WDVH5byG70aregWGKZlXA4WqTzfwb2v3fFqLK4tecLQkM\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user assigned to fabric client
}).then(async (priv_key) => {

    var milkItem = {
        "itemType": "milk",
        "qty": 260,
        "item_image": "IMG"
    };

    var milkProductionProcess = {
        "processType": "Milk Production",
        "production_units": "L",
        "farmid": "FARM001",
        "location": "LOCAT1",
        "machine_id": "MACH001"
    };

    console.log("Created Milk Item");
    console.log("Created Milk Production Process");
    var milkItemBuffer = new Buffer(JSON.stringify(milkItem), 'utf8');
    var milkProdProcessBuffer = new Buffer(JSON.stringify(milkProductionProcess), 'utf8');

    console.log("Signed Milk Item");
    console.log("Signed Milk Production Process");
    var milkItemSignature = fabric_client.getCryptoSuite().sign(priv_key, milkItemBuffer);
    var milkProductionProcessSignature = fabric_client.getCryptoSuite().sign(priv_key, milkProdProcessBuffer);

    var milkProductionRequest = {};
    milkProductionRequest.milk_production_spec_id = "";
    milkProductionRequest.milk_producer_public_key = priv_key.getPublicKey().toBytes();
    milkProductionRequest.milk_item = Array.prototype.slice.call(milkItemBuffer, 0);
    milkProductionRequest.milk_item_signature = milkItemSignature;
    milkProductionRequest.milk_production_process = Array.prototype.slice.call(milkProdProcessBuffer, 0);
    milkProductionRequest.milk_production_process_signature = milkProductionProcessSignature;

    console.log("Generating Milk Production Request");
    var postRequest = {};
    postRequest.chaincode_id = 'milkproducer_cc';
    postRequest.function = 'produce_milk';
    postRequest.args = [JSON.stringify(milkProductionRequest)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.request_type = 'create';
    postRequest.item_type = 'milk';
    postRequest.sync = true;

    console.log("Sending Milk Production Request");
    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
});
