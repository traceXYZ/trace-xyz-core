'use strict';
/*
 * Chaincode Invoke
 */

'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var request = require('request-promise-native');
const db = require('../server/dataaccess.js');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);


const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');
var seller_priv_key;
var user_seller;
var buyer_priv_key;
var user_buyer;

console.log('Store path:' + store_path);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user_seller = 'milk_producer_1';
    return fabric_client.getUserContext(user_seller, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded user1 from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }


    // todo: import the users private key user_001 private key used here
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgytuYw+D/jKbaM0nJ\n" +
        "7xnBqxyH3By7LvijbHERQUnexCShRANCAAT5vIdqAlz9zNzRiIJHQLSE0P9tVge9\n" +
        "uL//lpRbcH/WDVH5byG70aregWGKZlXA4WqTzfwb2v3fFqLK4tecLQkM\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user_seller assigned to fabric client
}).then((priv_key) => {
    seller_priv_key = priv_key;

    user_buyer = 'cheese_producer';

    // todo: import the users private key user_002 private key used here
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgyJHFiQmxkifxoKy3\n" +
        "zAyBoedOcpvTdIFk2k2Wqi60P22hRANCAATijNJaE3IMQnqqgs/xt33/N7H0FvM2\n" +
        "cAyj4pLnXzG1+0sLClURLU5z2wrpDXffgUZnvf5+QvPdNci2wcEOFJb3\n" +
        "-----END PRIVATE KEY-----";

    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user_seller assigned to fabric client
}).then((priv_key) => sendRequest(priv_key));

async function sendRequest(priv_key) {
    // We create request after reading user_seller's private key
    buyer_priv_key = priv_key;

    var soldQty = {
        "itemType": "milk",
        "qty": 250,
        "item_image": "IMG"
    };

    var milkSellingProcess = {
        "processType": "Milk Selling",
        "sold_units": "L",
        "farmid": "FARM001",
        "location": "LOCAT1",
        "pos_machine_id": "MACH001"
    };

    var soldItemBuffer = new Buffer(JSON.stringify(soldQty), 'utf8');
    var milkSellingProcessBuffer = new Buffer(JSON.stringify(milkSellingProcess), 'utf8');

    console.log("Milk Seller Signed the selling quantity");
    var soldItemSellerSignature = fabric_client.getCryptoSuite().sign(seller_priv_key, soldItemBuffer);
    console.log("Milk Buyer Signed the selling quantity");
    var soldItemBuyerSignature = fabric_client.getCryptoSuite().sign(buyer_priv_key, soldItemBuffer);

    console.log("Milk Seller Signed the selling process");
    var milkSellingProcessSignature = fabric_client.getCryptoSuite().sign(seller_priv_key, milkSellingProcessBuffer);

    var fileDataObj;
    var milk_item_id;
    var milk_item_ownership_challenge;
    var milk_item_handling_challenge;
    var fileData;
    console.log("Picking challenge to authenticate in blockchain network");
    var limit = 0;
    while (true) {
        // if (limit > 10000) {
        //     console.error("Error! Cannot pick challenges after 100 retries");
        //     process.exit();
        //     break
        // }
        limit++;
        fileData = await db.query({user_id: user_seller}, args[0]);
        if (fileData.length === 0) {
            continue;
        }
        fileDataObj = fileData[0];
        for (var key in fileDataObj.owning_items) {
            if (fileDataObj.owning_items[key].owner !== "" && fileDataObj.owning_items[key].handler !== ""
                && fileDataObj.owning_items[key].type === 'milk' && fileDataObj.owning_items[key].count === 0) {
                milk_item_id = key;
                milk_item_ownership_challenge = fileDataObj.owning_items[key].owner;
                milk_item_handling_challenge = fileDataObj.owning_items[key].handler;
                fileDataObj.owning_items[key].owner = "";
                fileDataObj.owning_items[key].handler = "";
                break
            }
        }
        if (milk_item_id !== undefined) {
            await db.update(user_seller, fileDataObj, args[0]);
            break;
        }
    }

    console.log("Ownership Challenge: " + milk_item_ownership_challenge.toString());
    console.log("Handling Challenge: " + milk_item_handling_challenge.toString());

    var milk_item_owner_challenge_buffer = new Buffer(milk_item_ownership_challenge.toString(), 'utf8');
    var milk_item_handler_challenge_buffer = new Buffer(milk_item_handling_challenge.toString(), 'utf8');

    console.log("Signing Milk Ownership Challenge");
    var milk_item_ownership_challenge_response = fabric_client.getCryptoSuite().sign(seller_priv_key, milk_item_owner_challenge_buffer);
    console.log("Signing Milk Handling Challenge");
    var milk_item_handling_challenge_response = fabric_client.getCryptoSuite().sign(seller_priv_key, milk_item_handler_challenge_buffer);

    var milkSellingRequest = {};
    milkSellingRequest.milk_selling_spec_id = "";
    milkSellingRequest.selling_source_ownership_token = {
        item_id: milk_item_id,
        input_challenge_response: milk_item_ownership_challenge_response,
        used_qty: 100
    };
    milkSellingRequest.selling_source_handling_token = {
        item_id: milk_item_id,
        input_challenge_response: milk_item_handling_challenge_response,
    };
    milkSellingRequest.selling_quantity_object = Array.prototype.slice.call(soldItemBuffer, 0);
    milkSellingRequest.milk_seller_public_key = seller_priv_key.getPublicKey().toBytes();
    milkSellingRequest.seller_signature_for_qty = soldItemSellerSignature;
    milkSellingRequest.milk_buyer_public_key = buyer_priv_key.getPublicKey().toBytes();
    milkSellingRequest.buyer_signature_for_qty = soldItemBuyerSignature;
    milkSellingRequest.milk_selling_process = Array.prototype.slice.call(milkSellingProcessBuffer, 0);
    milkSellingRequest.milk_selling_process_signature = milkSellingProcessSignature;

    console.log("Generating Milk Selling Request");
    var postRequest = {};
    postRequest.chaincode_id = 'milkproducer_cc';
    postRequest.function = 'sell_milk';
    postRequest.args = [JSON.stringify(milkSellingRequest)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.request_type = 'sell_raw';
    postRequest.item_type = 'milk';
    postRequest.buyer = user_buyer;
    postRequest.sold_item = milk_item_id;
    postRequest.sync = true;

    console.log("Sending Milk Selling Request");
    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user_seller, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
}

