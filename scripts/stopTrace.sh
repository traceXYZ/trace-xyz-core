#!/bin/bash
# Exit on first error
set -e

# stop blockchain network
cd ../networks/advanced-network
./stop.sh

#sleep
sleep 10

printf "Successfully stopped the Blockchain Network\n"

