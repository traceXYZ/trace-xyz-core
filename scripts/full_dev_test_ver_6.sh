#!/bin/bash

#mkdir UserContexts$1

# Day 1 Milk/Salt Produced sold and transported to the factory
node ./MilkProducer_1/ProduceMilk.js $1 $2
node ./MilkProducer_2/ProduceMilk.js $1 $2
node ./SaltProducer/ProduceSalt.js $1 $2
node ./MilkProducer_1/SellMilk.js $1 $2
node ./SaltProducer/SellSalt.js $1 $2
node ./CheeseProducer/TransportMilk.js $1 $2
node ./CheeseProducer/IOT_Assign_TransportMilk.js $1 $2
for i in `seq 1 30`;
do
 node ./CheeseProducer/IOT_Update_TransportMilk.js $1 $2
done
node ./MilkProducer_2/SellMilk.js $1 $2
node ./CheeseProducer/TransportMilk.js $1 $2
node ./CheeseProducer/IOT_Assign_TransportMilk.js $1 $2
for i in `seq 1 30`;
do
 node ./CheeseProducer/IOT_Update_TransportMilk.js $1 $2
done
node ./CheeseProducer/TransportSalt.js $1 $2
node ./CheeseProducer/IOT_Assign_TransportSalt.js $1 $2
for i in `seq 1 30`;
do
 node ./CheeseProducer/IOT_Update_TransportSalt.js $1 $2
done

# Day 1 All purchased milk and some salt is used for manufacturing Cheese and cheddar
node ./CheeseProducer/ManufactureCheese.js $1 $2
node ./CheeseProducer/IOT_Assign_CheeseManufacture.js $1 $2
for i in `seq 1 48`;
do
 node ./CheeseProducer/IOT_Update_CheeseManufacture.js $1 $2
done

node ./CheeseProducer/ManufactureCheddar.js $1 $2
node ./CheeseProducer/IOT_Assign_CheddarManufacture.js $1 $2
 #Day 2-100 Cheese is aged
for i in `seq 49 50`;
do
 node ./CheeseProducer/IOT_Update_CheeseManufacture.js $1 $2
 node ./CheeseProducer/IOT_Update_CheddarManufacture.js $1 $2
done
for i in `seq 1 48`;
do
 node ./CheeseProducer/IOT_Update_CheddarManufacture.js $1 $2
done

# Day 100 Cheese and Cheddar is split into packages and boxed to send to retailers
node ./CheeseProducer/SplitCheddar.js $1 $2
node ./CheeseProducer/SplitCheese.js $1 $2
node ./CheeseProducer/BoxCheese_Retailer_A.js $1 $2
node ./CheeseProducer/BoxCheese_Retailer_B.js $1 $2
node ./CheeseProducer/BoxCheese_Retailer_C.js $1 $2
node ./CheeseProducer/BoxCheddar_Retailer_D.js $1 $2
node ./CheeseProducer/BoxCheddar_Retailer_E.js $1 $2

# Day 101 Transported to local retailers
node ./CheeseProducer/Transport_Retailer_A.js $1 $2
node ./CheeseProducer/IOT_Assign_TransportRetailer_A.js $1 $2
for i in `seq 1 30`;
do
 node ./CheeseProducer/IOT_Update_TransportRetailer_A.js $1 $2
done

node ./CheeseProducer/Transport_Retailer_D.js $1 $2
node ./CheeseProducer/IOT_Assign_TransportRetailer_D.js $1 $2
for i in `seq 1 30`;
do
 node ./CheeseProducer/IOT_Update_TransportRetailer_D.js $1 $2
done

# All requests 8210

