'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var request = require('request-promise-native');
const db = require('../server/dataaccess.js');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user = 'cheese_producer';

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded user1 from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }

    // todo: import the users private key
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgyJHFiQmxkifxoKy3\n" +
        "zAyBoedOcpvTdIFk2k2Wqi60P22hRANCAATijNJaE3IMQnqqgs/xt33/N7H0FvM2\n" +
        "cAyj4pLnXzG1+0sLClURLU5z2wrpDXffgUZnvf5+QvPdNci2wcEOFJb3\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user assigned to fabric client
}).then(async (priv_key) => {
    // We create request after reading user's private key

    var cheeseItem = {
        "itemType": "Cheddar",
        "qty": 10,
        "item_image": "IMG"
    };

    var cheeseProductionProcess = {
        "processType": "Cheddar Production",
        "production_units": "KG",
        "fermenterid": "FM001",
        "location": "LOCAT3",
        "machine_id": "MACH003"
    };
    console.log("Generating Cheddar Manufacturing Process");
    var fileData;
    var fileDataObj;
    var salt_item_id;
    var salt_item_ownership_challenge;
    var salt_item_handling_challenge;
    var milk_item_id;
    var milk_item_ownership_challenge;
    var milk_item_handling_challenge;
    console.log("Picking challenges to authenticate in blockchain network");
    var limit = 0;
    while (true) {
        // if (limit > 100) {
        //     console.error("Error! Cannot pick challenges after 100 retries");
        //     process.exit();
        //     break
        // }
        // limit++;
        fileData = await db.query({user_id: user}, args[0]);
        if (fileData.length === 0) {
            continue;
        }
        fileDataObj = fileData[0];
        for (var key in fileDataObj.owning_items) {
            if (salt_item_id === undefined) {
                if (fileDataObj.owning_items[key].owner !== "" && fileDataObj.owning_items[key].handler !== ""
                    && fileDataObj.owning_items[key].type === 'salt' && fileDataObj.owning_items[key].count < 60) {
                    salt_item_id = key;
                    salt_item_ownership_challenge = fileDataObj.owning_items[key].owner;
                    salt_item_handling_challenge = fileDataObj.owning_items[key].handler;
                    fileDataObj.owning_items[key].owner = "";
                    fileDataObj.owning_items[key].handler = "";
                    console.log("Picked Salt For Manufacturing Cheddar");
                }
            }
            if (milk_item_id === undefined) {
                if (fileDataObj.owning_items[key].owner !== "" && fileDataObj.owning_items[key].handler !== ""
                    && fileDataObj.owning_items[key].type === 'milk' && fileDataObj.owning_items[key].count === 2) {
                    milk_item_id = key;
                    milk_item_ownership_challenge = fileDataObj.owning_items[key].owner;
                    milk_item_handling_challenge = fileDataObj.owning_items[key].handler;
                    fileDataObj.owning_items[key].owner = "";
                    fileDataObj.owning_items[key].handler = "";
                    console.log("Picked Milk For Manufacturing Cheddar");
                }
            }
        }
        if (salt_item_id !== undefined && milk_item_id !== undefined) {
            await db.update(user, fileDataObj, args[0]);
            break;
        }
    }

    var cheeseItemBuffer = new Buffer(JSON.stringify(cheeseItem), 'utf8');
    var cheeseProductionProcessBuffer = new Buffer(JSON.stringify(cheeseProductionProcess), 'utf8');
    console.log("Signing Cheddar Manufacturing Process");
    var cheeseItemSignature = fabric_client.getCryptoSuite().sign(priv_key, cheeseItemBuffer);
    var cheeseProductionProcessSignature = fabric_client.getCryptoSuite().sign(priv_key, cheeseProductionProcessBuffer);

    var milk_item_owner_challenge_buffer = new Buffer(milk_item_ownership_challenge.toString(), 'utf8');
    var milk_item_handler_challenge_buffer = new Buffer(milk_item_handling_challenge.toString(), 'utf8');

    console.log("Signing Milk Item Ownership Challenge");
    var milk_item_ownership_challenge_response = fabric_client.getCryptoSuite().sign(priv_key, milk_item_owner_challenge_buffer);
    console.log("Signing Milk Item Handling Challenge");
    var milk_item_handling_challenge_response = fabric_client.getCryptoSuite().sign(priv_key, milk_item_handler_challenge_buffer);

    var salt_item_owner_challenge_buffer = new Buffer(salt_item_ownership_challenge.toString(), 'utf8');
    var salt_item_handler_challenge_buffer = new Buffer(salt_item_handling_challenge.toString(), 'utf8');
    console.log("Signing Salt Item Ownership Challenge");
    var salt_item_ownership_challenge_response = fabric_client.getCryptoSuite().sign(priv_key, salt_item_owner_challenge_buffer);
    console.log("Signing Salt Item Handling Challenge");
    var salt_item_handling_challenge_response = fabric_client.getCryptoSuite().sign(priv_key, salt_item_handler_challenge_buffer);


    var cheeseManufactureRequest = {};
    cheeseManufactureRequest.manufacturing_spec_id = "";
    cheeseManufactureRequest.input_ownership_tokens = [
        {
            item_id: salt_item_id,
            input_challenge_response: salt_item_ownership_challenge_response,
            used_qty: 1
        }, {
            item_id: milk_item_id,
            input_challenge_response: milk_item_ownership_challenge_response,
            used_qty: 100
        }
    ];
    cheeseManufactureRequest.input_handling_tokens = [
        {
            item_id: salt_item_id,
            input_challenge_response: salt_item_handling_challenge_response,
        }, {
            item_id: milk_item_id,
            input_challenge_response: milk_item_handling_challenge_response,
        }
    ];
    cheeseManufactureRequest.owner_public_key = priv_key.getPublicKey().toBytes();
    cheeseManufactureRequest.manufacturing_process = Array.prototype.slice.call(cheeseProductionProcessBuffer, 0);
    cheeseManufactureRequest.manufacture_process_signature = cheeseProductionProcessSignature;
    cheeseManufactureRequest.cheese_item = Array.prototype.slice.call(cheeseItemBuffer, 0);
    cheeseManufactureRequest.cheese_item_signature = cheeseItemSignature;

    console.log("Generating Cheddar Manufacturing Request");
    var postRequest = {};
    postRequest.chaincode_id = 'cheeseproducer_cc';
    postRequest.function = 'produce_cheese';
    postRequest.args = [JSON.stringify(cheeseManufactureRequest)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.request_type = 'manufacture';
    postRequest.item_type = 'cheddar';
    postRequest.milk_id = milk_item_id;
    postRequest.salt_id = salt_item_id;

    console.log("Sending Cheese Manufacturing Request");
    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
});