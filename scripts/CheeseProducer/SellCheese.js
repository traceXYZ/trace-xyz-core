'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var os = require('os');
var fs = require('fs');
var winston = require('winston');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const logDir = '../logs';
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}
const now = new Date();
var logger = winston.createLogger({
    level: 'debug',
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/cheese_producer.log',
            level: 'info',
            json: false,
            timestamp: true
        }),

    ]
});
var request;
//
var fabric_client = new Fabric_Client();
var seller_priv_key;
var buyer_priv_key;
// setup the fabric network
var channel = fabric_client.newChannel('cheeseproduction');
var peer = fabric_client.newPeer('grpc://localhost:7051');
channel.addPeer(peer);
var order = fabric_client.newOrderer('grpc://localhost:7050')
channel.addOrderer(order);
var user_seller;
var user_buyer;


var args = process.argv.slice(2);

//
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');
console.log('Store path:' + store_path);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);

    // user_admin is a user_seller with org1.admin affiliation value set
    user_seller = 'user_002';

    // get the enrolled user_seller from persistence, this user_seller will sign all requests
    return fabric_client.getUserContext(user_seller, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded user1 from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }


    // todo: import the users private key user_001 private key used here
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQg/oGk2Ui0V5UzrhMp\n" +
        "E4FbzcfV52hVaNW9J07CIKpIvxWhRANCAASsclTEmA2ZPmeix6MxYNwAbakbQiYq\n" +
        "XT3Ev2OMtmkCj89UsxDU88tpO55E8im3j3mtlAox3LHa5aq40KZE2Q8U\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user_seller assigned to fabric client
}).then((priv_key) => {
    seller_priv_key = priv_key;

    user_buyer = 'user_007';

    // todo: import the users private key user_002 private key used here
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgwPN9basMItPsg6g2\n" +
        "QTItdrLT5vBQpFuN0N4bnDzLtOmhRANCAASm70uwLCVHV+OrEUoXnDuOtPXaMLA/\n" +
        "Pl7ZTPnoFe2Tx9k/nqYyVbyARO+RQVKQOiw72uHLlOxaY3O3Q8GUBvup\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user_seller assigned to fabric client
}).then((priv_key) => {
    // We create request after reading user_seller's private key
    buyer_priv_key = priv_key;
    tx_id = fabric_client.newTransactionID();
    console.log("Assigning transaction_id: ", tx_id._transaction_id);

    request = {
        //targets: let default to the peer assigned to the client
        chaincodeId: 'cheeseproducer_cc',
        fcn: 'sell_cheese',
        args: [''],
        chainId: 'cheeseproduction',
        txId: tx_id,
    };


    var cheeseItem = {
        "itemType": "cheese",
        "qty": 100,
        "item_image": "IMG"
    };

    var cheeseSellingProcess = {
        "processType": "Cheese Selling",
        "sold_units": "KG",
        "location": "LOCAT3",
        "pos_machine_id": "MACH004"
    };

    var cheeseItemBuffer = new Buffer(JSON.stringify(cheeseItem), 'utf8');
    var cheeseSellingProcessBuffer = new Buffer(JSON.stringify(cheeseSellingProcess), 'utf8');

    var cheeseItemBuyerSignature = fabric_client.getCryptoSuite().sign(buyer_priv_key, cheeseItemBuffer);


    var cheeseSellingProcessSignature = fabric_client.getCryptoSuite().sign(seller_priv_key, cheeseSellingProcessBuffer);
    logger.info('Request:', request);


    var fileData = fs.readFileSync('./UserContexts' + args[0] + '/' + user_seller + '.json');
    var fileDataObj = JSON.parse(fileData.toString());
    var cheese_item_id;
    var cheese_item_ownership_challenge;
    var cheese_item_handling_challenge;
    logger.debug(fileDataObj.owning_items);

    for (var key in fileDataObj.owning_items) {
        if (fileDataObj.owning_items[key].owner != "" && fileDataObj.owning_items[key].handler != ""
            && fileDataObj.owning_items[key].type == 'cheese') {
            cheese_item_id = key;
            cheese_item_ownership_challenge = fileDataObj.owning_items[key].owner;
            cheese_item_handling_challenge = fileDataObj.owning_items[key].handler;
        } else {
            continue
        }
        fileDataObj.owning_items[key].owner = "";
        fileDataObj.owning_items[key].handler = "";
        break
    }
    if (cheese_item_id == "" || cheese_item_id == undefined) {
        console.log("Cannot find the update challenge. quitting...");
        return
    }
    console.log("Successfully picked inputs");
    fs.writeFileSync('./UserContexts' + args[0] + '/' + user_seller + '.json', JSON.stringify(fileDataObj));

    console.log('OWNER');
    console.log(cheese_item_ownership_challenge.toString());
    console.log('HANDLER');
    console.log(cheese_item_handling_challenge.toString());

    var cheese_item_owner_challenge_buffer = new Buffer(cheese_item_ownership_challenge.toString(), 'utf8');
    var cheese_item_handler_challenge_buffer = new Buffer(cheese_item_handling_challenge.toString(), 'utf8');
    var cheese_item_ownership_challenge_response = fabric_client.getCryptoSuite().sign(seller_priv_key, cheese_item_owner_challenge_buffer);
    var cheese_item_handling_challenge_response = fabric_client.getCryptoSuite().sign(seller_priv_key, cheese_item_handler_challenge_buffer);

    // type CheeseSellingRequest struct {
    //     SellingSpecID string `json:"selling_spec_id"`
    //     TransportByBuyer bool `json:"transport_by_buyer"`
    //     CheeseOwnershipToken datastructs.InputItemToken `json:"cheese_ownership_token"`
    //     CheeseHandlingToken  datastructs.InputToken `json:"cheese_handling_token"`
    //     CheeseOwnerPublicKey string `json:"cheese_owner_public_key"`
    //     BuyerPublicKey string `json:"buyer_public_key"`
    //     BuyerSignatureForCheeseItem []byte `json:"buyer_signature_for_cheese_item"`
    //     SellingProcess []byte`json:"selling_process"`
    //     SellingProcessSignature []byte `json:"selling_process_signature"`
    // }

    var cheeseSellingRequest = {};
    cheeseSellingRequest.selling_spec_id = "";
    cheeseSellingRequest.transport_by_buyer = true;
    cheeseSellingRequest.cheese_ownership_token = {
        item_id: cheese_item_id,
        input_challenge_response: cheese_item_ownership_challenge_response,
    };
    cheeseSellingRequest.cheese_handling_token = {
        item_id: cheese_item_id,
        input_challenge_response: cheese_item_handling_challenge_response,
    };
    cheeseSellingRequest.cheese_owner_public_key = seller_priv_key.getPublicKey().toBytes();
    cheeseSellingRequest.buyer_public_key = buyer_priv_key.getPublicKey().toBytes();
    cheeseSellingRequest.buyer_signature_for_cheese_item = cheeseItemBuyerSignature;
    cheeseSellingRequest.selling_process = Array.prototype.slice.call(cheeseSellingProcessBuffer, 0);
    cheeseSellingRequest.selling_process_signature = cheeseSellingProcessSignature;

    request.args[0] = JSON.stringify(cheeseSellingRequest);

    logger.debug('Request:', "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    logger.debug(cheeseSellingRequest);
    return channel.sendTransactionProposal(request, 2000);


    // send the transaction proposal to the peers
}).then((results) => {
    let isProposalGood = false;
    var proposalResponses = results[0];
    var proposal = results[1];
    // console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    // console.log(proposalResponses[1].response)
    // console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    if (proposalResponses && proposalResponses[0].response && proposalResponses[0].response.status === 200) {

        isProposalGood = true;
        console.log('Transaction proposal was good');
        console.log('\x1b[33m%s\x1b[0m', 'START RESULT');
        console.log('\x1b[36m%s\x1b[0m', proposalResponses[0].response.payload);
        var response = JSON.parse(proposalResponses[0].response.payload);
        var updated_item_tokens = response.storage_network_response.updated_item_tokens;
        var updated_relationship_tokens = response.storage_network_response.updated_relationship_tokens;
        console.log();
        var sold_item_id = response.selling_item_id;
        var i;
        for (i = 0; i < updated_item_tokens.length; i++) {
            if (updated_item_tokens[i].item_id.includes(sold_item_id)) {
                var fileData;
                try {
                    fileData = fs.readFileSync('./UserContexts' + args[0] + '/' + user_seller + '.json');
                } catch (e) {
                    fileData = fs.openSync('./UserContexts' + args[0] + '/' + user_seller + '.json', 'w+');
                    fs.closeSync(fileData);
                    fileData = fs.readFileSync('./UserContexts' + args[0] + '/' + user_seller + '.json');
                }

                var userObject;
                if (fileData.toString() == "") {
                    userObject = {};
                } else {
                    userObject = JSON.parse(fileData.toString());
                }
                var item_token = updated_item_tokens[i].item_id;
                if (userObject.owning_items == null) {
                    userObject.owning_items = {};
                }
                if (userObject.owning_items[item_token] == null) {
                    userObject.owning_items[item_token] = {};
                }
                userObject.owning_items[item_token].owner = updated_item_tokens[i].next_update_challenge;
                fs.writeFileSync('./UserContexts' + args[0] + '/' + user_seller + '.json', JSON.stringify(userObject));
                //fs.closeSync(file)
            }
        }
        for (i = 0; i < updated_relationship_tokens.length; i++) {
            if (updated_relationship_tokens[i].item_id.includes(sold_item_id)) {
                var fileData;
                try {
                    fileData = fs.readFileSync('./UserContexts' + args[0] + '/' + user_seller + '.json');
                } catch (e) {
                    fileData = fs.openSync('./UserContexts' + args[0] + '/' + user_seller + '.json', 'w+');
                    fs.closeSync(fileData);
                    fileData = fs.readFileSync('./UserContexts' + args[0] + '/' + user_seller + '.json');
                }

                var userObject;
                if (fileData.toString() == "") {
                    userObject = {};
                } else {
                    userObject = JSON.parse(fileData.toString());
                }
                var item_token = updated_relationship_tokens[i].item_id;
                if (userObject.owning_items == null) {
                    userObject.owning_items = {};
                }
                if (userObject.owning_items[item_token] == null) {
                    userObject.owning_items[item_token] = {};
                }
                userObject.owning_items[item_token].handler = updated_relationship_tokens[i].next_update_challenge;
                fs.writeFileSync('./UserContexts' + args[0] + '/' + user_seller + '.json', JSON.stringify(userObject));
                console.log(userObject);
                //fs.closeSync(file)
            }
        }
        var stored_item_tokens = response.storage_network_response.stored_item_tokens;
        var stored_item_id = response.selling_quantity_key;
        var i;
        for (i = 0; i < stored_item_tokens.length; i++) {
            if (stored_item_tokens[i].item_id.includes(stored_item_id)) {
                var fileData;
                try {
                    fileData = fs.readFileSync('./UserContexts' + args[0] + '/' + user_buyer + '.json');
                } catch (e) {
                    fileData = fs.openSync('./UserContexts' + args[0] + '/' + user_buyer + '.json', 'w+');
                    fs.closeSync(fileData);
                    fileData = fs.readFileSync('./UserContexts' + args[0] + '/' + user_buyer + '.json');
                }

                var userObject;
                if (fileData.toString() == "") {
                    userObject = {};
                } else {
                    userObject = JSON.parse(fileData.toString());
                }
                var item_token = stored_item_tokens[i].item_id;
                if (userObject.owning_items == null) {
                    userObject.owning_items = {};
                }
                if (userObject.owning_items[item_token] == null) {
                    userObject.owning_items[item_token] = {};
                }

                userObject.owning_items[item_token].owner = stored_item_tokens[i].next_update_challenge;
                userObject.owning_items[item_token].handler = stored_item_tokens[i].next_update_challenge;
                userObject.owning_items[item_token].type = 'milk';
                console.log(userObject);
                fs.writeFileSync('./UserContexts' + args[0] + '/' + user_buyer + '.json', JSON.stringify(userObject));
                //fs.closeSync(file)
            }
        }
        console.log('\x1b[33m%s\x1b[0m', 'END RESULT');
    } else {
        console.error('Transaction proposal was bad');
    }
    if (isProposalGood) {
        console.log(util.format(
            'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
            proposalResponses[0].response.status, proposalResponses[0].response.message));
        // build up the request for the orderer to have the transaction committed
        var request = {
            proposalResponses: proposalResponses,
            proposal: proposal
        };

        // set the transaction listener and set a timeout of 30 sec
        // if the transaction did not get committed within the timeout period,
        // report a TIMEOUT status
        var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
        var promises = [];

        var sendPromise = channel.sendTransaction(request);
        promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

        // get an eventhub once the fabric client has a user_seller assigned. The user_seller
        // is required bacause the event registration must be signed
        let event_hub = fabric_client.newEventHub();
        event_hub.setPeerAddr('grpc://localhost:7053');

        // using resolve the promise so that result status may be processed
        // under the then clause rather than having the catch clause process
        // the status
        let txPromise = new Promise((resolve, reject) => {
            let handle = setTimeout(() => {
                event_hub.disconnect();
                resolve({event_status: 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
            }, 3000);
            event_hub.connect();
            event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                // this is the callback for transaction event status
                // first some clean up of event listener
                clearTimeout(handle);
                event_hub.unregisterTxEvent(transaction_id_string);
                event_hub.disconnect();

                // now let the application know what happened
                var return_status = {event_status: code, tx_id: transaction_id_string};
                if (code !== 'VALID') {
                    console.error('The transaction was invalid, code = ' + code);
                    resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                } else {
                    console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                    resolve(return_status);
                }
            }, (err) => {
                //this is the callback if something goes wrong with the event registration or processing
                reject(new Error('There was a problem with the eventhub ::' + err));
            });
        });
        promises.push(txPromise);

        return Promise.all(promises);
    } else {

        throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    }
}).then((results) => {
    console.log('Send transaction promise and event listener promise have completed');
    // check the results in the order the promises were added to the promise all list
    if (results && results[0] && results[0].status === 'SUCCESS') {
        console.log('Successfully sent transaction to the orderer.');
    } else {
        console.error('Failed to order the transaction. Error code: ' + response.status);
    }

    if (results && results[1] && results[1].event_status === 'VALID') {
        console.log('Successfully transaction executed by the peer');


    } else {
        console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
    }
}).catch((err) => {
    console.error('Failed to invoke successfully :: ' + err);
});
