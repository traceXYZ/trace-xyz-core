'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var fs = require('fs');
var request = require('request-promise-native');
const db = require('../server/dataaccess.js');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');
var iot_user;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user = 'cheese_producer';

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }
    var prKey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQg2wuEUd5rBtCjDXzx\n" +
        "BmEuaQw3QHRmuUIrKgJAgziye66hRANCAATd7O20dEq8gOlrlNZKC8tGs6acg3bZ\n" +
        "Bj6oZ0XZ90BjVE1c54IDuaP6HPUBQwt+e34BaWEMXtpphBaenBnflJnS\n" +
        "-----END PRIVATE KEY-----";


    return fabric_client.getCryptoSuite().importKey(prkey)
}).then((priv_key) => {
    iot_user = priv_key;
    // todo: import the users private key
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgyJHFiQmxkifxoKy3\n" +
        "zAyBoedOcpvTdIFk2k2Wqi60P22hRANCAATijNJaE3IMQnqqgs/xt33/N7H0FvM2\n" +
        "cAyj4pLnXzG1+0sLClURLU5z2wrpDXffgUZnvf5+QvPdNci2wcEOFJb3\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user assigned to fabric client
}).then(async (priv_key) => {
    // We create request after reading user's private key

    var transferProcess = {
        "processType": "IOT Register",
    };

    var milkTransportProcess = {
        "processType": "Milk Transport",
        "location": "LOCAT3",
        "lorry_id": "ABC-1234"
    };


    var transferProcessBuffer = new Buffer(JSON.stringify(transferProcess), 'utf8');
    var transferProcessSignature = fabric_client.getCryptoSuite().sign(priv_key, transferProcessBuffer);
    var transportProcessBuffer = new Buffer(JSON.stringify(milkTransportProcess), 'utf8');
    var transportProcessSignature = fabric_client.getCryptoSuite().sign(iot_user, transportProcessBuffer);

    var fileData;
    var fileDataObj;
    var process_id;
    var process_handling_challenge;
    var limit = 0;
    while (true) {
        if (limit > 100) {
            console.error("Error! Cannot pick challenges after 100 retries");
            process.exit();
            break
        }
        limit++;
        fileData = await db.query({user_id: user}, args[0]);
        if (fileData.length === 0) {
            continue;
        }
        fileDataObj = fileData[0];
        for (var key in fileDataObj.handling_processes) {
            //console.log(fileDataObj.owning_items);
            if (process_id === undefined) {
                if (fileDataObj.handling_processes[key].handler !== ""
                    && fileDataObj.handling_processes[key].process === 'cheese_producer_transport'
                    && fileDataObj.handling_processes[key].count < 30) {
                    process_id = key;
                    process_handling_challenge = fileDataObj.handling_processes[key].handler;
                    fileDataObj.handling_processes[key].handler = "";
                    console.log("Picked A Process");
                    break;
                }
            }
        }
        if (process_id !== undefined) {
            await db.update(user, fileDataObj, args[0]);
            break;
        }
    }

    var process_ownership_response = fabric_client.getCryptoSuite().sign(priv_key, process_handling_challenge);
    var process_handling_response = fabric_client.getCryptoSuite().sign(priv_key, process_handling_challenge);

    // type TransferRequest struct {
    //     SellingSpecID            string                 `json:"selling_spec_id"`
    //     OwnershipToken           datastructs.InputToken `json:"ownership_token"`
    //     HandlingToken            datastructs.InputToken `json:"handling_token"`
    //     OwnerPublicKey           string                 `json:"owner_public_key"`
    //     ReceiverPublicKey        string                 `json:"receiver_public_key"`
    //     ReceiverSignatureForItem []byte                 `json:"receiver_signature_for_item"`
    //     TransferProcess          []byte                 `json:"transfer_process"`
    //     TransferProcessSignature []byte                 `json:"transfer_process_signature"`
    //     TransferType             int                    `json:"transfer_type"`
    // }

    var specData = fs.readFileSync('./ChaincodeInitParams/TRANSFER_SPEC_001.txt');

    var transferRequest = {};
    transferRequest.splitting_spec_id = specData.toString();
    transferRequest.ownership_token = {
        item_id: process_id,
        input_challenge_response: process_ownership_response,
    };
    transferRequest.handling_token = {
        item_id: process_id,
        input_challenge_response: process_handling_response,
    };
    transferRequest.owner_public_key = priv_key.getPublicKey().toBytes();
    transferRequest.receiver_public_key = iot_user.getPublicKey().toBytes();
    transferRequest.receiver_signature_for_item = transportProcessSignature;
    transferRequest.transfer_process = Array.prototype.slice.call(transferProcessBuffer, 0);
    transferRequest.transfer_process_signature = transferProcessSignature;
    transferRequest.transfer_type = 2;

    var postRequest = {};
    postRequest.chaincode_id = 'cheeseproducer_cc';
    postRequest.function = 'transfer_item';
    postRequest.args = [JSON.stringify(transferRequest)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.sync = true;

    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
});