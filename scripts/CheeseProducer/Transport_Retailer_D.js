'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var fs = require('fs');
var request = require('request-promise-native');
const db = require('../server/dataaccess.js');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user = 'cheese_producer';

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }

    // todo: import the users private key user_002 private key used here
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgyJHFiQmxkifxoKy3\n" +
        "zAyBoedOcpvTdIFk2k2Wqi60P22hRANCAATijNJaE3IMQnqqgs/xt33/N7H0FvM2\n" +
        "cAyj4pLnXzG1+0sLClURLU5z2wrpDXffgUZnvf5+QvPdNci2wcEOFJb3\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user_transporter assigned to fabric client
}).then(async (priv_key) => {
    // We create request after reading user's private key

    var cheeseTransportProcess = {
        "processType": "Cheddar Box Transport",
        "location": "LOCAT3",
        "lorry_id": "ABC-1235"
    };

    console.log("Generating Cheddar Box Transport Process to Retailer D");
    var milkTransportProcessBuffer = new Buffer(JSON.stringify(cheeseTransportProcess), 'utf8');
    console.log("Signing Cheese Box Transport Process to Retailer D");
    var milkTransportProcessSignature = fabric_client.getCryptoSuite().sign(priv_key, milkTransportProcessBuffer);

    var fileData;
    var fileDataObj;
    var cheese_box_id;
    var cheese_box_ownership_challenge;
    var cheese_box_handling_challenge;

    console.log("Picking challenge to authenticate in blockchain network");
    var limit = 0;
    while (true) {
        // if (limit > 100) {
        //     console.error("Error! Cannot pick challenges after 100 retries");
        //     process.exit();
        //     break
        // }
        // limit++;
        fileData = await db.query({user_id: user}, args[0]);
        if (fileData.length === 0) {
            continue;
        }
        fileDataObj = fileData[0];
        for (var key in fileDataObj.owning_items) {
            if (cheese_box_id === undefined) {
                if (fileDataObj.owning_items[key].owner !== "" && fileDataObj.owning_items[key].handler !== ""
                    && fileDataObj.owning_items[key].type === 'cheddar_box_retailer_D' && fileDataObj.owning_items[key].count === 0) {
                    cheese_box_id = key;
                    cheese_box_ownership_challenge = fileDataObj.owning_items[key].owner;
                    cheese_box_handling_challenge = fileDataObj.owning_items[key].handler;
                    fileDataObj.owning_items[key].handler = "";
                    console.log("Picked Cheddar Box D for Transport");
                    break;
                }
            }
        }
        if (cheese_box_id !== undefined) {
            await db.update(user, fileDataObj, args[0]);
            break;
        }
    }

    console.log("Handling Challenge: " + cheese_box_handling_challenge.toString());
    var cheese_box_handler_challenge_buffer = new Buffer(cheese_box_handling_challenge.toString(), 'utf8');
    console.log("Signing Cheddar Box Handling Challenge");
    var cheese_box_handling_challenge_response = fabric_client.getCryptoSuite().sign(priv_key, cheese_box_handler_challenge_buffer);


    var specData = fs.readFileSync('./ChaincodeInitParams/MILK_TRANSPORT_SPEC_001.txt');

    var milkTransportRequest = {};
    milkTransportRequest.transport_spec_id = specData.toString();
    milkTransportRequest.transport_item_handling_token = {
        item_id: cheese_box_id,
        input_challenge_response: cheese_box_handling_challenge_response,
    };
    milkTransportRequest.transporter_public_key = priv_key.getPublicKey().toBytes();
    milkTransportRequest.transport_process = Array.prototype.slice.call(milkTransportProcessBuffer, 0);
    milkTransportRequest.transport_process_signature = milkTransportProcessSignature;

    console.log("Generating Cheese Box Transport Request");
    var postRequest = {};
    postRequest.chaincode_id = 'cheeseproducer_cc';
    postRequest.function = 'item_transport';
    postRequest.args = [JSON.stringify(milkTransportRequest)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.request_type = 'transport';
    postRequest.item_id = cheese_box_id;
    postRequest.sync = true;

    console.log("Sending Cheese Box Transport Request");
    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
});