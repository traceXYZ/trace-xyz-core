'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var fs = require('fs');
var request = require('request-promise-native');
const db = require('../server/dataaccess.js');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');
var iot_user;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user = 'cheese_producer';

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext(user, true);
}).then((priv_key) => {
    // todo: import the users private key
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgyJHFiQmxkifxoKy3\n" +
        "zAyBoedOcpvTdIFk2k2Wqi60P22hRANCAATijNJaE3IMQnqqgs/xt33/N7H0FvM2\n" +
        "cAyj4pLnXzG1+0sLClURLU5z2wrpDXffgUZnvf5+QvPdNci2wcEOFJb3\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user assigned to fabric client
}).then(async (priv_key) => {
    // We create request after reading user's private key
    iot_user = priv_key;

    console.log("Generating IOT update");
    var iotUpdate = {
        "update_type": "Temperature Report",
        "location": "LOCAT3",
        "temp": 30 + Math.floor(Math.random() * 11)
    };


    var iotUpdateBuffer = new Buffer(JSON.stringify(iotUpdate), 'utf8');
    var iotUpdateSignature = fabric_client.getCryptoSuite().sign(iot_user, iotUpdateBuffer);

    var fileData;
    var fileDataObj;
    var process_id;
    var process_handling_challenge;
    console.log("Picking challenges to authenticate in blockchain network");
    var limit = 0;
    var limit = 0;
    while (true) {
        // if (limit > 100) {
        //     console.error("Error! Cannot pick challenges after 100 retries");
        //     process.exit();
        //     break
        // }
        // limit++;
        // if (limit > 100) {
        //     console.error("Error! Cannot pick challenges after 100 retries");
        //     process.exit();
        //     break
        // }
        // limit++;
        fileData = await db.query({user_id: user}, args[0]);
        if (fileData.length === 0) {
            continue;
        }
        fileDataObj = fileData[0];
        for (var key in fileDataObj.handling_processes) {
            if (process_id === undefined) {
                if (fileDataObj.handling_processes[key].handler !== ""
                    && fileDataObj.handling_processes[key].process === 'cheese_producer_manufacture_cheese'
                    && fileDataObj.handling_processes[key].count < 10) {
                    process_id = key;
                    process_handling_challenge = fileDataObj.handling_processes[key].handler;
                    fileDataObj.handling_processes[key].handler = "";
                    break;
                }
            }
        }
        if (process_id !== undefined) {
            await db.update(user, fileDataObj, args[0]);
            break;
        }
    }


    var process_ownership_challenge_buffer = new Buffer(process_handling_challenge.toString(), 'utf8');
    console.log("Signing IOT Update");
    var process_ownership_response = fabric_client.getCryptoSuite().sign(iot_user, process_ownership_challenge_buffer);


    var iotUpdate = {};
    iotUpdate.process_token = {
        item_id: process_id,
        input_challenge_response: process_ownership_response,
    };
    iotUpdate.iot_device_public_key = iot_user.getPublicKey().toBytes();
    iotUpdate.update_signature = iotUpdateSignature;
    iotUpdate.update = Array.prototype.slice.call(iotUpdateBuffer, 0);
    iotUpdate.update_type = "IOTUpdate";

    console.log("Generating IOT Update Request");
    var postRequest = {};
    postRequest.chaincode_id = 'cheeseproducer_cc';
    postRequest.function = 'iot_update';
    postRequest.args = [JSON.stringify(iotUpdate)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.request_type = 'iot_update';
    postRequest.item_id = process_id;
    postRequest.sync = true;

    console.log("Sending IOT Update Request");
    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
});