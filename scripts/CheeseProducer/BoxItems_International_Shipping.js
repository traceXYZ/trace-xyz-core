'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var fs = require('fs');
var request = require('request-promise-native');
const db = require('../server/dataaccess.js');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user = 'cheese_producer';

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }

    // todo: import the users private key
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgyJHFiQmxkifxoKy3\n" +
        "zAyBoedOcpvTdIFk2k2Wqi60P22hRANCAATijNJaE3IMQnqqgs/xt33/N7H0FvM2\n" +
        "cAyj4pLnXzG1+0sLClURLU5z2wrpDXffgUZnvf5+QvPdNci2wcEOFJb3\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user assigned to fabric client
}).then(async (priv_key) => {
    // We create request after reading user's private key

    var cheeseBoxingProcess = {
        "processType": "International Shippping",
        "location": "LOCAT3",
        "pos_machine_id": "MACH006"
    };

    var cheeseBox = {
        "boxType": "Shipping Container Box",
        "color": "Black",
        "packing_machine_id": "MACH009"
    };

    console.log("Generating Packaging Process for International Shipping");
    var cheeseBoxingProcessBuffer = new Buffer(JSON.stringify(cheeseBoxingProcess), 'utf8');
    var cheeseBoxBuffer = new Buffer(JSON.stringify(cheeseBox), 'utf8');
    console.log("Signing Packaging Process for International Shipping");
    var cheeseBoxingProcessSignature = fabric_client.getCryptoSuite().sign(priv_key, cheeseBoxingProcessBuffer);
    console.log("Signing Container for International Shipping");
    var cheeseBoxSignature = fabric_client.getCryptoSuite().sign(priv_key, cheeseBoxBuffer);

    var fileData;
    var fileDataObj;
    var cheese_item_handling_challenges = [];
    var cheese_item_ids = [];
    console.log("Picking challenges to authenticate in blockchain network");
    var limit = 0;
    while (true) {
        // if (limit > 100) {
        //     console.error("Error! Cannot pick challenges after 100 retries");
        //     process.exit();
        //     break
        // }
        // limit++;
        fileData = await db.query({user_id: user}, args[0]);
        if (fileData.length === 0) {
            continue;
        }
        fileDataObj = fileData[0];
        for (var key in fileDataObj.owning_items) {
            if (fileDataObj.owning_items[key].owner !== "" && fileDataObj.owning_items[key].handler !== ""
                && fileDataObj.owning_items[key].type === 'cheese_box_retailer_B' &&
                fileDataObj.owning_items[key].count === 0) {
                cheese_item_handling_challenges.push(fileDataObj.owning_items[key].handler);
                cheese_item_ids.push(key);
                console.log("B")
                fileDataObj.owning_items[key].handler = "";
                break;
            } else if (fileDataObj.owning_items[key].owner !== "" && fileDataObj.owning_items[key].handler !== ""
                && fileDataObj.owning_items[key].type === 'cheese_box_retailer_C' &&
                fileDataObj.owning_items[key].count === 0) {
                cheese_item_handling_challenges.push(fileDataObj.owning_items[key].handler);
                cheese_item_ids.push(key);
                console.log("C")
                fileDataObj.owning_items[key].handler = "";
                break;
            } else if (fileDataObj.owning_items[key].owner !== "" && fileDataObj.owning_items[key].handler !== ""
                && fileDataObj.owning_items[key].type === 'cheddar_box_retailer_E' &&
                fileDataObj.owning_items[key].count === 0) {
                cheese_item_handling_challenges.push(fileDataObj.owning_items[key].handler);
                cheese_item_ids.push(key);
                console.log("E")
                fileDataObj.owning_items[key].handler = "";
                break;
            }

        }
        console.log(cheese_item_ids.length);
        await db.update(user, fileDataObj, args[0]);
        if (cheese_item_ids.length === 3) {
            break;
        }
    }

    var cheese_package_handling_challenge_responses = [];
    for (var i = 0; i < cheese_item_handling_challenges.length; i++) {
        console.log("Handling Challenge: " + cheese_item_handling_challenges[i].toString());
        var cheese_item_handler_challenge_buffer = new Buffer(cheese_item_handling_challenges[i], 'utf8');
        console.log("Signing Item Handler Challenge");
        var token = {
            item_id: cheese_item_ids[i],
            input_challenge_response: fabric_client.getCryptoSuite().sign(priv_key, cheese_item_handler_challenge_buffer),
        };
        cheese_package_handling_challenge_responses.push(token)
    }

    var specData = fs.readFileSync('./ChaincodeInitParams/CHEESE_BOX_SPEC_001.txt');

    var cheeseBoxingRequest = {};
    cheeseBoxingRequest.boxing_spec_id = specData.toString();

    cheeseBoxingRequest.cheese_package_handling_tokens = cheese_package_handling_challenge_responses;
    cheeseBoxingRequest.cheese_owner_public_key = priv_key.getPublicKey().toBytes();
    cheeseBoxingRequest.boxing_process = Array.prototype.slice.call(cheeseBoxingProcessBuffer, 0);
    cheeseBoxingRequest.boxing_process_signature = cheeseBoxingProcessSignature;
    cheeseBoxingRequest.cheese_box = Array.prototype.slice.call(cheeseBoxBuffer, 0);
    cheeseBoxingRequest.owner_signature_for_cheese_box = cheeseBoxSignature;

    console.log("Generating Shipping Container Request");
    var postRequest = {};
    postRequest.chaincode_id = 'cheeseproducer_cc';
    postRequest.function = 'box_cheese';
    postRequest.args = [JSON.stringify(cheeseBoxingRequest)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.request_type = 'merge';
    postRequest.item_type = 'shipping_container';
    postRequest.sync = true;

    console.log("Sending Shipping Container Request");
    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
});

