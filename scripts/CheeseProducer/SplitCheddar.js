'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var fs = require('fs');
var request = require('request-promise-native');
const db = require('../server/dataaccess.js');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user = 'cheese_producer';

    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }

    // todo: import the users private key
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgyJHFiQmxkifxoKy3\n" +
        "zAyBoedOcpvTdIFk2k2Wqi60P22hRANCAATijNJaE3IMQnqqgs/xt33/N7H0FvM2\n" +
        "cAyj4pLnXzG1+0sLClURLU5z2wrpDXffgUZnvf5+QvPdNci2wcEOFJb3\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
    // get a transaction id object based on the current user assigned to fabric client
}).then(async (priv_key) => {
    // We create request after reading user's private key

    var cheesePackages = [];
    for (var i = 0; i < 5; i++) {
        cheesePackages.push({
            "itemType": "cheddar",
            "qty": 0.02,
            "type": "Small Pack"
        })
    }
    for (var i = 0; i < 5; i++) {
        cheesePackages.push({
            "itemType": "cheddar",
            "qty": 0.08,
            "type": "Large Pack"
        })
    }

    console.log("Generating Cheddar Splitting and Packaging Process");
    var cheesePackageSignatures = [];
    var cheesePackagesBytes = [];
    console.log("Signing All generated Cheddar Packages");
    for (var i = 0; i < cheesePackages.length; i++) {
        var cheesePackageBuffer = new Buffer(JSON.stringify(cheesePackages[i]), 'utf8');
        cheesePackageSignatures.push(fabric_client.getCryptoSuite().sign(priv_key, cheesePackageBuffer));
        cheesePackagesBytes.push(Array.prototype.slice.call(cheesePackageBuffer, 0))
    }

    var cheeseSplittingProcess = {
        "processType": "Cheddar Splitting",
        "split_units": "KG",
        "location": "LOCAT3",
        "pos_machine_id": "MACH005"
    };

    var cheeseSplittingProcessBuffer = new Buffer(JSON.stringify(cheeseSplittingProcess), 'utf8');
    console.log("Signing Cheddar Splitting and Packaging Process");
    var cheeseSplittingProcessSignature = fabric_client.getCryptoSuite().sign(priv_key, cheeseSplittingProcessBuffer);

    var fileData;
    var fileDataObj;
    var cheese_item_id;
    var cheese_item_ownership_challenge;
    var cheese_item_handling_challenge;
    console.log("Picking challenges to authenticate in blockchain network");
    var limit = 0;
    while (true) {
        // if (limit > 100) {
        //     console.error("Error! Cannot pick challenges after 100 retries");
        //     process.exit();
        //     break
        // }
        // limit++;
        fileData = await db.query({user_id: user}, args[0]);
        if (fileData.length === 0) {
            continue;
        }
        fileDataObj = fileData[0];
        for (var key in fileDataObj.owning_items) {
            if (cheese_item_id === undefined) {
                if (fileDataObj.owning_items[key].owner !== "" && fileDataObj.owning_items[key].handler !== ""
                    && fileDataObj.owning_items[key].type === 'cheddar' && fileDataObj.owning_items[key].count === 0) {
                    cheese_item_id = key;
                    cheese_item_ownership_challenge = fileDataObj.owning_items[key].owner;
                    cheese_item_handling_challenge = fileDataObj.owning_items[key].handler;
                    fileDataObj.owning_items[key].handler = "";
                    break;
                }
            }
        }
        if (cheese_item_id !== undefined) {
            await db.update(user, fileDataObj, args[0]);
            break;
        }
    }

    var cheese_item_owner_challenge_buffer = new Buffer(cheese_item_ownership_challenge.toString(), 'utf8');
    var cheese_item_handler_challenge_buffer = new Buffer(cheese_item_handling_challenge.toString(), 'utf8');
    console.log("Signing Cheddar Ownership Challenge");
    var cheese_item_ownership_challenge_response = fabric_client.getCryptoSuite().sign(priv_key, cheese_item_owner_challenge_buffer);
    console.log("Signing Cheddar Handling Challenge");
    var cheese_item_handling_challenge_response = fabric_client.getCryptoSuite().sign(priv_key, cheese_item_handler_challenge_buffer);

    var specData = fs.readFileSync('./ChaincodeInitParams/CHEESE_SPLITTING_SPEC_001.txt');

    var cheeseSplittingRequest = {};
    cheeseSplittingRequest.splitting_spec_id = specData.toString();
    cheeseSplittingRequest.cheese_ownership_token = {
        item_id: cheese_item_id,
        input_challenge_response: cheese_item_ownership_challenge_response,
    };
    cheeseSplittingRequest.cheese_handling_token = {
        item_id: cheese_item_id,
        input_challenge_response: cheese_item_handling_challenge_response,
    };
    cheeseSplittingRequest.cheese_owner_public_key = priv_key.getPublicKey().toBytes();
    cheeseSplittingRequest.splitting_process = Array.prototype.slice.call(cheeseSplittingProcessBuffer, 0);
    cheeseSplittingRequest.splitting_process_signature = cheeseSplittingProcessSignature;
    cheeseSplittingRequest.cheese_packages = cheesePackagesBytes;
    cheeseSplittingRequest.package_signatures = cheesePackageSignatures;

    console.log("Generating Cheddar Splitting and Packaging Request");
    var postRequest = {};
    postRequest.chaincode_id = 'cheeseproducer_cc';
    postRequest.function = 'cut_and_package_cheese';
    postRequest.args = [JSON.stringify(cheeseSplittingRequest)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.request_type = 'divide';
    postRequest.item_type = 'cheddar_package';
    postRequest.cheese_id = cheese_item_id;
    postRequest.sync = true;

    console.log("Sending Cheddar Splitting and Packaging Request");
    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
});