#!/bin/bash
# Exit on first error
set -e

# clean the keystore
#rm -rf ../trace-key-store

# launch blockchain network
cd ../networks/advanced-network
./start.sh

#sleep
sleep 10

printf "Successfully launched the Blockchain Network\n"

