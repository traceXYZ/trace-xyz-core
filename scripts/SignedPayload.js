'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var Fabric_CA_Client = require('fabric-ca-client');

var path = require('path');
var util = require('util');
var os = require('os');
var fs = require('fs');
var winston = require('winston');
var logger = winston.createLogger({
    level: 'debug',
    transports: [
        new (winston.transports.Console)({colorize: true}),
    ]
});

//
var fabric_client = new Fabric_Client();
var fabric_ca_client;

// setup the fabric network
var channel = fabric_client.newChannel('cheeseproduction');
var peer = fabric_client.newPeer('grpc://localhost:7051');
channel.addPeer(peer);
var order = fabric_client.newOrderer('grpc://localhost:7050')
channel.addOrderer(order);


var args = process.argv.slice(2);

//
var member_user = null;
var store_path = path.join(__dirname, 'hfc-key-store');
console.log('Store path:' + store_path);
var tx_id = null;

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    // assign the store to the fabric client
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    // use the same location for the state store (where the users' certificate are kept)
    // and the crypto store (where the users' keys are kept)
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    var user = 'admin';


    if (args[3]) {
        user = args[3];
    }
    // get the enrolled user from persistence, this user will sign all requests
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded user1 from persistence');
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }

    // get a transaction id object based on the current user assigned to fabric client
    tx_id = fabric_client.newTransactionID();
    console.log("Assigning transaction_id: ", tx_id._transaction_id);


    console.log()
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQglmPRQDosk/vgbwp9\n" +
        "cTSzq35OwPdYM4DIJHNWRSYoBuChRANCAAR6Q8Qm7PqEkimkLXWvfNGMgGTQlVDi\n" +
        "VKqr4kuSSd+6B/oQkt3eQCkzMdRVwgfmm4aK6DUKM9Vq6d/6BymzOW6f\n" +
        "-----END PRIVATE KEY-----";

    var pukey = "-----BEGIN PUBLIC KEY-----\n" +
        "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEdAAj4KAUXvDNNgm3/NO2WQnQA96y\n" +
        "Re053DKDMc8dApmyevlev2kMkjDzLSCR1fg6h01Cu94is3xxzfCUBmamsQ==\n" +
        "-----END PUBLIC KEY-----";


    return fabric_client.getCryptoSuite().importKey(prkey)

}).then((priv_key) => {
    console.log(priv_key.getPublicKey())

    // var	tlsOptions = {
    //     trustedRoots: [],
    //     verify: false
    // };
    //fabric_ca_client = new Fabric_CA_Client('http://localhost:7054', tlsOptions , 'ca.tracexyz.com', fabric_client.getCryptoSuite());

    //return fabric_ca_client.newIdentityService().getAll(member_user)


    var msgHash = fabric_client.getCryptoSuite().hash("ABC", "SHA256");
    var msgSign = fabric_client.getCryptoSuite().sign(priv_key, new Buffer(msgHash, 'utf8'));

    console.log(msgSign.toString())

});