'use strict';
/*
 * Chaincode Invoke
 */

var Fabric_Client = require('fabric-client');
var path = require('path');
var winston = require('winston');
const MESSAGE = Symbol.for('message');
var request = require('request-promise-native');
require('console-stamp')(console, '[HH:MM:ss.l]');
var args = process.argv.slice(2);

const jsonFormatter = (logEntry) => {
    const base = {timestamp: new Date()};
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};

var logger = winston.createLogger({
    level: 'debug',
    format: winston.format(jsonFormatter)(),
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: '../logs/milk_producer.log',
            level: 'info',
            json: false,
        }),

    ]
});

var fabric_client = new Fabric_Client();
var user;
var member_user = null;
var store_path = path.join(__dirname, '../hfc-key-store');

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
Fabric_Client.newDefaultKeyValueStore({
    path: store_path
}).then((state_store) => {
    fabric_client.setStateStore(state_store);
    var crypto_suite = Fabric_Client.newCryptoSuite();
    var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    user = 'salt_producer';
    return fabric_client.getUserContext(user, true);
}).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
        member_user = user_from_store;
    } else {
        throw new Error('Failed to get user1.... run registerUser.js');
    }
    // todo: import the users private key
    var prkey = "-----BEGIN PRIVATE KEY-----\n" +
        "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgiHWkK8r6YiG/JLKc\n" +
        "+0bte5+NQqMEwkeoRHkoCJANCkShRANCAARG4+6yjsjfqsfTFpACm0LsBd2Tkv/9\n" +
        "/+M/lolVb2rXf20jyLAn7WfDWhiY5t98RGFuvOUQnc1zSwXOqRnPW4e3\n" +
        "-----END PRIVATE KEY-----";
    return fabric_client.getCryptoSuite().importKey(prkey)
}).then(async (priv_key) => {

    var saltItem = {
        "itemType": "salt",
        "qty": 100,
        "item_image": "IMG"
    };

    var saltProductionProcess = {
        "processType": "Salt Production",
        "production_units": "KG",
        "lakeid": "LK001",
        "location": "LOCAT2",
        "machine_id": "MACH002"
    };

    console.log("Created Salt Item");
    console.log("Created Salt Production Process");
    var saltItemBuffer = new Buffer(JSON.stringify(saltItem), 'utf8');
    var saltProdProcessBuffer = new Buffer(JSON.stringify(saltProductionProcess), 'utf8');

    console.log("Signed Salt Item");
    console.log("Signed Salt Production Process");
    var saltItemSignature = fabric_client.getCryptoSuite().sign(priv_key, saltItemBuffer);
    var saltProductionProcessSignature = fabric_client.getCryptoSuite().sign(priv_key, saltProdProcessBuffer);

    var saltProductionRequest = {};
    saltProductionRequest.salt_production_spec_id = "";
    saltProductionRequest.salt_producer_public_key = priv_key.getPublicKey().toBytes();
    saltProductionRequest.salt_item = Array.prototype.slice.call(saltItemBuffer, 0);
    saltProductionRequest.salt_item_signature = saltItemSignature;
    saltProductionRequest.salt_production_process = Array.prototype.slice.call(saltProdProcessBuffer, 0);
    saltProductionRequest.salt_production_process_signature = saltProductionProcessSignature;

    console.log("Generating Salt Production Request");
    var postRequest = {};
    postRequest.chaincode_id = 'saltproducer_cc';
    postRequest.function = 'produce_salt';
    postRequest.args = [JSON.stringify(saltProductionRequest)];
    postRequest.chain_id = 'cheeseproduction';
    postRequest.request_type = 'create';
    postRequest.item_type = 'salt';
    postRequest.sync = true;

    console.log("Sending Salt Production Request");
    var response = await request.post({
        url: 'http://localhost:' + args[1] + '/chaincode/invoke',
        form: {user_id: user, test_id: args[0], ccrequest: postRequest}
    });
    console.log(response);
    process.exit();
});